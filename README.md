# Get start

## Setup environment

1. Install python 3.12 : https://www.python.org/downloads/   

2. Create virtual environment:

    `py -3.12 -m venv .venv`

3. Activate the virtual environment, run:

   `.\.venv\Scripts\activate`

   If you want to deactivate it, run:

      `deactivate`

4. Install requirements

   `pip install -r ./requirements.txt`


## Build project locally

You can build the project locally to check the html. 
Go to the root folder and run: 

`make html` or `.\make.bat html`

Please pay attention to errors highlighted in the output.

Once it's build, open `build/html/index.html` 

## Deployment 
New commits to `develop` branch and `master` branch are deployed automatically. 

The development site: https://docs.device-service.gwdevices.com/en/develop/api/index.html

The production site: https://docs.device-service.gwdevices.com/en/latest/api/index.html

To manage the automatic build, login to https://readthedocs.org/dashboard/

## Tools 

* Table generator: https://www.tablesgenerator.com/text_tables
* reStructuredText: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
