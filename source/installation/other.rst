=========================
Switch Controller Setup
=========================


Switch Controller
------------------

Check the switch is working
***************************
The switches are simple loopback controllers. This effectively means that whilst an application listens and writes
to a single serial port, the switch can interrupt anything being sent on the 'loop'. You can test this by first plugging the
switch into a COM port. You can then open Putty to listen and write to the COM port simultaneously.

.. image:: static/Pictures/Putty_COM.png
   :scale: 70 %
   :align: center



+ In the Putty terminal, try to type with a keyboard. If the loop is broken, the letters should fail to appear.
+ The loop is broken or 'Open' when the button is not held down.
+ Now try pressing the switch button down and typing again.
+ Whilst the button is held down and the switch 'loop' is closed, you should see letters appear in the terminal.
+ If the switch works successfully, take note of the COM port and move onto the configuration.
+ If the switch doesn't seem to work, ie. you can't stop and start the keyboards input in the terminal with the button then perhaps check you are on the correct COM port.

Configuration
**************

The Switch Controller can be configured to monitor multiple IO switches and send hardware alerts to the genkiosk management
system.

+ Add the SwitchControllerService to the plugins.xaml.
+ Navigate to  C > ProgramData > GW Devices > Applications > Device Service > configs >modules
+ Edit SwitchControllerService.config

.. image:: static/Pictures/switch_controller_config_example.png
   :scale: 70 %
   :align: center

+ Set the value for NumberOfSwitches to the amount of configured switches.
+ Ensure for every row you increment the number after "Switch" in the field key. It will search these key value pairs by name.
+ The value for each switch holds multiple field values
+ Individual values are separated by '|'. The table below explains each individual field value. Make sure the order is correct.


<field key="Switch1" value="COM Port|Object Name|Object State Closed|Object State Open" />

    +----------------+------------------------------------------------------------------------------+
    | Field          | Description                                                                  |
    +================+==============================================================================+
    | COM Port       | The COM port the switch is plugged into.                                     |
    +----------------+------------------------------------------------------------------------------+
    | Object Name    | The name given to the switch based on its use. ie. Door                      |
    +----------------+------------------------------------------------------------------------------+
    | Object State   | A state to describe the object if the switch is found to be open. ie Unlocked|
    | Open           |                                                                              |
    +----------------+------------------------------------------------------------------------------+
    | Object State   | A state to describe the object if the switch is found to be closed. ie Locked|
    | Closed         |                                                                              |
    +----------------+------------------------------------------------------------------------------+

+ Remember, the switch is considered closed when the button is pressed and open when the button is released.

Configuring alerts
******************
Combining the 'Object Name' and the 'Object State' we can then create specific descriptions for our alerts ie:

+ Door Locked
+ Door Unlocked

All switches also have an Unknown state for when an error was thrown when trying to connect to a serial port, this
is usually when something is already connected to it.

Once you have configured a switch, start the device service and try opening and closing the switch a few times. Check
in the logs to see if hardware alerts have been triggered. Once genkiosk is ran, and the event logs are sent to V3
you should be able to see the new device alert states under Switch_Controller device. To trigger the unknown alert you
can force the switch to be busy by first connecting to the COM port via putty before starting the device service.

