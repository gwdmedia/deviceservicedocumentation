=========================
Installing Card Terminals
=========================
 
 
Please install the card terminal you will be using for your application. You may ignore the instructions for any
other payment providers and only follow the ones relevant to your application.
 
 
----------------------------------------------------------------------------------------------------------------------- 
 

Bolletta Device Service and IPP320 Card Terminal
------------------------------------------------

Installation
************
 
You should be provided a .zip of files for installation and testing of the Bolletta Device Service (BDS).
 
 
Begin by unzipping BDSSetup.zip and then run the setup.exe. The set up will install BDS, .NET and the IPP320 drivers as well.
Make sure to install the BDS to all users, not just the current user.
 
 
You can accept most of the default installation however it is important to force the device to always use a specific COM port.  
This will ensure that the device is automatically assigned the same COM Port on startup which means that no extra
configuration is required in future (providing you use the correct default COM Port 13).
 
 
Follow the installation instructions and when reaching the "USB Driver Parameters" screen follow these steps: 
 
 
+ Check the Force COM port feature. 
+ In the first "Product ID" drop down select - 0060 (Ingenico iPP3xx/iPP4xx) 
+ In the "Corresponding Virtual COM Port" enter the value 13. 
+ Under "Select by Connection Order" enter 13 into the first box. 
+ Click next and complete the installation. 
 
 
------------------------------------------------------------------------------------------------------------------------ 
 
 
Testing the IPP320 Card Reader
******************************
 
The settings used for BDS will be provided to you by Heartland and GWD. Once BDS is installed, you can test it by double clicking
the icon on the desktop. The BDS application will appear on the tray, you can right click the icon to update
configuration and to see the test URLs.

 
----------------------------------------------------------------------------------------------------------------------- 

 
Testing the BDS Web Service
***************************
 
 
Local Address is: http://localhost:8526/DeviceService/BDS/
 
 
This address should show the below error message when navigating in a browser on the machine running the service. 
 
 
.. image:: static/Pictures/serverRunning.PNG 
   :scale: 120 % 
   :align: center 
    
If you would like to consume the API before integrating it, we would recommend testing using Postman_ or Fiddler_. 
 
 
.. _Postman: https://www.getpostman.com/ 
.. _Fiddler: http://www.telerik.com/fiddler 
 
 
Below are details for manual consumption of the API and the results to expect if setup is working as expected. 
 
 
+-------------------+---------------------------------------+-------------------------------------+ 
|                   | Status                                | Transaction                         | 
+===================+=======================================+=====================================+ 
| Authentication    | Basic (User & PW provided)            | Basic (User & PW provided)          | 
+-------------------+---------------------------------------+-------------------------------------+ 
| Type              | GET                                   | POST                                | 
+-------------------+---------------------------------------+-------------------------------------+ 
| Sub Address       | /Status                               | /Transaction                        | 
+-------------------+---------------------------------------+-------------------------------------+ 
| Content Type      | N/A                                   | application/json                    | 
+-------------------+---------------------------------------+-------------------------------------+ 
| Body(raw)         | N/A                                   | { "amount":10,                      |
|                   |                                       | "Reference":"987987"}               |
+-------------------+---------------------------------------+-------------------------------------+ 
| Correct Result    | {"Description":"Available",           | Amount shown on card terminal.      |
|                   | "StatusCode":1}                       | See Transaction Request             |
|                   |                                       | for details.                        |
+-------------------+---------------------------------------+-------------------------------------+ 


-----------------------------------------------------------------------------------------------------------------------


GlobalPay IPP350 Card Terminal
------------------------------

Installation
************

You should be provided a .zip of files for installation and testing of the card terminal.


Begin by running the *IngenicoUSBDrivers setup.exe*.


You can accept most of the default installation but it is important to force the device to always use a specific COM port.
This will ensure that the device is automatically assigned the same COM port on startup which means that no extra
configuration is required in future (providing you use the correct default Com Port 13).


Follow the installation instructions and when reaching the "USB Driver Parameters" screen follow these steps:


+ Check the Force COM port feature.
+ In the first "Product ID" drop down select - 0060 (Ingenico iPP3xx/iPP4xx)
+ In the "Corresponding Virtual COM Port" enter the value 13.
+ Under "Select by Connection Order" enter 13 into the first box.
+ Click next and complete the installation.


------------------------------------------------------------------------------------------------------------------------


Testing the GlobalPay IPP350 Card Reader
****************************************


You can test the card terminal installation by using the test application provided by GlobalPay.
The test application "TestComConcert.exe" is provided inside the CardMachineInstallation folder.


+ Ensure the card terminal is plugged in and connected to the internet. Note that the terminal requires the custom cable it came with, transactions are not possible using a regular USB cable.
+ Run the test application "TestComConcert.exe".
+ Enter 13 as "COM Port" and 38400 as "Baud Rate". These are the default configuration values our service uses.
+ Click the Open COM Port button, a message box saying connection successful should show.
+ Click Send Request Message and you should see a transaction screen appear on the terminal.
+ If a transaction appears on the reader, then the setup and configuration is successful.


-----------------------------------------------------------------------------------------------------------------------


Acceptacard and Payworks Miura 010 Card Reader
----------------------------------------------

Installation
************

You should be provided a .zip of files for installation and testing of the card terminal and Payworks PayServer.


PayServer
^^^^^^^^^

Please create a new directory called Payworks in directly under C drive, "C:\\Payworks". Then unzip the contents of the
Payworks payserver bundle into the folder ("mpos.payserver.bundle-1.13.0-windows-x64" or equivalent). Add the "config.yaml"
you have been provided with into the "server" sub directory, and then Double click the "Install Payserver Service"
to install the service.


Miura 010 Card Reader
^^^^^^^^^^^^^^^^^^^^^

The card reader should be set up to be used in USB mode. This configurations should have been handled by Acceptacard for
the live readers, but for test devices, see http:\/\/www.payworks.mpymnt.com\/node\/303\/ for information. When you connect
the card reader to the kiosk, it should be visible in the Device Manager under "Ports (COM & LPT)". Check the port number
and then ensure this is included in the PayServer config.yaml and the Device Service AcceptacardService.config. We can
assist with updating these configuration files if necessary.
It should not show up as a drive. See http:\/\/www.payworks.mpymnt.com\/node\/304 for troubleshooting information.



------------------------------------------------------------------------------------------------------------------------


Testing the Acceptacard and Payworks Miura 010 Card Reader
**********************************************************


You can test the card reader installation by using the test application provided by Payworks. Unzip the test application
and then run the "Payworks.PayClient.Tester.exe". Fill in the merchant details, connect to the reader and start a payment
request. If you can see the amount show up on the card terminal and complete a transaction, the set up should be finished.


Troubleshooting
^^^^^^^^^^^^^^^

+ Ensure the Payworks Payserver is installed in the correct location so that it can be managed by Genkiosk
+ Ensure the PayServer is running and no other services are using the same port - you should always use the configuration
  file we provided to ensure there are no clashes
+ Check the port number in Device Manager for the Miura Card reader and then ensure this is included in the
  PayServer config.yaml and the Device Service AcceptacardService.config. These files can be remotely updated using
  Genkiosk if necessary.
+ For other card terminal issues, see the documentation at http:\/\/www.payworks.mpymnt.com. If any Payworks settings
  need to be updated, Acceptacard can handle that for you.



-----------------------------------------------------------------------------------------------------------------------


CreditCalls ChipDNA Server and Miura 010 Card Reader
-------------------------------------------------------------


Install and configure the ChipDNA Server
******************************************
In order to use Credit Calls Miura 010 Card Reader with the device service, you must configure and install Credit Calls'
ChipDNA service to automatically start with windows. The device service uses this to communicate with the card reader
and helps to enhance its security.

To do so:

1. Browse to C:\ChipDNA\ChipDNA Server where you will find a sample configuration file (chipdna.config.xml) that you can
   adapt. Or for a completely fresh start, overwrite this with a copy of the blank template file also provided
   (chipdna.config.xml.template).


2. Using your text editor, edit chipdna.config.xml and change the values with **** values to corresponding values:
   - TerminalID - An 8 digit Id provided in registration email with WebMIS.

   - TransactionKey - A 16 digit key provided in registration email with WebMIS.

   - Socket - 127.0.0.1:1869

   - ApplicationIdentifier - Application Identifier agreed with Creditcall.

   - Comport - Configured device comport found in Control Panel  Device manager - Com port

   - DeviceID - The 8 digit number following s/n 0 found on back of the device. (S/N 0xx-xxxxxx)

   - Baudrate - 115200;


3. When you have finished editing, save the file and close the text editor.


4. Using your text editor, edit service.install.bat and make sure that binpath shows the correct full path for
   ChipDNAServer.exe (in our examples this is C:\ChipDNA\ChipDNA Server\ChipDNAServer.exe).


5. Execute service.install.bat. If for any reason the installation fails, use the command prompt to run the following
   command which will uninstall any previous ChipDNA Server Service installations:
   sc delete CreditcallChipDNAServer.
   Then try installing again.


6. By default, the CreditcallChipDNAServer service is installed to run under the ‘Local System’ account and to be
   started manually. As we require the service to run automatically when the machine reboots we need to set the service
   to do this. Open the management console for Windows services and change the properties of the ChipDNA Server service
   from Manual to Automatic.

 6a. Control Panel -> Administrative Tools -> Services.

 6b. Right click CreditCall ChipDNAService and select properties.

 6c. Under startup type in the general tab select automatic and then click Apply -> Ok.

7. Right click on the service and click refresh, ensure the service is "running".

Install and config the Miura m-010 card terminal
************************************************
The card terminal is very straight forward to setup.

First ensure the usb connectivity is enabled. Out the box, on turning the device, you should be able to see MIURA
written in large text on the small terminal screen. Above the M should be a usb icon. If you cannot see the USB
icon, then please contact us to request the config from CreditCall.

Once connected and the ChipDNA Server is running, the usb symbol should change to that of a connected symbol,
indicating it is talking to the server. You should also see the device appear in your windows device manager under
"Ports (COM & LPT)".
If you have configured your ChipDNA Server properly, then the card terminal should begin pulling and downloading it's
config from the WebMIS server. This will be shown on the card terminal screen. On completion, the default screen should
show "ChipDNA" at the top and "Powered by Creditcall" underneath.

Set config files in the device service
****************************************

Before talking the the ChipDNA service the device service needs some values configured. This is partly for security
as the ChipDNA service requires the device service to provide it's Id to communicate, but also ensures the correct
card reader is being found and used in the event that there are multiple card readers connected to one kiosk.

1. Once the device service has been installed navigated your file explorer to:
   C:\ProgramData\GW Devices\Applications\Device Service\configs\modules
   (If you have changed the default installation address you will need to search for the modules directory)

2. Edit MiuraM010CreditCallService.config file.
   Enter the same configuration values which you used for the ChipDNA service config
   (terminal_id, ip_address (socket xxx.x.x.x:----), port(socket ---.-.-.-:xxxx), device_id).

3. Restart the kiosk and ensure both the Device Service and ChipDna service are running:
   Control Panel -> Administrative Tools -> Services.


Troubleshooting
^^^^^^^^^^^^^^^
+ Ensure both services are running in windows services.

+ If the Device Service is running but the ChipDNA service is not, then it is likely there is an issue with the installation or the config validation.

+ Ensure you can see the Pi Usb to Serial (COM-X) in windows device manager, "Ports (COM & LPT)" tab.

+ If the device service is returning responses, but the errors return indicate the Service or Card terminal is not available then check the ChipDNA service logs, found in the same directory as ChipDNAServer.exe and the config file it is in.

+ If the response is timing out, then it is likely that the terminal Id in device service does not match that in the ChipDNA service and so is ignoring the transaction request.