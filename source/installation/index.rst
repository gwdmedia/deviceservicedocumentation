============
Installation
============


The GWD Device Service must be installed first. Additionally any card terminals and other devices may need to be installed
with their appropriate drivers.

Current installation is tried and tested on Windows 10 Professional Edition and integrated with
the Ingenico IPP320 Card Terminal and Bolletta Device Service.


-----------------------------------------------------------------------------------------------------------------------

.. toctree::
   :maxdepth: 2

   device_service
   card_terminals
   cash_acceptors
   printers
   other


