===================
Installing Printers
===================

If you need to use the printer service to print receipts or other documents, install the printer and drivers following the
manufacturer's instructions. Provide GWD with the name and printer queue name for your printer so that the
Device Service set up is completed for you. GWD can also assist you in customising the receipt template for your application.


