=========================
Installing Cash Acceptors
=========================


Please install the cash acceptor you will be using for your application. You may ignore the instructions for any
other payment providers and only follow the ones relevant to your application.


-----------------------------------------------------------------------------------------------------------------------


ITL NV200/NV9, Smart Payout & SMART Coin System
-----------------------------------------------

The installation is the same for all the ITL devices as they share the drivers.

Installation
************

Connect the serial cable and install the driver. Here are the more detailed steps:

1. You should have received a zip of "NVxx, BVxx, DA3 - 64 bit" or "NVxx, BVxx, DA3 - 32 bit", unzip the files.
2. Connect the serial cable, then open the device manager on the computer and locate the device.
3. Right click the device, then choose to manually install/update the driver. In the following dialogue, browse to the "NVxx, BVxx, DA3 - 64/32 bit" directory. Please note that the driver is unsigned, so for Windows 8 and up you will need to disable driver signature enforcement temporarily to install the driver.

    a. Open an admin cmd and force restart with: **shutdown.exe /r /o /f /t 0**
    b. When the system restarts you can select "Disable Driver Signature Enforcement" from the option screens
    c. restart the PC again after the driver installation to enable to driver signature enforcement again

4. Once the driver has been installed, add the COM port number to the device service configuration found in
C:\\ProgramData\\GW Devices\\Applications\\Device Service\\configs\\modules\\NV200SmartPayoutService.config and restart
the device service.

Please refer to the manufacturer "Software Guide.pdf" for more detailed instructions if needed.

