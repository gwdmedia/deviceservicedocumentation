=============================
Installing GWD Device Service
=============================

Follow the steps below to install the Device Service.

+ In the files we have provided, find and run DeviceServiceSetup.exe


+  Ensure you allow Unknown Publisher


.. image:: static/Pictures/unknownPublisher.png
   :scale: 70 %
   :align: center

+ Accept the terms and conditions and install.


.. image:: static/Pictures/installation.png
   :scale: 70 %
   :align: center

After the installation, you can verify that the service is running by checking the local services.
The service will automatically start every time Windows starts. If you want to verify the service is  running


+ In the Windows menu find and launch "Services"


.. image:: static/Pictures/servicesIcon.png
   :align: center

+ In the list of services find "Device Service", it should currently be running.


.. image:: static/Pictures/running.PNG
   :scale: 100 %
   :align: center
