===================
Receipt Printer API
===================

This endpoint allows monitoring configured printer and printing receipts using specified template and data.

Url
---
The base url is http://localhost:8526/DeviceService/PrinterService .


Status
------
Get the current status of the printer named in the device service configuration.
This can be used to check if a printer is available before attempting to print a receipt.
Note that an external printer monitor may be required in order to get detailed information on the printer status.

:URL: /status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/printerservice/status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Printer Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "IsOutOfPaper": false,
            "IsOffline": false,
            "IsPaperJammed": false,
            "NeedUserIntervention": false,
            "HasPaperProblem": false,
            "IsBusy": false,
            "IsInError": false,
            "IsNotAvailable": false,
            "IsOutOfMemory": false,
            "IsPaused": false,
            "IsNotFound": false
        }


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }




Print Receipt
-------------
Print a receipt using specified template and data. If you want to print a customized receipt, please contact us.
We will help you to create a template and to configure the application to use that template.

:URL: /PrintReceipt
:Method: POST
:URL Params:  none
:Data Params:

    The data parameters needed to print a receipt may be different for each template. We will provide you with
    your template name and the parameters during the set up process.

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | template   | Yes      | Template name                                  | string             |
    +------------+----------+------------------------------------------------+--------------------+
    | parameters | Yes      | Data used by the receipt                       | object             |
    +------------+----------+------------------------------------------------+--------------------+


:Example:

    Please note that the template name and parameters in the request below are provided as an example only.
    You can contact us to get your own template and parameters list.

    ::

        POST http://localhost:8526/DeviceService/PrinterService/PrintReceipt HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "template": "bds_card",
            "parameters": {
                "merchantName":"test_merchant",
                "merchantAddress":"test street 123, test town, testland",
                "merchantPhone": "123287742",
                "totalAmount": 10.22,
                "transactionId": "1254889547",
                "cardNumber":"**** **** **** 3412",
                "isSuccessful": false,
                "transactionDate":"2017-12-08T14:00:02Z",
                "itemisedCost":[
                    {"label":"item1", "amount":5.22},
                    {"label":"item2", "amount":3},
                    {"label":"item3", "amount":2}
                ]
            }
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Printing Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "StatusCode": 1,
            "Description": "SuccessfullySentToPrintQueue"
        }


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: `Printing Status`_

    This is returned if any params are missing or incorrect.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "StatusCode": 2,
            "Description": "template name you provided is not a valid receipt template name."
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


--------------------------------------------------------------------------------------------------------------

Printer API Models
------------------

Printer Status
==============

The printer status has the following boolean fields for possible problems. The fields default to false, and if
the printer is available to use, all the fields should be false.
If a printer reports a problem when status is queried, the corresponding field will be set to true. Note that some printers
may not accurately return all this information, so it is possible for a print receipt request to fail even if the
status returned by a status request did not indicate errors. If the IsNotFound is set to true, a printer named in the
Device Service configuration was not found, so it may indicate an error in the configuration of the device service.


+-----------------------+
| Field name            |
+=======================+
| IsBusy                |
+-----------------------+
| IsInError             |
+-----------------------+
| IsNotAvailable        |
+-----------------------+
| IsOffline             |
+-----------------------+
| IsOutOfMemory         |
+-----------------------+
| IsOutOfPaper          |
+-----------------------+
| IsPaperJammed         |
+-----------------------+
| IsPaused              |
+-----------------------+
| HasPaperProblem       |
+-----------------------+
| NeedUserIntervention  |
+-----------------------+
| IsNotFound            |
+-----------------------+


Printing Status
===============

These are the status codes returned with a `Print Receipt`_ request. The Description returned will vary and may
give further details in case of an error.

+------------+------------------------------+-------------------------------------------------------------+
| StatusCode | Example Description          | Explanation                                                 |
+============+==============================+=============================================================+
| 0          | NoPrintQueue                 | This indicates an external error such as a printer not      |
|            |                              | available or the configuration for a printer being invalid. |
|            |                              | (Ensure that the printer is correctly installed and plugged |
|            |                              | in. The device service must also be configured to use this  |
|            |                              | printer, the configuration is done during initial set up.   |
|            |                              | Please contact us if you wish to change printers.)          |
+------------+------------------------------+-------------------------------------------------------------+
| 1          | SuccessfullySentToPrintQueue | Receipt created successfully and sent to print queue. Note  |
|            |                              | that we cannot guarantee the printing will finish           |
|            |                              | successfully, but a printer was found and the item was      |
|            |                              | added to the queue.                                         |
+------------+------------------------------+-------------------------------------------------------------+
| 2          | ErrorCreatingReceipt         | An error occurred while creating the receipt so the receipt |
|            |                              | cannot be printed. This may also mean that the receipt      |
|            |                              | template is missing or invalid.                             |
+------------+------------------------------+-------------------------------------------------------------+

