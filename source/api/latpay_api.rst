Latpay API
==========

This endpoint allows processing transactions using IDTech card reader through Latpay payment service.

Url
---
The base url is http://localhost:8526/DeviceService/Latpay/ .

Status
------
Get the current status of the card reader.
The status can be checked to make sure the card reader is available before making a quick pay request or when first time starting the application.
Please note that the connection status is not updated in realtime. If the card reader is disconnected after the service
is initialised, the status may still show as connected and a transaction will fail.

If the card read is busy, it means a transaction is being processed. You need to wait until the current transaction is finished, or cancel it
by using the cancellation API which will be described later in this doc.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/Latpay/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `DeviceStatus`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:12:53 GMT

        {
            "isBusy": false,
            "isConnected": true
        }


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





--------------------------------------------------------------------------------------------------------------



Quick Pay Request
-----------------

Send a new QuickPay transaction request to the terminal and receive a response detailing the transaction result.

If a user didn't successfully tap his card in 60 seconds after issuing this request, the transaction will be timed out.
This is handled by Latpay SDK. Please make sure the request to this endpoint doesn't timeout too early.

Note that you should check the transaction status to determine success and not rely on the HTTP status code.

:URL: /QuickPay

:Method: POST

:URL Params:  none

:Data Params:

    +------------------+----------+------------------------------------------------+--------------------+
    | Name             | Required | Description                                    | Data Type          |
    +==================+==========+================================================+====================+
    | amount           | Yes      | Transaction amount. Must be a positive number. | decimal or string  |
    +------------------+----------+------------------------------------------------+--------------------+
    | reference        | Yes      | Reference for the transaction.                 | string             |
    +------------------+----------+------------------------------------------------+--------------------+
    | purchase_summary |          | summary of the purchase                        | string             |
    +------------------+----------+------------------------------------------------+--------------------+

:Example:

    ::

        POST http://localhost:8526/DeviceService/Acceptacard/transaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "amount": 2,
            "reference": "20231019001929001",
            "purchase_summary": "donation"
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `QuickPayResponse`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 11:16:04 GMT

            {
                "result": {
                    "transactionId": "3894761",
                    "responseKey": "D6A64B02B3D3A5E0A8606D992DB2A133E8EC64402C3488595DB37E6052030173",
                    "customerId": "58268",
                    "transactionDateUTC": "05/11/2023 21:11:00",
                    "billing": {
                        "type": "emv",
                        "emv": null,
                        "card": {
                            "name": null,
                            "pan": "47617390XXXX0119",
                            "exp": "122025",
                            "token": "0xa55b7fc37f9e28a940450998481563afdb16110e"
                        },
                        "authResponse": {
                            "bankTransactionId": "330921568006",
                            "authCode": "039317",
                            "gatewayReference": "86568006"
                        }
                    },
                    "status": {
                        "statusCode": "0",
                        "statusDescription": null,
                        "errorCode": "",
                        "errorDescription": ""
                    },
                    "aid": "A00000000310109999",
                    "applicationLabel": "payworks visa"
                },
                "canceled": false,
                "timedout": false
            }



:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "amount",
                            "Message": "Amount 0 is not valid for a transaction"
                        }
                    ]
                }
            }

    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT


    500 Internal Server Error

        ::

            HTTP/1.1 500 Internal Server Error
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT
            {
                "ResponseStatus": {
                    "ErrorCode": "DeviceError",
                    "Message": null,
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "device_failure",
                            "Message": "Internal error while processing transaction"
                        }
                    ]
                }
            }


--------------------------------------------------------------------------------------------------------------


Cancel Transaction Request
--------------------------

Send a request to cancel the ongoing transaction. This endpoint will always return 200 HTTP OK to acknowledge the cancellation
has been started. Once the transaction is successfully cancelled, the response of the QuickPay request will have a flag saying the transaction is cancelled.

:URL: /CancelTransaction

:Method: POST

:URL Params:  none

:Data Params:

    NA

:Example:

    ::

        POST http://localhost:8526/DeviceService/Latpay/CancelTransaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body:

        ::

            HTTP/1.1 200 OK
            Content-Length: 0
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 11:16:04 GMT

:Error Responses:
    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT



--------------------------------------------------------------------------------------------------------------


Latpay API Models
=================

DeviceStatus
------------

+-------------+----------------------------------------------------------------+---------+
| Name        | Description                                                    | Type    |
+-------------+----------------------------------------------------------------+---------+
| isBusy      | Indicates whether the device is busy or not. When it's busy,   | Boolean |
|             | it means a transaction is in progress. You need to wait until  |         |
|             | it's no longer busy, or cancel the transaction                 |         |
+-------------+----------------------------------------------------------------+---------+
| isConnected | Indicates whether the device is physically connected.          | Boolean |
+-------------+----------------------------------------------------------------+---------+


QuickPayResponse
----------------

These fields are returned with a QuickPay request.

+-----------+--------------------------------------------------+---------------------------+
| Name      | Description                                      | Data Type                 |
+===========+==================================================+===========================+
| result    | The result of transaction                        | TransactionResult or null |
+-----------+--------------------------------------------------+---------------------------+
| cancelled | Indicates whether the transaction is cancelled.  | Boolean                   |
+-----------+--------------------------------------------------+---------------------------+
| timedout  | indicates whether the transaction timed out      | Boolean                   |
+-----------+--------------------------------------------------+---------------------------+

TransactionResult
-----------------

+---------------------------------------+-------------------------------------------------------------+-----------+
| Name                                  | Description                                                 | Data Type |
+=======================================+=============================================================+===========+
| transactionId                         | Latpay transaction id                                       | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| responseKey                           | Latpay response key                                         | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| customerId                            | Latpay custom id                                            | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| transactionDateUTC                    | Transaction date in UTC                                     | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| billing.type                          | Always emv (please refer to Latpay doc for more details)    | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| billing.card.pan                      | Masked card number                                          | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| billing.authResponse.authCode         | Gateway auth code                                           | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| billing.authResponse.gatewayReference | Gateway reference                                           | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| status.statusCode                     | Status code of the transaction.                             | string    |
|                                       | "0" - the transaction is approved.                          |           |
|                                       | "1" - the transaction is declined.                          |           |
|                                       | "2" - the transaction is pending or status is unknown.      |           |
+---------------------------------------+-------------------------------------------------------------+-----------+
| status.errorCode                      | If errorCode is not null or empty, it means the             |           |
|                                       | transaction failed.                                         | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
| applicationLabel                      | Latpay application label which indicates the type of card.  | string    |
+---------------------------------------+-------------------------------------------------------------+-----------+
