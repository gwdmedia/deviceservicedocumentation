GlobalPay and IPP350
====================

This endpoint allows processing transactions using GlobalPay and Ingenico IPP350 card terminal.

Url
---
The base url is http://localhost:8526/DeviceService/GlobalPay/ .

Status
------
Get the current status of the IPP350 Terminal. This can be used to ensure the terminal is available
before making a transaction request or when first starting the application.

:URL: /status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/GlobalPay/status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `IPP350CardTerminalState`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
           "Description":"Idle",
           "StatusCode":1
        }


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



--------------------------------------------------------------------------------------------------------------


Transaction Request
-------------------

Send a new transaction request to the terminal and receive a response detailing the transaction result.
The request result will be returned after the card terminal returns the transaction information.
This may take some time while the customer uses the terminal to pay. You should wait for the result to
be returned. If you time out before the transaction has completed you will not receive the information
but the terminal may still finish the transaction and charge the customer card.

:URL: /Transaction

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | Amount     | Yes      | Transaction amount. Must be a positive number. | decimal or string  |
    +------------+----------+------------------------------------------------+--------------------+

:Example:

    ::

        POST http://localhost:8526/DeviceService/GlobalPay/transaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "amount":15.00
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Transaction`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 11:16:04 GMT

            {
              "Transaction": {
                "CustomerReceipt": {
                  "AID": "AID: A0000000031010",
                  "Amount": "AMOUNT            £15.00",
                  "ApplicationLabel": "Visa Debit Test ",
                  "CVMResult": "PIN VERIFIED",
                  "CourtesyMessage": "Thank You",
                  "DataSource": "ICC",
                  "DateAndTime": "16/03/17 11:13",
                  "DiagnosticCode": "DIAG 73",
                  "FinalMessage1": "     CUSTOMER COPY      ",
                  "FinalMessage2": " PLEASE RETAIN RECEIPT",
                  "Handset": "HANDSET:1",
                  "IsDeclined": false,
                  "IsVoid": false,
                  "IssuerName": "VISA DEBIT",
                  "MerchantID": "M:***41234",
                  "MerchantName": "Global Payments         ",
                  "Message": "AUTH CODE:004040",
                  "PAN": "************5234",
                  "PanSequenceNumber": "PAN SEQ NO. 01",
                  "SCH_ID1": "SCH ID:00000000025485348",
                  "SCH_ID2": "77",
                  "SequenceNumber": "337",
                  "TerminalID": "TID:****1279        S",
                  "TransactionNumber": "TXN 0219",
                  "TransactionType": "SALE"
                },
                "ErrorCode": 0,
                "ErrorMessage": "NoError",
                "SignatureRequired": false,
                "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61",
                "MerchantReceipt": {
                  "AID": "AID: A0000000031010",
                  "Amount": "AMOUNT            £15.00",
                  "ApplicationLabel": "Visa Debit Test ",
                  "CVMResult": "PIN VERIFIED",
                  "DataSource": "ICC",
                  "DateAndTime": "16/03/17 11:13",
                  "DiagnosticCode": "DIAG 73",
                  "ExpiryDate": "EXP 12/20",
                  "FinalMessage1": "     MERCHANT COPY      ",
                  "FinalMessage2": " PLEASE RETAIN RECEIPT",
                  "Handset": "HANDSET:1",
                  "IsDeclined": false,
                  "IsVoid": false,
                  "IssuerName": "VISA DEBIT",
                  "MerchantID": "M:12341234",
                  "MerchantName": "Global Payments         ",
                  "Message": "AUTH CODE:004040",
                  "PAN": "************5234",
                  "PanSequenceNumber": "PAN SEQ NO. 01",
                  "SCH_ID1": "SCH ID:00000000025485348",
                  "SCH_ID2": "77",
                  "SequenceNumber": "337",
                  "StartDate": "STT 11/06",
                  "TerminalID": "TID:22161279        S",
                  "TransactionNumber": "TXN 0219",
                  "TransactionType": "SALE"
                }
              }
            }



        See `GlobalPay IPP350 Example Transaction JSON`_ for more examples for different kind of transaction results.

:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "amount",
                            "Message": "Amount 0 is not valid for a transaction"
                        }
                    ]
                }
            }

    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT



--------------------------------------------------------------------------------------------------------------

Refund Request
--------------

This is only available for later versions of Device Service after April 2019. The ComConcert.dll version must
be 3.0 or above or the refund will be processed as a sale transaction.

Send a new refund request to the terminal and receive a response detailing the transaction result.
The request result will be returned after the card terminal returns the transaction information.
This may take some time while the customer uses the terminal to complete the refund. You should wait for the result to
be returned. If you time out before the transaction has completed you will not receive the information
but the terminal may still finish the transaction and refund the customer card. Customer signature may be required,
please check the "SignatureRequired" field in the response.

:URL: /Refund

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | Amount     | Yes      | Refund amount. Must be a positive number.      | decimal or string  |
    +------------+----------+------------------------------------------------+--------------------+

:Example:

    ::

        POST http://localhost:8526/DeviceService/GlobalPay/Refund HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "amount": 5.00
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Transaction`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 11:16:04 GMT

            {
                "Transaction": {
                    "MerchantReceipt": {
                        "MerchantName": "Global Payments         ",
                        "MerchantID": "M:12341234",
                        "TerminalID": "TID:22161279        ",
                        "SequenceNumber": "S209",
                        "SCH_ID1": null,
                        "SCH_ID2": null,
                        "Handset": null,
                        "IssuerName": null,
                        "AID": null,
                        "ApplicationLabel": null,
                        "PAN": null,
                        "ExpiryDate": null,
                        "PanSequenceNumber": null,
                        "StartDate": null,
                        "DataSource": null,
                        "TransactionType": null,
                        "Amount": null,
                        "CVMResult": null,
                        "DateAndTime": null,
                        "Message": null,
                        "TransactionNumber": null,
                        "DiagnosticCode": null,
                        "FinalMessage1": null,
                        "FinalMessage2": null,
                        "IsVoid": false,
                        "IsDeclined": false
                    },
                    "CustomerReceipt": {
                        "MerchantName": "Global Payments         ",
                        "MerchantID": "M:***41234",
                        "TerminalID": "TID:****1279        ",
                        "SequenceNumber": "S209",
                        "SCH_ID1": null,
                        "SCH_ID2": null,
                        "Handset": "HANDSET:1",
                        "IssuerName": "VISA DEBIT",
                        "AID": "AID: A0000000031010",
                        "ApplicationLabel": "Visa Debit Test ",
                        "PAN": "************5234",
                        "PanSequenceNumber": "PAN SEQ NO. 01",
                        "DataSource": "ICC",
                        "TransactionType": "REFUND",
                        "Amount": "AMOUNT             #5.00",
                        "CVMResult": "SIGNATURE VERIFIED",
                        "CourtesyMessage": "Thank You",
                        "DateAndTime": "23/04/19 15:03",
                        "Message": "AUTH CODE:004087",
                        "TransactionNumber": null,
                        "DiagnosticCode": "DIAG 44",
                        "FinalMessage1": "     CUSTOMER COPY      ",
                        "FinalMessage2": " PLEASE RETAIN RECEIPT",
                        "IsVoid": false,
                        "IsDeclined": false
                    },
                    "ErrorCode": 0,
                    "ErrorMessage": "NoError",
                    "ReceiptId": "5b0a7cfe-5fc9-4ed6-856f-8170ff59c224",
                    "SignatureRequired": true
                }
            }


        See `GlobalPay IPP350 Example Transaction JSON`_ for more examples for different kind of transaction results.

:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "amount",
                            "Message": "Amount 0 is not valid for a refund"
                        }
                    ]
                }
            }

    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT



--------------------------------------------------------------------------------------------------------------

Reverse Last Transaction
------------------------

This is only available for later versions of Device Service after April 2019.

Reverse the last card transaction (sale or refund) made. This is intended to be used after a transaction is made where a signature is
required, but the customer has not provided a valid signature. The transaction number can be found on the Merchant receipt.

A reversal may only be completed within 80 seconds of the transaction being completed. HTTP 200 response indicates that
the reversal was successful. A customer receipt must be printed after a reversal, and the receipt should clearly
state the transaction was void. HTTP 500 will be returned if there is no transaction to reverse or the reversal fails.

:URL: /Reverse

:Method: POST

:URL Params:  none

:Data Params:

    +-------------------+----------+--------------------------------------------------------------+-----------+
    | Name              | Required | Description                                                  | Data Type |
    +===================+==========+==============================================================+===========+
    | transactionNumber | Yes      | The 4 digit transaction number of the transaction to cancel. | string    |
    +-------------------+----------+--------------------------------------------------------------+-----------+

:Example:

    ::

        POST http://localhost:8526/DeviceService/GlobalPay/Reverse HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "transactionNumber": "0225"
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2019 11:16:04 GMT





:Error Responses:

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

        This is returned when the reversal was attempted and failed for any reason.

        ::

            HTTP/1.1 500 Internal Server Error
            Content-Type: application/json; charset=utf-8

            {
                "ResponseStatus": {
                    "ErrorCode": "DeviceError",
                    "Errors": [
                        {
                            "ErrorCode": "device_error",
                            "Message": "Unable to reverse last transation.
                            Please see log files for further information."
                        }
                    ]
                }
            }



    :Code: 400 Bad Request

        This is returned if the transaction number is not provided or isn't formatted correctly.

        ::

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "transactionNumber",
                            "Message": "Please provide the 4 digit transaction number of the transaction to reverse"
                        }
                    ]
                }
            }


    :Code: 401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT



--------------------------------------------------------------------------------------------------------------


Print Receipt
-------------

Send a request to print a receipt for a recent card transaction that has been made via the terminal. Note that this is only
intended for simple receipts, if you have more specific requirements for a receipt, you can use the Receipt Printer
API to achieve that. You can contact us to get a customised receipt template if required.

:URL: /PrintReceipt

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | |name|     | Yes*     | Name of the merchant. *If not provided, the    | string             |
    |            |          | value is picked from the card terminal. The    |                    |
    |            |          | merchant name must be printed on receipt.      |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |phone|    | No       | Merchant phone number. Optional on receipt.    | string             |
    +------------+----------+------------------------------------------------+--------------------+
    | |address|  | Yes      | Address of the merchant. Must be printed on    | string             |
    |            |          | the receipt.                                   |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |amount|   | Yes      | Transaction amount as decimal. This should be  | decimal or string  |
    |            |          | the amount that was used for the transaction.  |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |trId|     | Yes      | Unique id for the transaction. You can provide | string             |
    |            |          | any ID you wish to help you identify the       |                    |
    |            |          | transaction.                                   |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |resId|    | Yes      | Unique id for the receipt that was returned    | string             |
    |            |          | with the transaction results. Only the last    |                    |
    |            |          | 100 transactions made while the service is     |                    |
    |            |          | running are available to print. If the service |                    |
    |            |          | is restarted then previous receipts will no    |                    |
    |            |          | longer be available. If a receipt is not found |                    |
    |            |          | a 400 response is returned.                    |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |footer|   | No       | Message to be printed at the end of the        | string             |
    |            |          | receipt. If no value is provided, defaults to  |                    |
    |            |          | "Thank You!".                                  |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |tname|    | No       | Change to use a custom template. If not        | string             |
    |            |          | provided, defaults to donation station receipt.|                    |
    +------------+----------+------------------------------------------------+--------------------+


.. |name|  replace:: merchantName
.. |phone|  replace:: merchantPhone
.. |address|  replace:: merchantAddress
.. |amount|  replace:: totalAmount
.. |trId|  replace:: transactionId
.. |resId|  replace:: receiptId
.. |footer|  replace:: footerMessage
.. |tname|  replace:: templateName


:Example:

    ::

        POST http://localhost:8526/DeviceService/GlobalPay/PrintReceipt HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "merchantName": "Kittens Ltd",
            "merchantAddress": "Purr Street 1, London E1 1XX",
            "merchantPhone": "0104 12301123",
            "receiptId": "abcdefgh-fe3a-4410-aa40-b33132d91611",
            "transactionId": "123121323232323",
            "totalAmount": 15.00,
            "footerMessage" : "Thank you for the kittens, they are so fluffy!"
        }




:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `PrintingStatus`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 11:16:04 GMT

            {
              "Description": "SuccessfullySentToPrintQueue",
              "StatusCode": 1
            }




:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "receiptId",
                            "Message": "Couldn't find receipt with ID
                                    db1d540c-f47b-4268-a81d-92c072871a61"
                        }
                    ]
                }
            }


    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "Invalid UserName or Password",
                    "Message": "Invalid UserName or Password"
                }
            }


--------------------------------------------------------------------------------------------------------------


GlobalPay and IPP350 Models
===========================
        

IPP350CardTerminalState
-----------------------

These are the status codes and descriptions returned in a `Status`_ request.

+------------+----------------+-------------------------------------------------------------+
| StatusCode | Description    | Explanation                                                 |
+============+================+=============================================================+
| 0          | Unknown        | Unable to get status of the terminal.                       |
+------------+----------------+-------------------------------------------------------------+
| 1          | Idle           | The terminal is available and ready to process              |
|            |                | transactions.                                               |
+------------+----------------+-------------------------------------------------------------+
| 2          | NotAvailable   | Unable to communicate with the terminal. This may be a      |
|            |                | an error in the configuration or the terminal is not        |
|            |                | connected.                                                  |
+------------+----------------+-------------------------------------------------------------+
| 3          | Busy           | Terminal is already processing a request.                   |
+------------+----------------+-------------------------------------------------------------+




Transaction
-----------

These fields are returned with a Transaction request.

+------------------------+---------------------------------------------------------+--------------------+
| Name                   | Description                                             | Data Type          |
+========================+=========================================================+====================+
| CustomerReceipt        | Details of the transaction as provided by the terminal, | `CustomerReceipt`_ |
|                        | aimed to be printed on customer receipt. May be null    |                    |
|                        | and omitted if transaction didn't complete successfully.|                    |
|                        | See CustomerReceipt for fields. Note that by default    |                    |
|                        | the customer receipt information is not returned for    |                    |
|                        | Contactless payments.                                   |                    |
+------------------------+---------------------------------------------------------+--------------------+
| MerchantReceipt        | Details of the transaction as provided by the terminal, | `MerchantReceipt`_ |
|                        | aimed to be printed on customer receipt. May be null    |                    |
|                        | and omitted if transaction didn't complete successfully.|                    |
|                        | See MerchantReceipt for fields. Note that many fields   |                    |
|                        | may be null for Refund transactions.                    |                    |
+------------------------+---------------------------------------------------------+--------------------+
| `ErrorCode`_           | Code that indicates whether communication with the      | Integer            |
|                        | terminal was successful and what error was encountered. |                    |
|                        | See the `ErrorCode`_ for all the possible values.       |                    |
|                        | Note that this does not indicate whether the            |                    |
|                        | transaction was authorized or declined.                 |                    |
+------------------------+---------------------------------------------------------+--------------------+
| ErrorMessage           | Description of the `ErrorCode`_                         | String             |
+------------------------+---------------------------------------------------------+--------------------+
| ReceiptId              | Unique identifier for the transaction. This can be used | String             |
|                        | when requesting to print a receipt.                     |                    |
+------------------------+---------------------------------------------------------+--------------------+
| SignatureRequired      | A flag indicating if customer signature is required. If | Boolean            |
|                        | this is true, a merchant receipt should be printed with |                    |
|                        | a line for customer to sign. After signature has been   |                    |
|                        | collected, print the customer receipt. If no signature  |                    |
|                        | was provided, reverse the last transaction. Update the  |                    |
|                        | customer receipt to be void and change the CVMResult to |                    |
|                        | "INVALID SIGNATURE" and Message to "VOID" before        |                    |
|                        | printing.                                               |                    |
+------------------------+---------------------------------------------------------+--------------------+


CustomerReceipt
---------------

Customer receipt contains all the information provided by the IPP350 Terminal after a transaction
request has been made. Fields containing strings may be null and omitted if the terminal does not provide the
information. Boolean fields default to false. Note that customer receipt will not be populated for
Contactless transactions, all the transaction details will be included in the `MerchantReceipt`_
instead.

+------------------------+---------------------------------------------------------+-----------------+
| Name                   | Description                                             | Data Type       |
+========================+=========================================================+=================+
| AID                    | Application ID. Required on receipt for ICC/Contactless | String          |
|                        | cards.                                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| Amount                 | Amount. Required on receipt.                            | String          |
+------------------------+---------------------------------------------------------+-----------------+
| ApplicationLabel       | Application label. Required on receipt for              | String          |
|                        | ICC/Contactless cards.                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| CourtesyMessage        | Courtesy message. Optional on receipt.                  | String          |
+------------------------+---------------------------------------------------------+-----------------+
| CVMResult              | Cardholder verification method, for example             | String          |
|                        | PIN VERIFIED or SIGNATURE VERIFIED.                     |                 |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| DataSource             | ie. ICC/KEYED/SWIPE/CONTACTLESS                         | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| DateAndTime            | Time and date for the transaction. DD/MM/YY HH:mm       | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| DiagnosticCode         | Diagnostic Code. Optional on receipt.                   | String          |
+------------------------+---------------------------------------------------------+-----------------+
| FinalMessage1          | Message for customer. Optional on receipt.              | String          |
+------------------------+---------------------------------------------------------+-----------------+
| FinalMessage2          | Message for customer. Optional on receipt.              | String          |
+------------------------+---------------------------------------------------------+-----------------+
| Handset                | Handset. Optional on receipt.                           | String          |
+------------------------+---------------------------------------------------------+-----------------+
| IsDeclined             | Indicates if the transaction was declined by the        | Boolean         |
|                        | carrier. If the transaction was declined, this must be  |                 |
|                        | stated on the receipt.                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| IssuerName             | Card Issuer.                                            | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| IsVoid                 | Indicates if the transaction is void, for example       | Boolean         |
|                        | Cancelled. Required on receipt if the transaction was   |                 |
|                        | void.                                                   |                 |
+------------------------+---------------------------------------------------------+-----------------+
| MerchantID             | The Merchant Account Number. Partly masked.             | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| MerchantName           | Merchant name. Required on receipt but the value does   | String          |
|                        | not have to be provided by the terminal.                |                 |
+------------------------+---------------------------------------------------------+-----------------+
| Message                | ie. AUTH CODE/CANCELLED/DECLINED etc.                   | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| PAN                    | Card number, masked so that only the last four numbers  | String          |
|                        | are shown. Required on receipt.                         |                 |
+------------------------+---------------------------------------------------------+-----------------+
| PanSequenceNumber      | PAN Sequence number. Required on receipt for            | String          |
|                        | ICC/Contactless cards.                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| SCH_ID1                | Optional on receipt.                                    | String          |
+------------------------+---------------------------------------------------------+-----------------+
| SCH_ID2                | Optional on receipt.                                    | String          |
+------------------------+---------------------------------------------------------+-----------------+
| SequenceNumber         | Sequence number. Optional on receipt.                   | String          |
+------------------------+---------------------------------------------------------+-----------------+
| TerminalID             | The Terminal ID. Partly masked.                         | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| TransactionNumber      | Transaction number. Required on receipt.                | String          |
+------------------------+---------------------------------------------------------+-----------------+
| TransactionType        | ie. SALE or REFUND etc. Required on receipt.            | String          |
+------------------------+---------------------------------------------------------+-----------------+


MerchantReceipt
---------------

Merchant receipt contains all the information provided by the IPP350 Terminal after a transaction
request has been made. Fields containing strings may be null and omitted if the terminal does not provide the
information. Boolean fields default to false.

+------------------------+---------------------------------------------------------+-----------------+
| Name                   | Description                                             | Data Type       |
+========================+=========================================================+=================+
| AID                    | Application ID. Required on receipt for ICC/Contactless | String          |
|                        | cards.                                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| Amount                 | Amount. Required on receipt.                            | String          |
+------------------------+---------------------------------------------------------+-----------------+
| ApplicationLabel       | Application label. Required on receipt for              | String          |
|                        | ICC/Contactless cards.                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| CVMResult              | Cardholder verification method, for example             | String          |
|                        | PIN VERIFIED or SIGNATURE VERIFIED.                     |                 |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| DataSource             | ie. ICC/KEYED/SWIPE/CONTACTLESS                         | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| DateAndTime            | Time and date for the transaction. DD/MM/YY HH:mm       | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| DiagnosticCode         | Diagnostic Code. Optional on receipt.                   | String          |
+------------------------+---------------------------------------------------------+-----------------+
| ExpiryDate             | Card Expiry date. Required on receipt.                  | String          |
+------------------------+---------------------------------------------------------+-----------------+
| FinalMessage1          | Message for customer. Optional on receipt.              | String          |
+------------------------+---------------------------------------------------------+-----------------+
| FinalMessage2          | Message for customer. Optional on receipt.              | String          |
+------------------------+---------------------------------------------------------+-----------------+
| Handset                | Handset. Optional on receipt.                           | String          |
+------------------------+---------------------------------------------------------+-----------------+
| IsDeclined             | Indicates if the transaction was declined by the        | Boolean         |
|                        | carrier. If the transaction was declined, this must be  |                 |
|                        | stated on the receipt.                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| IssuerName             | Card Issuer.                                            | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| IsVoid                 | Indicates if the transaction is void, for example       | Boolean         |
|                        | Cancelled. Required on receipt if the transaction was   |                 |
|                        | void.                                                   |                 |
+------------------------+---------------------------------------------------------+-----------------+
| MerchantID             | The Merchant Account Number.                            | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| MerchantName           | Merchant name. Required on receipt but the value does   | String          |
|                        | not have to be provided by the terminal.                |                 |
+------------------------+---------------------------------------------------------+-----------------+
| Message                | ie. AUTH CODE/CANCELLED/DECLINED etc.                   | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| PAN                    | Card number, masked so that only the last four numbers  | String          |
|                        | are shown. Required on receipt.                         |                 |
+------------------------+---------------------------------------------------------+-----------------+
| PanSequenceNumber      | PAN Sequence number. Required on receipt for            | String          |
|                        | ICC/Contactless cards.                                  |                 |
+------------------------+---------------------------------------------------------+-----------------+
| SCH_ID1                | Optional on receipt.                                    | String          |
+------------------------+---------------------------------------------------------+-----------------+
| SCH_ID2                | Optional on receipt.                                    | String          |
+------------------------+---------------------------------------------------------+-----------------+
| SequenceNumber         | Sequence number. Optional on receipt.                   | String          |
+------------------------+---------------------------------------------------------+-----------------+
| StartDate              | Card Start date. Required on receipt.                   | String          |
+------------------------+---------------------------------------------------------+-----------------+
| TerminalID             | The Terminal ID.                                        | String          |
|                        | Required on receipt.                                    |                 |
+------------------------+---------------------------------------------------------+-----------------+
| TransactionNumber      | Transaction number. Required on receipt.                | String          |
+------------------------+---------------------------------------------------------+-----------------+
| TransactionType        | ie. SALE or REFUND etc. Required on receipt.            | String          |
+------------------------+---------------------------------------------------------+-----------------+


ErrorCode
----------

These are the error codes and messages returned in a `Transaction`_.

+------------+-----------------------+-----------------------------------------------------------+
| ErrorCode  | ErrorMessage          | Description                                               |
+============+=======================+===========================================================+
| 0          | NoError               | Successful communication with the terminal. Note that the |
|            |                       | transaction itself may not have been successful.          |
+------------+-----------------------+-----------------------------------------------------------+
| 1          | TerminalBusy          | The terminal is already processing a request.             |
+------------+-----------------------+-----------------------------------------------------------+
| 2          | HardwareError         | Error communicating with the terminal, for example if the |
|            |                       | terminal is not connected or the port is not correct.     |
+------------+-----------------------+-----------------------------------------------------------+
| 3          | InternalError         | Internal error while dealing with the request.            |
+------------+-----------------------+-----------------------------------------------------------+
| 4          | CancelledBeforeCardIn | Customer clicks cancel on terminal before entering card   |
|            |                       | or transaction times out when card was never inserted.    |
+------------+-----------------------+-----------------------------------------------------------+
| 5          | ReceiptNotAvailable   | Receipt cannot be provided.                               |
+------------+-----------------------+-----------------------------------------------------------+


PrintingStatus
--------------

These are the error codes and messages returned for a `Print Receipt`_ request.

+------------+------------------------------+-------------------------------------------+
| StatusCode | Description                  | Explanation                               |
+============+==============================+===========================================+
| 0          | NoPrintQueue                 | Unable to send receipt to be printed      |
|            |                              | because printer is not available.         |
+------------+------------------------------+-------------------------------------------+
| 1          | SuccessfullySentToPrintQueue | Receipt created successfully and sent     |
|            |                              | to print queue. We cannot guarantee the   |
|            |                              | printing finishes successfully, but a     |
|            |                              | printer was found and the receipt was     |
|            |                              | added to the queue.                       |
+------------+------------------------------+-------------------------------------------+
| 2          | ErrorCreatingReceipt         | Internal error while creating the receipt.|
|            |                              | This may indicate improper parameters     |
|            |                              | were sent with the request or that the    |
|            |                              | receipt template is not configured        |
|            |                              | correctly.                                |
+------------+------------------------------+-------------------------------------------+

--------------------------------------------------------------------------------------------------------------

GlobalPay IPP350 Example Transaction JSON
=========================================

Here are some examples of the JSON returned by a transaction request for different kinds of transaction results.

:Chip and Pin:

    ::

        {
          "Transaction": {
            "CustomerReceipt": {
              "AID": "AID: A0000000031010",
              "Amount": "AMOUNT             £2.00",
              "ApplicationLabel": "Visa Debit Test ",
              "CVMResult": "PIN VERIFIED",
              "CourtesyMessage": "Thank You",
              "DataSource": "ICC",
              "DateAndTime": "13/03/17 15:06",
              "DiagnosticCode": "DIAG 73",
              "FinalMessage1": "     CUSTOMER COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": false,
              "IsVoid": false,
              "IssuerName": "VISA DEBIT",
              "MerchantID": "M:***41234",
              "MerchantName": "Global Payments         ",
              "Message": "AUTH CODE:003608",
              "PAN": "************5234",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SCH_ID1": "SCH ID:00000000025442144",
              "SCH_ID2": "45",
              "SequenceNumber": "321",
              "TerminalID": "TID:****1279        S",
              "TransactionNumber": "TXN 0204",
              "TransactionType": "SALE"
            },
            "ErrorCode": 0,
            "ErrorMessage": "NoError",
            "SignatureRequired": false,
            "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61",
            "MerchantReceipt": {
              "AID": "AID: A0000000031010",
              "Amount": "AMOUNT             £2.00",
              "ApplicationLabel": "Visa Debit Test ",
              "CVMResult": "PIN VERIFIED",
              "DataSource": "ICC",
              "DateAndTime": "13/03/17 15:06",
              "DiagnosticCode": "DIAG 73",
              "ExpiryDate": "EXP 12/20",
              "FinalMessage1": "     MERCHANT COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": false,
              "IsVoid": false,
              "IssuerName": "VISA DEBIT",
              "MerchantID": "M:12341234",
              "MerchantName": "Global Payments         ",
              "Message": "AUTH CODE:003608",
              "PAN": "************5234",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SCH_ID1": "SCH ID:00000000025442144",
              "SCH_ID2": "45",
              "SequenceNumber": "321",
              "StartDate": "STT 11/06",
              "TerminalID": "TID:22161279        S",
              "TransactionNumber": "TXN 0204",
              "TransactionType": "SALE"
            }
          }
        }


:Swiped:

    ::

        {
            "Transaction": {
                "MerchantReceipt": {
                    "MerchantName": "Global Payments         ",
                    "MerchantID": "M:12341234",
                    "TerminalID": "TID:22161279        ",
                    "SequenceNumber": "S317",
                    "SCH_ID1": "SCH ID:HNR0004960426  ",
                    "SCH_ID2": null,
                    "Handset": "HANDSET:1",
                    "IssuerName": "DEBIT MASTERCARD",
                    "AID": null,
                    "ApplicationLabel": null,
                    "PAN": "************0206",
                    "ExpiryDate": "EXP 12/20",
                    "PanSequenceNumber": null,
                    "StartDate": null,
                    "DataSource": "SWIPED",
                    "TransactionType": "SALE",
                    "Amount": "AMOUNT             £1.00",
                    "CVMResult": null,
                    "DateAndTime": "26/04/19 14:22",
                    "Message": "AUTH CODE:000435",
                    "TransactionNumber": "TXN 0280",
                    "DiagnosticCode": null,
                    "FinalMessage1": "     MERCHANT COPY      ",
                    "FinalMessage2": " PLEASE RETAIN RECEIPT",
                    "IsVoid": false,
                    "IsDeclined": false
                },
                "CustomerReceipt": {
                    "MerchantName": "Global Payments         ",
                    "MerchantID": "M:***41234",
                    "TerminalID": "TID:****1279        ",
                    "SequenceNumber": "S317",
                    "SCH_ID1": "SCH ID:HNR0004960426  ",
                    "SCH_ID2": null,
                    "Handset": "HANDSET:1",
                    "IssuerName": "DEBIT MASTERCARD",
                    "AID": null,
                    "ApplicationLabel": null,
                    "PAN": "************0206",
                    "PanSequenceNumber": null,
                    "DataSource": "SWIPED",
                    "TransactionType": "SALE",
                    "Amount": "AMOUNT             £1.00",
                    "CVMResult": "SIGNATURE VERIFIED",
                    "CourtesyMessage": "Thank You",
                    "DateAndTime": "26/04/19 14:22",
                    "Message": "AUTH CODE:000435",
                    "TransactionNumber": null,
                    "DiagnosticCode": null,
                    "FinalMessage1": "     CUSTOMER COPY      ",
                    "FinalMessage2": " PLEASE RETAIN RECEIPT",
                    "IsVoid": false,
                    "IsDeclined": false
                },
                "ErrorCode": 0,
                "ErrorMessage": "NoError",
                "ReceiptId": "753c0dc9-4d2a-4640-aa93-1188cd444ca6",
                "SignatureRequired": true
            }
        }

:Declined:

    ::

        {
          "Transaction": {
            "CustomerReceipt": {
              "AID": "AID: A0000000031010",
              "Amount": "AMOUNT           £147.77",
              "ApplicationLabel": "Visa Debit Test ",
              "CourtesyMessage": "Thank You",
              "DataSource": "ICC",
              "DateAndTime": "13/03/17 15:10",
              "DiagnosticCode": "DIAG 1073",
              "FinalMessage1": "     CUSTOMER COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": true,
              "IsVoid": false,
              "IssuerName": "VISA DEBIT",
              "MerchantID": "M:***41234",
              "MerchantName": "Global Payments         ",
              "Message": "DECLINE",
              "PAN": "************5234",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SCH_ID1": "SCH ID:00000000025442244",
              "SCH_ID2": "46",
              "SequenceNumber": "322",
              "TerminalID": "TID:****1279        S",
              "TransactionNumber": "TXN 0205",
              "TransactionType": "SALE"
            },
            "ErrorCode": 0,
            "ErrorMessage": "NoError",
            "SignatureRequired": false,
            "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61",
            "MerchantReceipt": {
              "AID": "AID: A0000000031010",
              "Amount": "AMOUNT           £147.77",
              "ApplicationLabel": "Visa Debit Test ",
              "DataSource": "ICC",
              "DateAndTime": "13/03/17 15:10",
              "DiagnosticCode": "DIAG 1073",
              "ExpiryDate": "EXP 12/20",
              "FinalMessage1": "     MERCHANT COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": true,
              "IsVoid": false,
              "IssuerName": "VISA DEBIT",
              "MerchantID": "M:12341234",
              "MerchantName": "Global Payments         ",
              "Message": "DECLINE",
              "PAN": "************5234",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SCH_ID1": "SCH ID:00000000025442244",
              "SCH_ID2": "46",
              "SequenceNumber": "322",
              "StartDate": "STT 11/06",
              "TerminalID": "TID:22161279        S",
              "TransactionNumber": "TXN 0205",
              "TransactionType": "SALE"
            }
          }
        }


:Contactless:

    ::

        {
          "Transaction": {
            "CustomerReceipt": {
                "IsVoid": false,
                "IsDeclined": false
            },
            "ErrorCode": 0,
            "ErrorMessage": "NoError",
            "SignatureRequired": false,
            "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61",
            "MerchantReceipt": {
              "AID": "AID: A0000000041010",
              "Amount": "AMOUNT             £0.20",
              "ApplicationLabel": "MASTERCARD",
              "DataSource": "CONTACTLESS",
              "DateAndTime": "13/03/17 16:56",
              "DiagnosticCode": "DIAG 1073",
              "ExpiryDate": "EXP 02/20",
              "FinalMessage1": "     MERCHANT COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": false,
              "IsVoid": false,
              "IssuerName": "MASTERCARD",
              "MerchantID": "M:12341234",
              "MerchantName": "Global Payments         ",
              "Message": "AUTH CODE:001008",
              "PAN": "************6451",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SCH_ID1": "SCH ID:HNR0057660313  ",
              "SequenceNumber": "327",
              "StartDate": "STT 17/02",
              "TerminalID": "TID:22161279        S",
              "TransactionNumber": "TXN 0209",
              "TransactionType": "SALE"
            }
          }
        }


:Void:

    ::

        {
          "Transaction": {
            "CustomerReceipt": {
              "AID": "AID: A0000000031010",
              "Amount": "AMOUNT           £147.77",
              "ApplicationLabel": "Visa Debit Test ",
              "CourtesyMessage": "Thank You",
              "DataSource": "ICC",
              "DateAndTime": "13/03/17 15:12",
              "FinalMessage1": "     CUSTOMER COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": false,
              "IsVoid": true,
              "IssuerName": "VISA DEBIT",
              "MerchantID": "M:***41234",
              "MerchantName": "Global Payments         ",
              "PAN": "************5234",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SequenceNumber": "323",
              "TerminalID": "TID:****1279        S",
              "TransactionType": "SALE"
            },
            "ErrorCode": 0,
            "ErrorMessage": "NoError",
            "SignatureRequired": false,
            "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61",
            "MerchantReceipt": {
              "AID": "AID: A0000000031010",
              "Amount": "AMOUNT           £147.77",
              "ApplicationLabel": "Visa Debit Test ",
              "DataSource": "ICC",
              "DateAndTime": "13/03/17 15:12",
              "ExpiryDate": "EXP 12/20",
              "FinalMessage1": "     MERCHANT COPY      ",
              "FinalMessage2": " PLEASE RETAIN RECEIPT",
              "Handset": "HANDSET:1",
              "IsDeclined": false,
              "IsVoid": true,
              "IssuerName": "VISA DEBIT",
              "MerchantID": "M:12341234",
              "MerchantName": "Global Payments         ",
              "PAN": "************5234",
              "PanSequenceNumber": "PAN SEQ NO. 01",
              "SequenceNumber": "323",
              "StartDate": "STT 11/06",
              "TerminalID": "TID:22161279        S",
              "TransactionType": "SALE"
            }
          }
        }

:Internal Error:

    ::

        {
            "Transaction": {
                "ErrorCode": 3,
                "ErrorMessage": "InternalError",
                "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61"
            }
        }


:Hardware Error:

    ::

        {
            "Transaction": {
                "ErrorCode": 2,
                "ErrorMessage": "HardwareError",
                "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61"
            }
        }


:Cancelled:

    ::

        {
            "Transaction": {
                "ErrorCode": 4,
                "ErrorMessage": "CancelledBeforeCardIn",
                "ReceiptId": "db1d540c-f47b-4268-a81d-92c072871a61"
            }
        }


:Contactless Refund:

    ::

        {
            "Transaction": {
                "MerchantReceipt": {
                    "MerchantName": "Global Payments         ",
                    "MerchantID": "M:12341234",
                    "TerminalID": "TID:22161279        ",
                    "SequenceNumber": "S207",
                    "SCH_ID1": null,
                    "SCH_ID2": null,
                    "Handset": "HANDSET:1",
                    "IssuerName": "VISA CREDIT",
                    "AID": "AID: A0000000031010",
                    "ApplicationLabel": "Payworks VISA",
                    "PAN": "************0119",
                    "ExpiryDate": "EXP 12/25",
                    "PanSequenceNumber": null,
                    "StartDate": null,
                    "DataSource": "VISA CONTACTLESS",
                    "TransactionType": "REFUND",
                    "Amount": "AMOUNT             #1.00",
                    "CVMResult": "NO CARDHOLDER VERIFICATION",
                    "DateAndTime": "23/04/19 14:57",
                    "Message": "REFUND ACCEPTED",
                    "TransactionNumber": "TXN 0197",
                    "DiagnosticCode": null,
                    "FinalMessage1": "     MERCHANT COPY      ",
                    "FinalMessage2": " PLEASE RETAIN RECEIPT",
                    "IsVoid": false,
                    "IsDeclined": false
                },
                "CustomerReceipt": {
                    "MerchantName": "",
                    "MerchantID": "",
                    "TerminalID": "",
                    "SequenceNumber": "",
                    "SCH_ID1": null,
                    "SCH_ID2": null,
                    "Handset": "",
                    "IssuerName": "",
                    "AID": "",
                    "ApplicationLabel": "",
                    "PAN": "",
                    "PanSequenceNumber": "",
                    "DataSource": "",
                    "TransactionType": "",
                    "Amount": "",
                    "CVMResult": "",
                    "CourtesyMessage": "",
                    "DateAndTime": "",
                    "Message": "",
                    "TransactionNumber": "",
                    "DiagnosticCode": "",
                    "FinalMessage1": null,
                    "FinalMessage2": null,
                    "IsVoid": false,
                    "IsDeclined": false
                },
                "ErrorCode": 0,
                "ErrorMessage": "NoError",
                "ReceiptId": "e3f60bc6-ec76-4f29-9d98-f6982b4364d8",
                "SignatureRequired": false
            }
        }


:Swiped Refund:

    ::

        {
            "Transaction": {
                "MerchantReceipt": {
                    "MerchantName": "Global Payments         ",
                    "MerchantID": "M:12341234",
                    "TerminalID": "TID:22161279        ",
                    "SequenceNumber": "S316",
                    "SCH_ID1": null,
                    "SCH_ID2": null,
                    "Handset": "HANDSET:1",
                    "IssuerName": "DEBIT MASTERCARD",
                    "AID": null,
                    "ApplicationLabel": null,
                    "PAN": "************0206",
                    "ExpiryDate": "EXP 12/20",
                    "PanSequenceNumber": null,
                    "StartDate": null,
                    "DataSource": "SWIPED",
                    "TransactionType": "REFUND",
                    "Amount": "AMOUNT             £1.00",
                    "CVMResult": null,
                    "DateAndTime": "26/04/19 14:21",
                    "Message": "AUTH CODE:360919",
                    "TransactionNumber": "TXN 0279",
                    "DiagnosticCode": null,
                    "FinalMessage1": "     MERCHANT COPY      ",
                    "FinalMessage2": " PLEASE RETAIN RECEIPT",
                    "IsVoid": false,
                    "IsDeclined": false
                },
                "CustomerReceipt": {
                    "MerchantName": "Global Payments         ",
                    "MerchantID": "M:***41234",
                    "TerminalID": "TID:****1279        ",
                    "SequenceNumber": "S316",
                    "SCH_ID1": null,
                    "SCH_ID2": null,
                    "Handset": "HANDSET:1",
                    "IssuerName": "DEBIT MASTERCARD",
                    "AID": null,
                    "ApplicationLabel": null,
                    "PAN": "************0206",
                    "PanSequenceNumber": null,
                    "DataSource": "SWIPED",
                    "TransactionType": "REFUND",
                    "Amount": "AMOUNT             £1.00",
                    "CVMResult": "SIGNATURE VERIFIED",
                    "CourtesyMessage": "Thank You",
                    "DateAndTime": "26/04/19 14:21",
                    "Message": "AUTH CODE:360919",
                    "TransactionNumber": null,
                    "DiagnosticCode": null,
                    "FinalMessage1": "     CUSTOMER COPY      ",
                    "FinalMessage2": " PLEASE RETAIN RECEIPT",
                    "IsVoid": false,
                    "IsDeclined": false
                },
                "ErrorCode": 0,
                "ErrorMessage": "NoError",
                "ReceiptId": "86a57be7-e621-4548-a9c5-27dc217b5f83",
                "SignatureRequired": true
            }
        }
