========================
Mei SC Note Acceptor API
========================

This endpoint allows accepting notes using Mei SC note acceptor and monitoring its status.

Url
---
The base url is http://localhost:8526/DeviceService/NoteAcceptor/MeiSC.

Get Status
----------
Get the current status of the Mei SC note acceptor. The states also contains information of notes inserted since when
you last time checked the status. It also has the information of escrowed note. It's recommended to check status before
you enable the note acceptor to make sure it doesn't have any notes that haven't been dealt with and that it isn't in a fatal state.
It is also recommended to check status after you disable the note acceptor to make sure it's successfully disabled and all notes received have been dealt with.


:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/NoteAcceptor/MeiSC/Status HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Acceptor Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "Connectivity": "Connected",
            "HardwareState": "Enabled",
            "EscrowedBill": {
                "WhenInserted": "2018-03-07T10:15:41.6316841Z",
                "Value": 5,
                "Currency": "GBP"
            },
            "NotesReceivedSinceLastCheck": [
                {
                    "WhenInserted": "2018-03-07T10:15:38.0089773Z",
                    "Value": 10,
                    "Currency": "GBP"
                }
            ]
        }

:Error Responses:
    There isn't any error response for this service.


Enable the Note Acceptor
------------------------
Enable the note acceptor to start taking notes. If the note acceptor is disabled, it will reject all the notes.
It's recommended to call `Get Status`_
every 500 - 1000ms after the acceptor is enabled so you can get inserted notes info in time.

:URL: /Enable
:Method: POST
:URL Params:  none
:Data Params:

    +-----------------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | Name            | Required | Description                                                                                           | Data Type                 |
    +=================+==========+=======================================================================================================+===========================+
    | auto_stack      |  false   | Indicates if  the device should stack or escrow a note when a note is accepted.                       | Boolean (default to true) |
    |                 |          | When it's true, the device will stack the note immediately, otherwise the note will be kept escrowed  |                           |
    |                 |          | until you call `Return Escrowed Note`_ or `Stack Escrowed Note`_                                      |                           |
    +-----------------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+


:Example:

    ::

        POST http://localhost:8526/DeviceService/NoteAcceptor/MeiSC/Enable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the note acceptor when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.
    Please notice that `Get Status`_ request will not return the error and will not cause other requests
    to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }



    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the acceptor has encountered an issue. You can get more details by sending
    a `Get Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which is in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The note acceptor has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Disable the Note Acceptor
-------------------------
Disable the note acceptor to stop taking notes. If the note acceptor is disabled, it will reject all the notes.
It is recommended to perform a `Get Status`_ request after successfully disabling the note acceptor to ensure there
are no notes inserted and not dealt with. If you disable the acceptor while there is a note escrowed, then the escrowed
note will be returned to user.

:URL: /Disable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/NoteAcceptor/MeiSC/Disable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the note acceptor when the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please notice that `Get Status`_ request will
    not return the error and will not cause other requests to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the note acceptor has encountered an issue. You can get more details by sending a `Get Status`_ request.
    The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which is in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The note acceptor has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

Return Escrowed Note
--------------------
Return the escrowed note. After you send the request, the escrowed note will be returned to user.
This request is usually used when a note is in escrow and you do not want to stack the note.
If there is no escrowed note, an error will be returned.

:URL: /ReturnEscrow
:Method: POST
:URL Params:  none
:Data Params: none

:Example:
    ::

        POST http://localhost:8526/DeviceService/NoteAcceptor/MeiSC/ReturnEscrow HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

            This is returned if you are trying to return an escrowed note while the device is busy. This usually happens if
            you send a new request while device is processing the previous request.
            Please note that `Get Status`_ request will not return the error and will not cause other requests
            to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: No Escrow Error

            This is returned if you are trying to return when there isn't a note in escrow.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Errors": [
                    {
                        "ErrorCode": "no_escrow",
                        "Message": "There isn't an escrowed note.
                         Please check state to find out more"
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

            This is returned when the device has encountered an issue and therefore cannot return the note. You can get
            more details by sending a `Get Status`_ request. The issue is usually hardware related, for example the device has motor error.
            If you need to troubleshoot the issue, you can also check the log files in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Device is unavailable to return note.
                        Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

Stack Escrowed Note
-------------------
Stack the escrowed note. After you send the request, the escrowed note will be stacked to cash box and it will no longer be possible
to return it to the user. If there is no escrowed note, an error will be returned.

:URL: /StackEscrow
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/NoteAcceptor/MeiSC/StackEscrow HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

        This is returned if you are trying to stack an escrowed note while the device is busy. This usually happens if
        you send a new request while device is processing the previous request.
        Please note that `Get Status`_ request will not return the error and will not cause other requests
        to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: No Escrow Error

        This is returned if you are trying to stack when there is no note in escrow.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Errors": [
                    {
                        "ErrorCode": "no_escrow",
                        "Message": "There isn't an escrowed note.
                        Please check state to find out more"
                    }
                ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

        This is returned when the device has encountered an issue and therefore cannot stack the note. You can get
        more details by sending a `Get Status`_ request. The issue is usually hardware related, for example the device has motor error.
        If you need to troubleshoot the issue, you can also check log files in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Device is unavailable to return note.
                        Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

        This is returned if the username or password are incorrect or have not been provided.
        The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

Note Acceptor API Models
------------------------

Acceptor Status
===============

The acceptor status is a JSON object returned with a `Get Status`_ request. It has four fields:
Connectivity, HardwareState, EscrowedBill and NotesReceivedSinceLastCheck.

:Example:

    ::

        {
            "Connectivity": "Connected",
            "HardwareState": "Enabled",
            "EscrowedBill": {
                "WhenInserted": "2018-03-07T10:15:41.6316841Z",
                "Value": 5,
                "Currency": "GBP"
            },
            "NotesReceivedSinceLastCheck": [
                {
                    "WhenInserted": "2018-03-07T10:15:38.0089773Z",
                    "Value": 10,
                    "Currency": "GBP"
                }
            ]
        }


Connectivity
^^^^^^^^^^^^

+--------------------+-------------------------------------------------+
| Value              | Explanation                                     |
+====================+=================================================+
| Disconnected       | Device is not connected                         |
+--------------------+-------------------------------------------------+
| Connected          | Device is connected                             |
+--------------------+-------------------------------------------------+


HardwareState
^^^^^^^^^^^^^

+----------+----------------------------------------------------------------------------------------------------------+
| Value    | Explanation                                                                                              |
+==========+==========================================================================================================+
| Unknown  | State is unknown. This usually happens when the device is unavailable, so the state cannot be retrieved. |
+----------+----------------------------------------------------------------------------------------------------------+
| Enabled  | Device is enabled so it's ready to take note.                                                            |
+----------+----------------------------------------------------------------------------------------------------------+
| Disabled | Device is disabled so it cannot take notes                                                               |
+----------+----------------------------------------------------------------------------------------------------------+
| Error    | Device is in an error state so it cannot take notes.                                                     |
+----------+----------------------------------------------------------------------------------------------------------+

EscrowedBill
^^^^^^^^^^^^

This field shows the escrowed note that is held in the note acceptor. If there isn't any escrowed note, it will be null.
You can either return or stack an escrowed note by `Return Escrowed Note`_ or `Stack Escrowed Note`_ respectively. If
the note is stacked, the note will show in the `NotesReceivedSinceLastCheck`_ list.

NotesReceivedSinceLastCheck
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a list of notes that are received by the acceptor since when you last time check the status by `Get Status`_ .
The list is cleaned up afterwards, so next time when you check the status, you will get a new list.

Each note in the list has its value, currency and timestamp of when it's inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was inserted.       |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+

