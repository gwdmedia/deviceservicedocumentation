=========================
Zebra Barcode Scanner API
=========================

This endpoint allows scanning barcodes using a Zebra barcode scanner and checking if a scanner is connected.
Please note that the scanner drivers must be installed first, and the scanner has to be set to send "No read"
messages. Please refer to the scanner documentation for further information.

Url
---
The base url is http://localhost:8526/DeviceService/ZebraScanner/

Get Status
----------
Get the current connection status of the scanner. You can see if the device is currently connected to the PC, and you
can also see if the device is busy handling another request.


:URL: /Status

:Method: GET

:URL Params:  none

:Data Params: none


:Example:
    ::

        GET http://localhost:8526/DeviceService/ZebraScanner/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

:Code: 200 OK

:Content-Type: application/json

:Content Body: `Scanner Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
          "ScannerConnected": true,
          "Busy": false
        }



:Error Response:

:Code: 401 Unauthorized
    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Scan Barcode
------------
This endpoint activates the scanner, waits for a barcode to be scanned and then returns details of the barcode that was found.
If a barcode is not found before the device times out,
a "No Read" response is returned. Only one barcode can be requested at a time, and you should always wait until a response is
returned as the resulting barcodes is not stored, they are only sent in the response. The timeout for waiting for a barcode to be scanned can be set both on
the device itself and on the device service configuration. Please note that the timeout set in device service should be
longer than the hardware timeout to ensure the scanner does not stay active after returning a "no read" response.

:URL: /ScanBarcode

:Method: GET

:URL Params:  none

:Data Params: none


:Example:

        ::

            GET http://localhost:8526/DeviceService/ZebraScanner/ScanBarcode/ HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
            Content-Type: application/json


:Success Response:

  :Code: 200 OK

  :Content-Type: application/json

  :Content Body: `Scan Barcode Data`_ as barcode

    ::

      HTTP/1.1 200 OK
      Content-Length: 37
      Content-Type: application/json; charset=utf-8
      Server: Microsoft-HTTPAPI/2.0
      Date: Thu, 16 Mar 2017 11:12:53 GMT
        {
            "CodeType": 3,
            "CodeTypeName": "ST_CODE_128",
            "Content": "1234"
        }


:No Read Reasponse:

  :Code: 200 OK

  :Content-Type: application/json

  :Content Body: `Scan Barcode Data`_ as barcode. The CodeType is always 1, and the Content is "NR".

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
        "CodeType": 1,
        "CodeTypeName": "ST_CODE_39",
        "Content": "NR"
        }


:Error Responses:


    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This error response is returned when ScanBarcode request is sent while a previous request is still pending.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "The device is busy."
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This error is returned if a suitable scanner is not connected.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "Barcode could not be scanned. The scanner is not connected."
                    }
                ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



--------------------------------------------------------------------------------------------------------------

Access Scanner API Models
-------------------------

Scanner Status
==============

The scanner status is a JSON object returned with a `Get Status`_ request. It has two boolean fields to describe the status.

+-------------------+-----------------------------------------------------------------------------------------------+
| Field name        | Explanation                                                                                   |
+===================+===============================================================================================+
| ScannerConnected  | Is the scanner connected to scan for documents or not.                                        |
+-------------------+-----------------------------------------------------------------------------------------------+
| Busy              | Is the scanner busy handling a Scan Barcode data request or not.                              |
+-------------------+-----------------------------------------------------------------------------------------------+

Scan Barcode Data
=================

After a barcode is successfully scanned, the following details are returned by the
`Scan Barcode`_ endpoint as the barcode.


+-------------------+------------------------------------------------------------------+
| Field name        | Explanation                                                      |
+===================+==================================================================+
| CodeType          | Numeric representation of code type. Please see the manufacturer |
|                   | documentation for the list of possible values.                   |
+-------------------+------------------------------------------------------------------+
| CodeTypeName      | The type of barcode as a string, for example "ST_CODE_39"        |
+-------------------+------------------------------------------------------------------+
| Content           | Decoded value of the scanned barcode.                            |
+-------------------+------------------------------------------------------------------+



































