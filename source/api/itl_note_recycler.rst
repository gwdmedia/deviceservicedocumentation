=======================================================
ITL NV200 and Smart Payout & ITL Smart Coin System APIs
=======================================================

The ITL SMART Payout and SMART Coin System can be used independently or together sharing the same COM port. They share
their configuration file in the device service settings file NV200SmartPayoutService.config. This can be used to
determine which device(s) are used. There is a separate API for each, please see below for details.

Please note that if both devices are run together, you can still only send commands to one device at a time as they
share the COM port. You may get a busy response if one of the devices is responding to a slow request, so you should
ensure you handle the busy status in your client application.


==============================
ITL NV200 and Smart Payout API
==============================

Url
---
The base url is http://localhost:8526/DeviceService/ITL/NoteRecycler.

Get Status
----------
Get the current status of the NV200 and Smart Payout note recycler. The status also contains information of notes inserted since when
you last time checked the status and any note currently in escrow. It's recommended to check status before
you enable the note recycler to make sure it doesn't have any notes that haven't been dealt with and that it isn't in a fatal state.
It is also recommended to check status after you disable the note recycler to make sure it's successfully disabled and all notes received have been dealt with.


:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/ITL/NoteRecycler/Status HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Recycler Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "CurrentRecyclerState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false,
                "IsCashboxInPlace": true,
                "IsStackerFull": false
            },
            "EscrowedBill": {
                "WhenInserted": "2018-03-07T10:15:41.6316841Z",
                "Value": 5,
                "Currency": "GBP"
            },
            "NotesReceivedSinceLastCheck": [
                {
                    "WhenInserted": "2018-03-07T10:15:38.0089773Z",
                    "Value": 10,
                    "Currency": "GBP"
                }
            ]
        }

:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



    :Code: 500 DeviceError

    This is returned if the device service configuration does not have the device set to be used.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "The note recycler has not been configured to be in use. Please check the device service configuration."
                    }
                ]
            }
        }


Enable the Note Recycler to accept notes
----------------------------------------
Enable the note recycler to start taking notes. If the note recycler is disabled, it will reject all the notes.
It's recommended to call `Get Status`_
every 500 - 1000ms after the recycler is enabled so you can get inserted notes info in time.

If "auto_stack" is set to true, the device will be disabled after accepring and stacking a note. If "auto_stack" is set to false,
the device will accept notes until it has bene disabled (or an error occurs).

:URL: /Enable
:Method: POST
:URL Params:  none
:Data Params:

    +-----------------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | Name            | Required | Description                                                                                           | Data Type                 |
    +=================+==========+=======================================================================================================+===========================+
    | auto_stack      |  false   | Indicates if  the device should stack or escrow a note when a note is accepted.                       | Boolean (default to true) |
    |                 |          | When it's true, the device will stack the note immediately, otherwise the note will be kept escrowed  |                           |
    |                 |          | until you call `Return Escrowed Note`_ or `Stack Escrowed Note`_                                      |                           |
    +-----------------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+


:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NoteRecycler/Enable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

        {
            "auto_stack": false
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the note recycler when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.
    Please notice that `Get Status`_ request will not return the error and will not cause other requests
    to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy.
                        Please wait until other actions have finished."
                    }
                ]
            }
        }



    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the recycler has encountered an issue. You can get more details by sending
    a `Get Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The note recycler has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned if the device service configuration does not have the device set to be used.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "The note recycler has not been configured to be in use.
                        Please check the device service configuration."
                    }
                ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Dispense a Note
---------------
Use this endpoint to dispense one note of the given value to the end user. This will enable the payout device and dispense a note
for the user to collect from the bezel. Errors will be returned if dispensing is not possible for any reason. To reduce the number of
errors, you should call the `Get All Notes`_ beforehand, and only then request notes that you know are available to dispense.
If using the coin acceptor as well, you can also use the `Check Dispensing Amount`_ endpoint to get an array of notes that
can be dispensed to achieve the wanted amount.

You should ensure you do not time out this request until a response has been returned as otherwise you will not know for certain if the
note was dispensed or not.

Please note that the payout will be disabled after this call if dispensing a note is attempted, regardless of the
dispensing being successful or not, or whether the device was enabled prior to making the call.
If you wish to accept notes, you must enable the device again after making this call. Use `Get Status`_ to see if the device is enabled or not,
but note that the device is listed as enabled while the note is dispensed.



:URL: /DispenseNote
:Method: POST
:URL Params:  none
:Data Params:

    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | Name      | Required | Description                                                                                           | Data Type                 |
    +===========+==========+=======================================================================================================+===========================+
    | amount    |  true    | Value of the note to be dispensed. This value must be for a note that the recycler supports and one   | Integer                   |
    |           |          | that is available in the payout.                                                                      |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | currency  |  true    | Three letter currency code for the note to be dispensed, for example "GBP". Must match the data       | Boolean (default to true) |
    |           |          | data channels the device has been set up with.                                                        |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | timestamp |  true    | Current UTC timestamp in  ISO 8601 format, e.g. "2018-10-18T16:49:56Z". Call will be rejected if the  | Datetime as ISO 8601      |
    |           |          | timestamp is too old.                                                                                 | formatted string          |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | signature |  true    | SHA256 hash of the timestamp sent with the request formatted as yyyyMMddHHmmss UTC plus the MD5 hash  | String                    |
    |           |          | of the dispensing password that was provided to you with the set up instructions.                     |                           |
    |           |          | SHA256(yyyyMMddHHmmss + md5(password)). UTF8 encoding is used for the hashes.                         |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+


:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NoteRecycler/dispenseNote HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

        {
            "amount": 10,
            "currency": "GBP",
            "timestamp": "2018-10-18T16:49:56Z",
            "signature": "bf73a37326b58c3cac9fc3fd3931f6ea547f5b1ab1b6b3567d95c019d04454a5"
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to dispense a note when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions have finished."
                    }
                ]
            }
        }


    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Invalid Argument

    This is returned if the parameters provided are missing or incorrect. See the ErrorCode, FieldName and Message
    for further information. If this response is returned, the dispensing is not attempted so the device will stay enabled
    if it was already enabled when the request was made.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": "invalid_argument",
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "required",
                        "FieldName": "Signature",
                        "Message": "Please provide a valid signature."
                    }
                ]
            }
        }





    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the recycler has encountered an issue while attempting to dispense a note. The device will be
    disabled after this response is returned. This can also be returned if the requested note was not available to payout,
    or if the device has not been configured to be in use.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unable to dispense requested note."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





Disable the Note Recycler
-------------------------
Disable the note recyler to stop accepting notes. If the note recycler is disabled, it will reject all the notes.
It is recommended to perform a `Get Status`_ request after successfully disabling the note recycler to ensure there
are no notes inserted and not dealt with. If you disable the recycler while there is a note in escrow, then the escrowed
note will be returned to user. You should always
request the status after disabling the recycler to ensure all notes are dealt with and to verify the device is disabled.

:URL: /Disable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NoteRecycler/Disable/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the note recycler when the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please notice that `Get Status`_ request will
    not return the error and will not cause other requests to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the recycler has encountered an issue. You can get more details by sending
    a `Get Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The note recycler has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

Get All Notes
-------------
Get a list of all the notes currently available in the Smart Payout. This should be used to check the availability of notes to be dispensed
before calling `Dispense a Note`_.


:URL: /NotesInPayout
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/ITL/NoteRecycler/NotesInPayout HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Notes in Payout`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        [
            {
                "Count": 0,
                "Value": 5,
                "Currency": "GBP"
            },
            {
                "Count": 0,
                "Value": 10,
                "Currency": "GBP"
            },
            {
                "Count": 2,
                "Value": 20,
                "Currency": "GBP"
            },
            {
                "Count": 1,
                "Value": 50,
                "Currency": "GBP"
            }
        ]

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to get the note information while the device is busy. This usually happens if
    you send a new request while device is processing the previous request. You should try again in a moment.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



Return Escrowed Note
--------------------
Return the escrowed note. After you send the request, the escrowed note will be returned to user.
This request is usually used when a note is in escrow and you do not want to stack the note.
If there is no note in escrow, an error will be returned.

:URL: /ReturnEscrow
:Method: POST
:URL Params:  none
:Data Params: none

:Example:
    ::

        POST http://localhost:8526/DeviceService/ITL/NoteRecycler/ReturnEscrow/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

            This is returned if you are trying to return an escrowed note while the device is busy. This usually happens if
            you send a new request while device is processing the previous request.
            Please note that `Get Status`_ request will not return the error and will not cause other requests
            to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: No Escrow Error

            This is returned if you are trying to return when there isn't a note in escrow.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "no_escrow",
                        "Message": "There is no note held in escrow. Please check state to find out more"
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

            This is returned when the device has encountered an issue and therefore cannot return the note. You can get
            more details by sending a `Get Status`_ request. The issue is usually hardware related, for example the device has motor error.
            If you need to troubleshoot the issue, you can also check the log files in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unexpected error. Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

Stack Escrowed Note
-------------------
Stack the note that is currently in escrow. After you send the request, the escrowed note will be stacked to cash box and it will no longer be possible
to return it to the user. If there is no note in escrow, an error will be returned.

:URL: /StackEscrow
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NoteRecycler/StackEscrow/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

        This is returned if you are trying to stack an escrowed note while the device is busy. This usually happens if
        you send a new request while device is processing the previous request.
        Please note that `Get Status`_ request will not return the error and will not cause other requests
        to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: No Escrow Error

            This is returned if you are trying to stack when there is no note in escrow.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "no_escrow",
                        "Message": "There is no note held in escrow. Please check state to find out more"
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

            This is returned when the device has encountered an issue and therefore cannot stack the note. You can get
            more details by sending a `Get Status`_ request. The issue is usually hardware related.
            If you need to troubleshoot the issue, you can also check the log files
            in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unexpected error. Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

        This is returned if the username or password are incorrect or have not been provided.
        The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

Note Recycler API Models
------------------------

Recycler Status
===============

The recycler status is a JSON object returned with a `Get Status`_ request. It has three fields:
CurrentRecyclerState, EscrowedBill and NotesReceivedSinceLastCheck.

:Example:

    ::

        {
            "CurrentRecyclerState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false,
                "IsCashboxInPlace": true,
                "IsStackerFull": false
            },
            "EscrowedBill": {
                "WhenInserted": "2018-03-07T10:15:41.6316841Z",
                "Value": 5,
                "Currency": "GBP"
            },
            "NotesReceivedSinceLastCheck": [
                {
                    "WhenInserted": "2018-03-07T10:15:38.0089773Z",
                    "Value": 10,
                    "Currency": "GBP"
                }
            ]
        }


CurrentRecyclerState
^^^^^^^^^^^^^^^^^^^^

The current recycler state has five boolean values that describe the status of the device.

+--------------------+-------------------------------------------------------------------------------------------------------+
| Field              | Explanation                                                                                           |
+====================+=======================================================================================================+
| IsConnected        | Whether the device is connected or not. Will be false if serial port cannot be opened. Default false. |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsEnabled          | Is the device enabled to accept or dispense notes. Default false.                                     |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsJammed           | Is the device detected to be jammed. Log files will show more details of jams. Default false.         |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsCashboxInPlace   | Is the cash box in place. If not, the device will not accept any notes. Default true.                 |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsStackerFull      | Is the cash box full. If yes, the device will not accept any more notes. Default false.               |
+--------------------+-------------------------------------------------------------------------------------------------------+


EscrowedBill
^^^^^^^^^^^^

This field shows the escrowed note that is held in the note recycler. If there isn't any note in escrow, it will be null.
You can either return or stack an escrowed note by `Return Escrowed Note`_ or `Stack Escrowed Note`_ respectively. If
the note is stacked, the note will show in the `NotesReceivedSinceLastCheck`_ list.

If there is a note in escrow, you can see its value, currency and timestamp of when it was inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was inserted.       |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+

NotesReceivedSinceLastCheck
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a list of notes that are received and accepted by the recycler since when you last time check the status by `Get Status`_ .
The list is cleaned up afterwards, so next time when you check the status, you will get a new list. The notes on this list can
not be returned to the user.

Each note in the list has its value, currency and timestamp of when it's inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was inserted.       |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+


Notes in Payout
===============

The notes in payout is an array of the notes. The number of notes listed here depend on how the recycler has been set up
and which note channels are enabled. Each Note has the following fields:

+--------------+------------------------------------------------------------------+
| Field        | Explanation                                                      |
+==============+==================================================================+
| Count        | How many of this kind of note are there in the payout. May be 0. |
+--------------+------------------------------------------------------------------+
| Value        | The value of the note. For example, 10 means ten pounds in GBP.  |
+--------------+------------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.           |
+--------------+------------------------------------------------------------------+




=========================
ITL SMART Coin System API
=========================

Url
---
The base url is http://localhost:8526/DeviceService/ITL/CoinHopper.

Get Coin Hopper Status
----------------------
Get the current status of the SMART Coin system (Coin Mech and Coin Hopper). The status also contains information of coins inserted since when
you last time checked the status. It's recommended to check status before
you enable the note Coin Hopper to make sure it doesn't have any coins that haven't been dealt with and that it isn't in a fatal state.
It is also recommended to check status after you disable the Coin Hopper to make sure it's successfully disabled and all coins received have been dealt with.


:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/ITL/CoinHopper/Status HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Coin Hopper Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "CurrentHopperState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false
            },
            "CoinsReceivedSinceLastCheck": []
        }

:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



    :Code: 500 DeviceError

    This is returned if the device service configuration does not have the device set to be used.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "The coin hopper has not been configured to be in use.
                         Please check the device service configuration."
                    }
                ]
            }
        }


Enable the Coin Hopper to accept coins
--------------------------------------
Enable the coin hopper to start accepting coins. If the coin hopper is disabled, it will reject all the coins.
It's recommended to call `Get Coin Hopper Status`_
every 500 - 1000ms after the coin hopper is enabled so you can get inserted coins info in time. The hopper will stay
enabled until it is disabled by sending the disable command, or another command that leads to it getting disabled.

:URL: /Enable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NoteRecycler/Enable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the coin hopper when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.
    Please notice that `Get Coin Hopper Status`_ request will not return the error and will not cause other requests
    to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy.
                        Please wait until other actions have finished."
                    }
                ]
            }
        }



    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the hopper has encountered an issue. You can get more details by sending
    a `Get Coin Hopper Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The coin hopper has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned if the device service configuration does not have the device set to be used.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "The coin hopper has not been configured to be in use.
                        Please check the device service configuration."
                    }
                ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Dispense Change
---------------
Use this endpoint to dispense the given amount of change in coins to the end user, or to check if it would be possible to
do it. This will enable the coin device and dispense the coins until the value has been reached.
Errors will be returned if dispensing is not possible for any reason or if only part of the coins was paid out. To reduce the number of
errors, you should test whether the amount is possible to dispense before attempting to dispense. You can so it either by using this endpoint with the "test" parameter set to true,
or by using the `Check Dispensing Amount`_ endpoint to get an array of notes (if note recycler is used) and coin change amount
that can be dispensed to achieve the wanted total amount.

You should ensure you do not time out this request until a response has been returned as otherwise you will not know for
certain if dispensing was sucessful or not.

Please note that the hopper will be disabled after this call if dispensing coins is attempted, regardless of the
dispensing being successful or not, or whether the device was enabled prior to making the call.
If you wish to accept coins, you must enable the device again after making this call. Use `Get Coin Hopper Status`_ to see
if the device is enabled or not.



:URL: /DispenseChange
:Method: POST
:URL Params:  none
:Data Params:

    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | Name      | Required | Description                                                                                           | Data Type                 |
    +===========+==========+=======================================================================================================+===========================+
    | amount    |  true    | Total value of the coins to be dispensed. If more than 2 decimals are provided, they are truncated.   | Decimal                   |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | currency  |  true    | Three letter currency code for the note to be dispensed, for example "GBP". Must match the data       | Boolean (default to true) |
    |           |          | data channels the device has been set up with.                                                        |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | test      |  false   | Boolean to signify if the change should be dispensed. If this is set to true, no money will be        | Boolean (default to       |
    |           |          | dispensed but receiving a successful response means the device should be able to dispense the amount. | false)                    |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | signature |  true    | SHA256 hash of the timestamp sent with the request formatted as yyyyMMddHHmmss UTC plus the MD5 hash  | String                    |
    |           |          | of the dispensing password that was provided to you with the set up instructions.                     |                           |
    |           |          | SHA256(yyyyMMddHHmmss + md5(password)). UTF8 encoding is used for the hashes.                         |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | timestamp |  true    | Current UTC timestamp in  ISO 8601 format, e.g. "2018-10-18T16:49:56Z". Call will be rejected if the  | Datetime as ISO 8601      |
    |           |          | timestamp is too old.                                                                                 | formatted string          |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+


:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/CoinHopper/DispenseChange HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

        {
            "amount": 5.56,
            "currency": "GBP",
            "timestamp": "2018-10-18T16:49:56Z",
            "signature": "bf73a37326b58c3cac9fc3fd3931f6ea547f5b1ab1b6b3567d95c019d04454a5",
            "test": false
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error - Partial Payout

    This is returned when the hopper started to dispense the change but for some reason was unable to dispense the full amount.
    This should not happen regularly if the devcie settings are correct, but it is possible the hopper does not find the coin it thought
    was available and so the payout fails. The amount already dispensed is listed in the Message of the DeviceError, for
    example in the response below, the amount dispensed was 1.30.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "partial_payout",
                        "Message": "1.30"
                    }
                ]
            }
        }


    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to dispense change when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions have finished."
                    }
                ]
            }
        }


    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Invalid Argument

    This is returned if the parameters provided are missing or incorrect. See the ErrorCode, FieldName and Message
    for further information. If this response is returned, the dispensing is not attempted so the device will stay enabled
    if it was already enabled when the request was made.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": "invalid_argument",
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "required",
                        "FieldName": "Signature",
                        "Message": "Please provide a valid signature."
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the hopper has encountered an issue while attempting to dispense the requested amount,
    or if the amount is not available to be dispensed. The device will be
    disabled after this response is returned. This can also be returned if the device has not been configured to be in use.
    See the "Message" field for further information.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unable to dispense requested amount of coins."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





Disable the Coin Hopper
-----------------------
Disable the Coin Hopper to stop accepting coins. If the Coin Hopper is disabled, it will reject all the inserted coins.
It is recommended to perform a `Get Coin Hopper Status`_ request after successfully disabling the Coin Hopper to ensure there
are no coins inserted and not dealt with.

:URL: /Disable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/CoinHopper/Disable/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the Coin Hopper when the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please notice that `Get Coin Hopper Status`_ request will
    not return the error and will not cause other requests to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the recycler has encountered an issue. You can get more details by sending
    a `Get Coin Hopper Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The coin hopper has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Get All Coins
-------------
Get a list of all the coins currently available in the Coin Hopper. This can be used to check the availability of coins to be dispensed
before calling `Dispense Change`_.


:URL: /CoinsInHopper
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/ITL/CoinHopper/CoinsInHopper HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Coins In Hopper`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        [
            {
                "Count": 2,
                "Value": 0.01,
                "Currency": "GBP"
            },
            {
                "Count": 3,
                "Value": 0.02,
                "Currency": "GBP"
            },
            {
                "Count": 3,
                "Value": 0.05,
                "Currency": "GBP"
            },
            {
                "Count": 4,
                "Value": 0.1,
                "Currency": "GBP"
            },
            {
                "Count": 3,
                "Value": 0.2,
                "Currency": "GBP"
            },
            {
                "Count": 7,
                "Value": 0.5,
                "Currency": "GBP"
            },
            {
                "Count": 1,
                "Value": 1,
                "Currency": "GBP"
            },
            {
                "Count": 0,
                "Value": 2,
                "Currency": "GBP"
            }
        ]

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to get the coin information while the device is busy. This usually happens if
    you send a new request while device is processing the previous request. You should try again in a moment.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



Check Dispensing Amount
-----------------------
This endpoint can be used to check if the amount provided is possible to dispense from the coin hopper and/or the note
acceptor. You will also receive the split of coins total to dispense, and an array of notes to dispense in order to
achieve the total amount.

Please note that the coin hopper must be enabled in the device service configuration in order to use this endpoint.
The note acceptor is optional, if it is not configured to be in use then only coin availability is checked.


:URL: /CheckDispensingAmount
:Method: POST
:URL Params:  none
:Data Params:

    +-----------+----------+-------------------------------------------------------------------------------------------------+---------------------------+
    | Name      | Required | Description                                                                                     | Data Type                 |
    +===========+==========+=================================================================================================+===========================+
    | amount    |  true    | Total amount you wish to check if it is available to be dispensed. If more than 2 decimals are  | Decimal                   |
    |           |          | provided, they are truncated.                                                                   |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------+---------------------------+
    | currency  |  true    | Three letter currency code for the note to be dispensed, for example "GBP". Must match the data | String                    |
    |           |          | data channels the device has been set up with.                                                  |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------+---------------------------+

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/CoinHopper/CheckDispensingAmount/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

        {
          "amount":38.51,
          "currency": "GBP"
        }

:Success Response When Amount Is Available:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Check Dispensing Amount Response`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "AmountPayable": true,
            "CoinTotal": 3.51,
            "NoteTotal": 35,
            "NoteValueList": [
                20,
                10,
                5
            ]
        }

    :Success Response When Amount Is Not Available:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Check Dispensing Amount Response`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "AmountPayable": false,
            "CoinTotal": 0,
            "NoteTotal": 0,
            "NoteValueList": null
        }



:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

        This is returned if you are trying to call this endpoint while either note recycler or coin hopper is busy. This usually happens if
        you send a new request while either device is processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

        This is returned when the device has encountered an issue and therefore cannot calculate if the amount is available. You can get
        more details by sending a `Get Status`_ and `Get Coin Hopper Status`_ request. The issue is usually hardware related.
        If you need to troubleshoot the issue, you can also check the log files
        in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unexpected error. Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

        This is returned if the username or password are incorrect or have not been provided.
        The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

Coin Hopper API Models
----------------------

Coin Hopper Status
==================

The hopper status is a JSON object returned with a `Get Coin Hopper Status`_ request. It has two fields:
CurrentHopperState and CoinsReceivedSinceLastCheck.

:Example:

    ::

        {
            "CurrentHopperState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false
            },
            "CoinsReceivedSinceLastCheck": [
                {
                "WhenInserted": "2018-03-07T10:15:38.0089773Z",
                "Value": 1,
                "Currency": "GBP"
                }]
        }



CurrentHopperState
^^^^^^^^^^^^^^^^^^^^

The current hopper state has five boolean values that describe the status of the device.

+--------------------+-------------------------------------------------------------------------------------------------------+
| Field              | Explanation                                                                                           |
+====================+=======================================================================================================+
| IsConnected        | Whether the device is connected or not. Will be false if serial port cannot be opened. Default false. |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsEnabled          | Is the device enabled to accept or dispense coins. Default false.                                     |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsJammed           | Is the device detected to be jammed. Log files will show more details of jams. Default false.         |
+--------------------+-------------------------------------------------------------------------------------------------------+


CoinsReceivedSinceLastCheck
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a list of notes that are received and accepted by the recycler since when you last time check the status by `Get Coin Hopper Status`_ .
The list is cleaned up afterwards, so next time when you check the status, you will get a new list. The notes on this list can
not be returned to the user.

Each note in the list has its value, currency and timestamp of when it's inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was inserted.       |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+


Coins in Hopper
===============

The coins in hopper is an array of the coins that are in the hopper and can be dispensed out.
The number of coins listed here depend on how the hopper has been set up
and which coin channels are enabled. Each Coin has the following fields:

+--------------+------------------------------------------------------------------+
| Field        | Explanation                                                      |
+==============+==================================================================+
| Count        | How many of this kind of coin are there in the payout. May be 0. |
+--------------+------------------------------------------------------------------+
| Value        | The value of the coin. For example, 2 means two pounds in GBP.   |
+--------------+------------------------------------------------------------------+
| Currency     | The currency code of the coin, for example GBP or EUR.           |
+--------------+------------------------------------------------------------------+


Check Dispensing Amount Response
================================

The Check Dispensing Amount Response is a JSON object returned with a `Check Dispensing Amount`_ request. It has four fields:
AmountPayable, CoinTotal, NoteTotal and NoteValueList.

:Example:

    ::

        {
            "AmountPayable": true,
            "CoinTotal": 3.51,
            "NoteTotal": 35,
            "NoteValueList": [
                20,
                10,
                5
            ]
        }



+--------------------+------------------------------------------------------------------------------------------------------------+
| Field              | Explanation                                                                                                |
+====================+============================================================================================================+
| AmountPayable      | Boolean that tells whether it is possible to pay out the requested amount or not.                          |
+--------------------+------------------------------------------------------------------------------------------------------------+
| CoinTotal          | Total amount of coins to dispense out. The Hopper will have this amount available to dispense. May be 0.   |
+--------------------+------------------------------------------------------------------------------------------------------------+
| NoteTotal          | Total amount of notes to dispense out. The recycler will have this amount available to dispense. May be 0. |
+--------------------+------------------------------------------------------------------------------------------------------------+
| NoteValueList      | An array of integer values for notes to dispense in order to achieve the NoteTotal. The recycler has these |
|                    | available to dispense. May be null or empty, but will have at least one value if NoteTotal > 0.            |
+--------------------+------------------------------------------------------------------------------------------------------------+