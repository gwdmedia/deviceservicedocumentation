============================
LED Controller I/O Board API
============================

This endpoint allows writing command strings to an LED Controller I/O board connected via serial.
Communication is only from the PC to the I/O board, any messages sent by the control board are not read.

Url
---
The base url is http://localhost:8526/DeviceService/LEDController/.

Get Status
----------
Get the current connection status of the board.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/LEDController/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "status": "SerialConnected"
        }

:Error Responses:
    There isn't any error response for this service.

Send Command
------------
Send a command string to the LED controller board via serial. Note that the command is not validated by the API, it is passed on to
the serial port as it is. The API cannot verify whether the board does anything with the data sent to it, a successful
result is returned as long as the data was written to the serial port. If the data cannot be sent, an error will be returned.

:URL: /SendCommand
:Method: POST
:URL Params:  none
:Data Params:

    +------------+----------+-------------------------------------------------+--------------------+
    | Name       | Required | Description                                     | Data Type          |
    +============+==========+=================================================+====================+
    | Command    | Yes      | The command string to be written to the serial  | string             |
    |            |          | port. The I/O board manufacturer should confirm |                    |
    |            |          | the acceptable commands to send, the string is  |                    |
    |            |          | written as provided and not validated by the    |                    |
    |            |          | API.                                            |                    |
    +------------+----------+-------------------------------------------------+--------------------+


:Example:

    ::

        POST http://localhost:8526/DeviceService/LEDController/SendCommand HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        {
            "Command": "O(00,03,4)E"
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are send a command while the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please note that `Get Status`_ request will
    not return the error, however you will get the error if you send a different request while `Get Status`_ request is
    processing.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error - Serial Port Error

    This is returned when the serial port cannot be connected to. This may be because the serial is not connected
    correctly or it is not using the COM port specified in the configuration. It can also mean that another service is
    already connected to the COM port. If you need to troubleshoot the issue further, you can check the log files
    which are in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

         {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "serial_port_error",
                        "Message": "Serial port cannot be opened so unable to send command."
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    An unexpected error during writing the command.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

         {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "device_failure",
                  "Message": "Internal error while sending command"
                }
              ]
            }
         }






    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }




LED Controller API Models
-------------------------

Status
======

+--------------------+-----------------------------------------------------------------------------------------------+
| Value              | Explanation                                                                                   |
+====================+===============================================================================================+
| Unknown            | Device not initialised yet.                                                                   |
+--------------------+-----------------------------------------------------------------------------------------------+
| SerialConnected    | Connection to the I/O board was successful and commands can be sent.                          |
+--------------------+-----------------------------------------------------------------------------------------------+
| SerialNotConnected | Unable to connect to the I/O board. Ensure that the board is connected and can be seen in the |
|                    | device manager. The COM Port must be set correctly in the config file, and you should ensure  |
|                    | nothing else is listening to the same port.                                                   |
+--------------------+-----------------------------------------------------------------------------------------------+
