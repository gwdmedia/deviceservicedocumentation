==================
Access Scanner API
==================

This endpoint allows scanning barcodes, passport MRZ and driving licence AAMVA information using Access ARD Scanner.
The scanner requires no extra drivers or installation, the drivers needed are automatically installed when the device
is connected via USB.

Url
---
The base url is http://localhost:8526/DeviceService/AccessScanner/ADR .

Get Status
----------
Get the current status of the scanner. You can see if the device is currently enabled for scanning and if there is
suitable hardware connected.


:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/AccessScanner/ADR/Status HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Scanner Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "Active": false,
            "Busy": false,
            "AutoStopEnabled": true,
            "HardwareConnected": true
        }



:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



Enable the Scanner
------------------
The scanner is initialized when the device service starts, but it will not be scanning for documents until it is enabled.
Use this endpoint to enable the scanner in order to start scanning. If "AutoStopEnabled" is set to true, the scanner will stop scanning after it successfully
reads and decodes passport MRZ data, driving licence AAMVA data or a barcode. If "AutoStopEnabled" is set to false, the device will keep scanning documents until it
is disabled by sending a `Disable the Scanner`_ request. "AutoStopEnabled" defaults to false and can be changed in the device service settings.

When a barcode, AAMVA or MRZ details are successfully read, the information is stored and can be retrieved using the `Get Passport`_, `Get Driving Licence`_,
and `Get Barcode`_ endpoints.
Note that starting the scanner will clear out any previously read MRZ, AAMVA and barcode information from memory, so you should
ensure you have requested the details you need before enabling the scanner again.

:URL: /Enable
:Method: POST
:URL Params:  none
:Data Params: none


:Example:

    ::

        POST http://localhost:8526/DeviceService/AccessScanner/ADR/Enable/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the scanning cannot be started. This usually means that there is no scanner connected, or that
    the scanner has not finished initializing yet. You can get more details by sending
    a `Get Status`_ request, and retry if a device is connected.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "Unable to start scanning, device not found or
                         not initiliazed yet. Check status for more details."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Disable the Scanner
-------------------
Disable the scanner to stop reading documents. If the scanner is disabled, it will ignore all documents placed on the reader.
It is recommended to perform a `Get Passport`_, `Get Driving Licence`_ and `Get Barcode`_ request after successfully disabling scanner to ensure that
all documents that have been read are processed. This endpoint always returns a successful response, regardless of a scanner being
connected or already disabled.

If "AutoStopEnabled" is set to true, you should never call the disable after a successful scan. Calling disable too soon after a
barcode scan may lead to the scanner halting in the middle of processing and then resuming this process when the scanner is next
enabled, which may lead to stale barcode data being returned even though there is nothing on the scanner. To avoid this, turn on the
auto stop feature in the device service settings and allow the scanner to disable itself after scanning, and only call the
`Disable the Scanner`_ endpoint to stop scanning in case no barcode or passport info was read.

:URL: /Disable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/AccessScanner/ADR/Disable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Get Passport
------------
Get the MRZ information of a passport that was last scanned. Note that the information will only be returned once, the
passport details are not kept after they have been returned and subsequent calls will return null. This endpoint can be polled
to detect a passport having been scanned.

:URL: /Passport
:Method: GET
:URL Params:  none
:Data Params: none

:Example:
    ::

        GET http://localhost:8526/DeviceService/AccessScanner/ADR/Passport/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `MRZ`_ as passport

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "passport": {
                "RawMRZ": "P<GBRKITTENS<<BUTTON<EYES<<<<<<<<<<<<<<<<
                \n1234567895GBR9803151F2506033<<<<<<<<<<<<<<05\n",
                "DocumentNumber": "123456789",
                "DOB": "980315",
                "Expiry": "250603",
                "Issuer": "GBR",
                "Nationality": "GBR",
                "LastNames": "KITTENS",
                "FirstNames": "BUTTON EYES                ",
                "Type": "P ",
                "Discretionary1": "<<<<<<<<<<<<<<",
                "Discretionary2": "OMG SO CUTE",
                "Gender": "F"
            }
        }

    :Content Body when no passport found:

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "passport": null
        }


:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Get Driving Licence
-------------------
Get the AAMVA information of a driving licence that was last scanned. Note that the information will only be returned once, the
driving licence details are not kept after they have been returned and subsequent calls will return null. This endpoint can be polled
to detect a driving licence having been scanned.

:URL: /DrivingLicence
:Method: GET
:URL Params:  none
:Data Params: none

:Example:
    ::

        GET http://localhost:8526/DeviceService/AccessScanner/ADR/DrivingLicence/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `AAMVA`_ as driving licence

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "drivingLicence": {
                "RawAAMVA": "@\n\rANSI 636035080002DL00410282ZI03230021DLDAQK21220083323
                \nDCSWAKEHOUSE\nDDEN\nDACMICHAEL\nDDFN\nDADNONE\nDDGN\nDCAD\nDCBB\nDCDNONE
                \nDBD08062020\nDBB12131983\nDBA03152023\nDBC1\nDAU071 in\nDAYBRO
                \nDAG734 E CONSTITUTION DR\nDAIPALATINE\nDAJIL\nDAK600741234
                \nDCF20200806276LB7629\nDCGUSA\nDAW308\nDAHAPT 3
                \nDCKK20020083354ILLVSL01\nDDAN\nDDB09172015\nDDDY\rZIZIAORG\nZIB\nZIC\nZID\r",
                "DocumentType": "DL",
                "DocumentNumber": "K21220083323",
                "LastNames": "WAKEHOUSE",
                "FirstNames": "MICHAEL",
                "Address": "734 E CONSTITUTION DR, PALATINE IL 600741234   USA",
                "DOB": "19831213",
                "EyeColor": "BRO",
                "Height": "071 IN",
                "Gender": "Male",
                "IssueDate": "20200806",
                "ExpiryDate": "20230315",
                "VehicleClass": "D",
                "EndorsementCodes": "NONE",
                "RestrictionCodes": "B",
                "Jurisdiction": "B",
                "AAMVAVersion": "Illinois",
                "JurisdictionDetails": ""
            }
        }

    :Content Body when no passport found:

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "drivingLicence": null
        }


:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Get Barcode
-----------
Get the Barcode information of the last scanned barcode. Note that the information will only be returned once, the
details are not kept after they have been returned and subsequent calls will return null. This endpoint can be polled
to detect a barcode having been scanned. Note that if an `AAMVA`_ barcode is detected, it is returned as
`Get Driving Licence`_ instead of a barcode.

:URL: /Barcode
:Method: GET
:URL Params:  none
:Data Params: none

:Example:
    ::

        GET http://localhost:8526/DeviceService/AccessScanner/ADR/Barcode/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Barcode`_ as barcode

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "barcode": {
                "BarCodeType": 10,
                "BarCode": "FRA123456E"
            }
        }

    :Content Body when no barcode found:

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "barcode": null
        }


:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

Access Scanner API Models
-------------------------

Scanner Status
==============

The scanner status is a JSON object returned with a `Get Status`_ request. It has four fields boolean fields to describe the status.

+-------------------+-----------------------------------------------------------------------------------------------+
| Field name        | Explanation                                                                                   |
+===================+===============================================================================================+
| Active            | Is the scanner enabled to scan for documents or not.                                          |
+-------------------+-----------------------------------------------------------------------------------------------+
| Busy              | Is the scanner currently reading a document.                                                  |
+-------------------+-----------------------------------------------------------------------------------------------+
| AutoStopEnabled   | Whether to automatically stop scanning after successfully reading MRZ details. Note that      |
|                   | this has no effect for barcodes. Can be changed in the device service settings.               |
+-------------------+-----------------------------------------------------------------------------------------------+
| HardwareConnected | Is a scanner detected to be plugged in or not.                                                |
+-------------------+-----------------------------------------------------------------------------------------------+

MRZ
===

After a machine readable passport is successfully scanned and decoded, the following details are returned by the
`Get Passport`_ endpoint as the passport. All the fields are Strings.


+-------------------+-----------------------------------------------------------------------------------------------+
| Field name        | Explanation                                                                                   |
+===================+===============================================================================================+
| RawMRZ            | The MRZ on the document presented to the scanner.                                             |
+-------------------+-----------------------------------------------------------------------------------------------+
| DocumentNumber    | Passport/Document number.                                                                     |
+-------------------+-----------------------------------------------------------------------------------------------+
| DOB               | Date of birth of the document holder.                                                         |
+-------------------+-----------------------------------------------------------------------------------------------+
| Expiry            | Document’s date of expiry.                                                                    |
+-------------------+-----------------------------------------------------------------------------------------------+
| Issuer            | Country of Issue.                                                                             |
+-------------------+-----------------------------------------------------------------------------------------------+
| Nationality       | Nationality of document holder.                                                               |
+-------------------+-----------------------------------------------------------------------------------------------+
| LastName          | Last name of document holder.                                                                 |
+-------------------+-----------------------------------------------------------------------------------------------+
| FirstName         | First name of document holder.                                                                |
+-------------------+-----------------------------------------------------------------------------------------------+
| Type              | Document type.                                                                                |
+-------------------+-----------------------------------------------------------------------------------------------+
| Discretionary1    | Data content dependent on document issuer.                                                    |
+-------------------+-----------------------------------------------------------------------------------------------+
| Discretionary2    | Data content dependent on document issuer.                                                    |
+-------------------+-----------------------------------------------------------------------------------------------+
| Gender            | Sex of the document holder.                                                                   |
+-------------------+-----------------------------------------------------------------------------------------------+

AAMVA
=====

There are some PDF417 barcodes that have extended data with a specific format that is defined by the American
Association of Motor Vehicle Administrators (AAMVA). If this format is detected, it will only be available via the
`Get Driving Licence`_ endpoint and not the `Get Barcode`_.

After AAMVA compliant driving licence is successfully scanned and decoded, the following details are returned by the
`Get Driving Licence`_ endpoint as the driving licence. All the values are strings.


+----------------------+-----------------------------------------------------------------------------------------------+
| Field name           | Explanation                                                                                   |
+======================+===============================================================================================+
| RawAAMVA             | The raw AAMVA data on the document presented to the scanner.                                  |
|                      | See https:\/\/www.aamva.org\/2013DLIDCardDesignStandard\/ for more details.                   |
+----------------------+-----------------------------------------------------------------------------------------------+
| DocumentType         | Document type.                                                                                |
+----------------------+-----------------------------------------------------------------------------------------------+
| DocumentNumber       | Document ID.                                                                                  |
+----------------------+-----------------------------------------------------------------------------------------------+
| LastNames            | Family name of the document holder.                                                           |
+----------------------+-----------------------------------------------------------------------------------------------+
| FirstNames           | Name of the document holder.                                                                  |
+----------------------+-----------------------------------------------------------------------------------------------+
| Address              | Address of the document holder.                                                               |
+----------------------+-----------------------------------------------------------------------------------------------+
| DOB                  | Date of birth  of the document holder.                                                        |
+----------------------+-----------------------------------------------------------------------------------------------+
| EyeColor             | Eye colour of the document holder.                                                            |
+----------------------+-----------------------------------------------------------------------------------------------+
| Height               | Height of the document holder.                                                                |
+----------------------+-----------------------------------------------------------------------------------------------+
| Gender               | Sex of the document holder.                                                                   |
+----------------------+-----------------------------------------------------------------------------------------------+
| IssueDate            | Issue date of the document.                                                                   |
+----------------------+-----------------------------------------------------------------------------------------------+
| ExpiryDate           | Expiry date of the document.                                                                  |
+----------------------+-----------------------------------------------------------------------------------------------+
| VehicleClass         | Document's vehicle class.                                                                     |
+----------------------+-----------------------------------------------------------------------------------------------+
| EndorsementCodes     | Document's endorsement codes.                                                                 |
+----------------------+-----------------------------------------------------------------------------------------------+
| RestrictionCodes     | Document's restriction codes.                                                                 |
+----------------------+-----------------------------------------------------------------------------------------------+
| Jurisdiction         | Document's jurisdiction.                                                                      |
+----------------------+-----------------------------------------------------------------------------------------------+
| AAMVAVersion         | Document's AAMVA version.                                                                     |
+----------------------+-----------------------------------------------------------------------------------------------+
| JurisdictionDetails  | Document's jurisdiction details, can be empty                                                 |
+----------------------+-----------------------------------------------------------------------------------------------+


Barcode
=======

After a barcode is successfully scanned, the following details are returned by the
`Get Barcode`_ endpoint as the barcode.


+-------------------+------------------------------------------------------------------+
| Field name        | Explanation                                                      |
+===================+==================================================================+
| BarCodeType       | The type of barcode as integer, see `Barcode Types`_ for values. |
+-------------------+------------------------------------------------------------------+
| BarCode           | The barcode                                                      |
+-------------------+------------------------------------------------------------------+

Barcode Types
=============

These are the Barcode type options.

+-------------+-------------------------------------+
| value       | Barcode type                        |
+=============+=====================================+
| 1           | BC_DATAMATRIX                       |
+-------------+-------------------------------------+
| 2           | BC_QR                               |
+-------------+-------------------------------------+
| 3           | BC_AZTEC                            |
+-------------+-------------------------------------+
| 4           | BC_MC                               |
+-------------+-------------------------------------+
| 5           | BC_PDF417 [1]_                      |
+-------------+-------------------------------------+
| 6           | BC_MICRO_PDF417                     |
+-------------+-------------------------------------+
| 7           | BC_CCA                              |
+-------------+-------------------------------------+
| 8           | BC_CCB                              |
+-------------+-------------------------------------+
| 9           | BC_CCC                              |
+-------------+-------------------------------------+
| 10          | BC_C39                              |
+-------------+-------------------------------------+
| 11          | BC_I25                              |
+-------------+-------------------------------------+
| 12          | BC_CODEBAR                          |
+-------------+-------------------------------------+
| 13          | BC_C128                             |
+-------------+-------------------------------------+
| 14          | BC_C93                              |
+-------------+-------------------------------------+
| 15          | BC_UPCA                             |
+-------------+-------------------------------------+
| 16          | BC_UPCE                             |
+-------------+-------------------------------------+
| 17          | BC_EAN13                            |
+-------------+-------------------------------------+
| 18          | BC_EAN8                             |
+-------------+-------------------------------------+
| 19          | BC_DATABAR_14                       |
+-------------+-------------------------------------+
| 20          | BC_DATABAR_STACKED                  |
+-------------+-------------------------------------+
| 21          | BC_DATABAR_LIMITED                  |
+-------------+-------------------------------------+
| 22          | BC_DATABAR_EXPANDED                 |
+-------------+-------------------------------------+
| 23          | BC_DATABAR_EXP_STA                  |
+-------------+-------------------------------------+
| 24          | BC_DATABAR_HAN_XIN                  |
+-------------+-------------------------------------+
| 25          | BC_DATABAR_QR_MICRO                 |
+-------------+-------------------------------------+
| 27          | BC_C11                              |
+-------------+-------------------------------------+
| 28          | BC_C32                              |
+-------------+-------------------------------------+
| 29          | BC_PLESSEY                          |
+-------------+-------------------------------------+
| 30          | BC_MSI_PLESSEY                      |
+-------------+-------------------------------------+
| 31          | BC_TELEPEN                          |
+-------------+-------------------------------------+
| 32          | BC_TRIOPIC                          |
+-------------+-------------------------------------+
| 33          | BC_PHARMACODE                       |
+-------------+-------------------------------------+
| 34          | BC_M25                              |
+-------------+-------------------------------------+
| 35          | BC_S25                              |
+-------------+-------------------------------------+
| 36          | BC_C49                              |
+-------------+-------------------------------------+
| 37          | BC_C16K                             |
+-------------+-------------------------------------+
| 38          | BC_CODABLOCK                        |
+-------------+-------------------------------------+
| 39          | BC_POSTNET                          |
+-------------+-------------------------------------+
| 40          | BC_USPS_PLANET                      |
+-------------+-------------------------------------+
| 41          | BC_USPS_INTEL_MAIL                  |
+-------------+-------------------------------------+
| 42          | BC_AUSTRA_POST                      |
+-------------+-------------------------------------+
| 43          | BC_DUTCH_POST                       |
+-------------+-------------------------------------+
| 44          | BC_JAPAN_POST                       |
+-------------+-------------------------------------+
| 45          | BC_ROYAL_MAIL                       |
+-------------+-------------------------------------+
| 46          | BC_UPU_TAGS                         |
+-------------+-------------------------------------+
| 47          | BC_KOREA_POST                       |
+-------------+-------------------------------------+
| 48          | BC_HK25                             |
+-------------+-------------------------------------+
| 49          | BC_NEC25                            |
+-------------+-------------------------------------+
| 50          | BC_IATA25                           |
+-------------+-------------------------------------+
| 51          | CANADA_POST                         |
+-------------+-------------------------------------+

.. [1] If detected as having AAMVA extended data, these are returned via the `Get Driving Licence`_ endpoint.
