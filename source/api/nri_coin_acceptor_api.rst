=====================
NRI Coin Acceptor API
=====================

This endpoint allows accepting coins using NRI G13 coin acceptor and monitoring its status.

Url
---
The base url is http://localhost:8526/DeviceService/CoinAcceptor/NRIG13

Get Status
----------
Get the current status of the NRI G13 coin acceptor. The states also contains information of coins inserted since when
you last time checked the status. It's recommended to check status before you enable the coin acceptor to make sure it
doesn't have any coins that haven't been dealt with and that it isn't in a fatal state.
It is also recommended to check status after you disable the coin
acceptor to make sure it's successfully disabled and all coins received have been dealt with.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/CoinAcceptor/NRIG13/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Coin Acceptor Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

         {
          "Connectivity": "Connected",
          "HardwareState": "Enabled",
          "CoinsReceivedSinceLastCheck": [
            {
              "WhenInserted": "2018-02-01T16:23:39.0828453Z",
              "CoinValue": 1,
              "Currency": "GBP"
            },
            {
              "WhenInserted": "2018-02-01T16:23:41.5979891Z",
              "CoinValue": 0.5,
              "Currency": "GBP"
            }
          ]
        }

:Error Responses:
        :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



Enable the Coin Acceptor
------------------------
Enable the coin acceptor to start taking coins. If the coin acceptor is disabled, it will reject all the coins.
It's recommended to call `Get Status`_
every 500 - 1000ms after the acceptor is enabled so you can get inserted coins info in time.

:URL: /Enable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/CoinAcceptor/NRIG13/Enable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the coin acceptor when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.
    Please note that `Get Status`_ request will not return the error and will not cause other requests
    to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }



    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the acceptor has encountered an issue. You can get more details by sending
    a `Get Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which is in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The coin acceptor has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Disable the Coin Acceptor
-------------------------
Disable the coin acceptor to stop taking coins. If the coin acceptor is disabled, it will reject all the coins.
It is recommended to perform a `Get Status`_ request after successfully disabling the coin acceptor to ensure there
are no coins inserted and not dealt with.

:URL: /Disable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/CoinAcceptor/NRIG13/Disable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the coin acceptor when the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please note that `Get Status`_ request will
    not return the error and will not cause other requests to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the coin acceptor has encountered an issue. You can get more details by sending a `Get Status`_ request.
    The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which is in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The coin acceptor has encountered an issue.
                              Please check status to find out more."
                }
              ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

NRI Coin Acceptor API Models
----------------------------

Coin Acceptor Status
====================

The coin acceptor status is a JSON object returned with a `Get Status`_ request.
It has 3 fields: Connectivity, HardwareState and CoinsReceivedSinceLastCheck.

:Example:

    ::

        {
          "Connectivity": "Connected",
          "HardwareState": "Enabled",
          "CoinsReceivedSinceLastCheck": [
            {
              "WhenInserted": "2018-02-01T16:23:39.0828453Z",
              "CoinValue": 1,
              "Currency": "GBP"
            },
            {
              "WhenInserted": "2018-02-01T16:23:41.5979891Z",
              "CoinValue": 0.5,
              "Currency": "GBP"
            }
          ]
        }

Connectivity
^^^^^^^^^^^^

+--------------------+-------------------------------------------------+
| Value              | Explanation                                     |
+====================+=================================================+
| Disconnected       | Device is not connected                         |
+--------------------+-------------------------------------------------+
| Connected          | Device is connected                             |
+--------------------+-------------------------------------------------+

HardwareState
^^^^^^^^^^^^^

+----------+-----------------------------------------------------------------------------------------------------------+
| Value    | Explanation                                                                                               |
+==========+===========================================================================================================+
| Unknown  | State is unknown. This usually happens when the device is unavailable, so the state cannot be retrieved.  |
+----------+-----------------------------------------------------------------------------------------------------------+
| Enabled  | Device is enabled, so it's ready to take coins.                                                           |
+----------+-----------------------------------------------------------------------------------------------------------+
| Disabled | Device is disabled, so it will reject all coins.                                                          |
+----------+-----------------------------------------------------------------------------------------------------------+

CoinsReceivedSinceLastCheck
^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is a list of coins that are received by the acceptor since when you last time check the status by `Get Status`_ .
The list is cleaned up afterwards, so next time when you check the status, you will get a new list.

Each coin in the list has its value, currency and timestamp of when it's inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the coin was inserted.       |
+--------------+-----------------------------------------------------------------+
| CoinValue    | The value of the coin. For example, 0.1 means 10 pence in GBP.  |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the coin, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+

:Example:

    ::

            {
              "WhenInserted": "2018-02-01T16:23:39.0828453Z",
              "CoinValue": 1,
              "Currency": "GBP"
            }


Configuration
-------------

The config file for NRI C31 coin acceptor is located at C:\\ProgramData\\GW Devices\\Applications\\Device Service\\configs\\modules\\NRICoinAcceptorService.config
(You only need to care about this when you are installing the Device Service.)

:Example:

    ::

        <?xml version="1.0" encoding="utf-8"?>
        <config>
          <field key="port" value="COM1" />
          <field key="baud_rate" value="9600" />
          <field key="currency" value="GBP" />
          <field key="code1" value="0.01" />
          <field key="code2" value="0.01" />
          <field key="code3" value="0.02" />
          <field key="code4" value="0.02" />
          <field key="code5" value="0.05" />
          <field key="code6" value="0.05" />
          <field key="code7" value="0.1" />
          <field key="code8" value="0.1" />
          <field key="code9" value="0.2" />
          <field key="code10" value="0.5" />
          <field key="code11" value="1.0" />
          <field key="code12" value="2.0" />
        </config>

The config file is in XML. It can have one or more fields. Each field has a key and its value. The coin acceptor outputs the programmed
coins in numbers from 1 to 16. The fields from code1 to code16 are used to map the coin code(1-16) to actual coin value. The coin value
should be a decimal number. For example, if we use GBP, 0.01 means 10 pence and 2.0 means 2 pounds. The code* fields are optional, because
not all codes are used if a country doesn't have so many types of coins, but you have to config the ones supported by your country. You can
ask coin accept manufacturer if you have any questions on which coin code to use.
 
+-----------+----------------------------------------------+----------+
| Field     | Description                                  |          |
+===========+==============================================+==========+
| port      | COM port number of the device.               | required |
+-----------+----------------------------------------------+----------+
| baud_rate | Baud Rate used when connect to the device    | required |
+-----------+----------------------------------------------+----------+
| currency  | The currency that the coin acceptor is using | required |
+-----------+----------------------------------------------+----------+
| code1     | The coin value for coin code 1               | optional |
+-----------+----------------------------------------------+----------+
| code2     | The coin value for coin code 2               | optional |
+-----------+----------------------------------------------+----------+
| code3     | The coin value for coin code 3               | optional |
+-----------+----------------------------------------------+----------+
| code4     | The coin value for coin code 4               | optional |
+-----------+----------------------------------------------+----------+
| code5     | The coin value for coin code 5               | optional |
+-----------+----------------------------------------------+----------+
| code6     | The coin value for coin code 6               | optional |
+-----------+----------------------------------------------+----------+
| code7     | The coin value for coin code 7               | optional |
+-----------+----------------------------------------------+----------+
| code8     | The coin value for coin code 8               | optional |
+-----------+----------------------------------------------+----------+
| code9     | The coin value for coin code 9               | optional |
+-----------+----------------------------------------------+----------+
| code10    | The coin value for coin code 10              | optional |
+-----------+----------------------------------------------+----------+
| code11    | The coin value for coin code 11              | optional |
+-----------+----------------------------------------------+----------+
| code12    | The coin value for coin code 12              | optional |
+-----------+----------------------------------------------+----------+
| code13    | The coin value for coin code 13              | optional |
+-----------+----------------------------------------------+----------+
| code14    | The coin value for coin code 14              | optional |
+-----------+----------------------------------------------+----------+
| code15    | The coin value for coin code 15              | optional |
+-----------+----------------------------------------------+----------+
| code16    | The coin value for coin code 16              | optional |
+-----------+----------------------------------------------+----------+