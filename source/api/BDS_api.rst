BDS and IPP320
==============

This endpoint allows processing transactions using Heartland Bolletta Device Service and Ingenico IPP320 card terminal.

Url
---
The base url is http://localhost:8526/DeviceService/BDS/ .

Status
------
Get the current status of the BDS and IPP320 terminal. This can be used to ensure the terminal is available
before making a transaction request or when first starting the application.

:URL: /status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::
    
        GET http://localhost:8526/DeviceService/BDS/status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response: 

    :Code: 200 OK
    
    :Content-Type: application/json
    
    :Content Body: `IPP320BDSState`_
    
    ::
    
        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {"Description":"Available","StatusCode":1}
    

:Error Response:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }




--------------------------------------------------------------------------------------------------------------


Transaction Request
-------------------

Send a new transaction request to the terminal and receive a response detailing the transaction result.
The request result will be returned after the card terminal returns the transaction information.
This may take some time while the customer uses the terminal to pay. You should wait for the result to
be returned. If you time out before the transaction has completed you will not receive the information
but the terminal may still finish the transaction and charge the customer card.

:URL: /Transaction

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | Amount     | Yes      | Transaction amount. Must be a positive number. | decimal or string  |
    +------------+----------+------------------------------------------------+--------------------+
    | Reference  | Yes      | Any reference to identify the customer/bill.   | string             |
    +------------+----------+------------------------------------------------+--------------------+
    | Payor      | Yes      | Details to identify the customer. At least Post| `Individual`_      |
    |            |          | Code should be provided, but you may provide   | as json            |
    |            |          | any other `Individual`_ information as well.   |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | BillType   | No       | BDS transaction BillType as configured by      | string             |
    |            |          | Heartland. Default is "Bill Payment" if no     |                    |
    |            |          | value is provided.                             |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |proMet|   | No       | BDS transaction ProcessingMethod as configured | string             |
    |            |          | by Heartland. Default is "Credit" if no value  |                    |
    |            |          | is provided. Other possible value is "Debit".  |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |appType|  | No       | BDS transaction ApplicationType  as configured | string             |
    |            |          | by Heartland. Default is "Cashier" if no value |                    |
    |            |          | is provided. Other possible values are         |                    |
    |            |          | 'ExternalPos' and 'ExternalPosMoto'.           |                    |
    +------------+----------+------------------------------------------------+--------------------+

.. |appType|  replace:: ApplicationType

.. |proMet|  replace:: ProcessingMethod

:Example:

    ::

        POST http://localhost:8526/DeviceService/BDS/Transaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "Reference":"987987",
            "Amount":12,
            "Payor": {
                "PostalCode":"36832",
                "FirstName": "Ada",
                "LastName": "Lovelace"
            }
        }


:Success Response:

    :Code: 200 OK

        Note that the response may be 200 OK even if the transaction itself failed. You should check the fields in the
        response to determine if the transaction was successful or not. Please see the example JSON.

    :Content-Type: application/json

    :Content Body: `Transaction Response`_

        ::

            HTTP/1.1 200 OK
            Content-Type: application/json; charset=utf-8

            {
                "ErrorCode": "NoError",
                "Successful": true,
                "TransactionId": 690489,
                "TransactionDateTime": "2017-12-12T11:39:51.3715218Z",
                "Amount": 12,
                "FeeAmount": 0,
                "Bills": [
                    {
                        "BillType": "Bill Payment",
                        "ID1": "987987",
                        "Amount": 12
                    }
                ],
                "Payor": {
                    "FirstName": "Ada",
                    "LastName": "Lovelace",
                    "PostalCode": "36832"
                },
                "ReadDetails": {
                    "ReadId": "6a909a4d-1a07-424d-af92-4c8a8658d500",
                    "PaymentMethod": "DiscoverCredit",
                    "Amount": 12,
                    "ConvenienceFee": 0,
                    "ExpirationMonth": 12,
                    "ExpirationYear": 2025,
                    "AccountLastFour": "6527",
                    "CardHolderName": "DIS TEST CARD",
                    "EntryMethod": 3
                },
                "Authorizations": [
                    {
                        "Amount": 12,
                        "AuthCode": "012030",
                        "Type": 1,
                        "BatchId": "444608",
                        "MaskedAccountNumber": "6527",
                        "PaymentMethod": "DiscoverCredit",
                        "GatewayReferenceId": "1031344874"
                    }
                ]
            }


        See `Example Transaction JSON`_ for more examples for different kind of transaction results.

:Error Responses:

        **400 Bad Request**

        This is returned if any parameters are missing or incorrect.
        The FieldName and Message give further information on what is missing.

        ::

            HTTP/1.1 400 Bad Request
            Content-Type: application/json; charset=utf-8

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "ApplicationType",
                            "Message": "ApplicationType should either be
                                'Cashier', 'ExternalPos', or 'ExternalPosMoto'."
                        }
                    ]
                }
            }



        **401 Unauthorized**

        This is returned if the username or password are incorrect or have not been provided.
        The message body may be empty or it may contain a further description.

        ::

                {
                    "ResponseStatus": {
                        "ErrorCode": "Invalid UserName or Password",
                        "Message": "Invalid UserName or Password"
                    }
                }



        **500 Internal Server Error**

        This may be returned if there is an error during processing the transaction or if the BDS is not available.
        The body may contain more details of the error.

        ::

                {
                    "ResponseStatus": {
                        "ErrorCode": "DeviceError",
                        "Errors": [
                            {
                                "ErrorCode": "unavailable",
                                "Message": "Card Terminal is not available"
                            }
                        ]
                    }
                }



--------------------------------------------------------------------------------------------------------------



Example Transaction JSON
------------------------

Here are some examples of the JSON returned by a transaction request for different kinds of transaction results.

:Timed out:

    ::

        {
            "ErrorCode": "DeviceFunctionError",
            "Successful": false,
            "ErrorCodeDetails": "Timeout while waiting for user action.",
            "Amount": 12,
            "FeeAmount": 0,
            "Bills": [
                {
                    "BillType": "Bill Payment",
                    "ID1": "987987",
                    "Amount": 12
                }
            ],
            "Payor": {
                "PostalCode": "36832"
            }
        }


:Customer Cancelled Transaction:

    ::

        {
            "ErrorCode": "DeviceFunctionError",
            "Successful": false,
            "ErrorCodeDetails": "User Cancelled Operation.",
            "Amount": 12,
            "FeeAmount": 0,
            "Bills": [
                {
                    "BillType": "Bill Payment",
                    "ID1": "987987",
                    "Amount": 12
                }
            ],
            "Payor": {
                "PostalCode": "36832"
            }
        }


:User Declined the Amount:

    ::

        {
            "ErrorCode": "DeviceFunctionError",
            "Successful": false,
            "ErrorCodeDetails": "User declined the amount.",
            "Amount": 1.51,
            "FeeAmount": 0,
            "Bills": [
                {
                    "BillType": "Bill Payment",
                    "ID1": "123456",
                    "Amount": 1.51
                }
            ],
            "Payor": {
                "FirstName": "Jim",
                "LastName": "Smith"
            }
        }



--------------------------------------------------------------------------------------------------------------


BDS and IPP320 Models
=====================
        
IPP320BDSState
--------------

These are the status codes and descriptions returned in a `Status`_ request.
    
+------------+-------------------+-------------------------------------------------------------+
| StatusCode | Description       | Explanation                                                 |
+============+===================+=============================================================+
| 0          | Unknown           | State is unknown and cannot be determined.                  |
+------------+-------------------+-------------------------------------------------------------+
| 1          | Available         | The BDS is running and there is a card terminal             |
|            |                   | connected.                                                  |
+------------+-------------------+-------------------------------------------------------------+
| 2          | NoConnectedDevice | BDS is running but no card terminal is detected to be       |
|            |                   | connected. This may also happen if the card terminal is in  |
|            |                   | the middle of processing a transaction so not available.    |
+------------+-------------------+-------------------------------------------------------------+
| 3          | NotAvailable      | BDS is not running.                                         |
+------------+-------------------+-------------------------------------------------------------+



Transaction Response
--------------------

Transaction Response contains all the information provided by the IPP320 Terminal after a transaction
request has been made.

+----------------------+-----------------------------------------------------------+----------------------+
| Name                 | Description                                               | Data Type            |
+======================+===========================================================+======================+
| Successful           | Indicates if the Transaction was successful               | Boolean              |
+----------------------+-----------------------------------------------------------+----------------------+
| ErrorCode            | An error code to indicate what happened in the event of an| `BDSErrorCode`_      |
|                      | error.                                                    |                      |
+----------------------+-----------------------------------------------------------+----------------------+
| ErrorCodeDetails     | Details of the error if applicable.                       | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| IssuerResponseCode   | The issuer response code.                                 | Integer              |
+----------------------+-----------------------------------------------------------+----------------------+
| TransactionId        | The Bolletta Transaction ID of the Transaction.           | Integer              |
+----------------------+-----------------------------------------------------------+----------------------+
| TransactionDateTime  | The Transaction DateTime as recorded by Bolletta          | Date                 |
+----------------------+-----------------------------------------------------------+----------------------+
| Amount               | The Base Amount of the transaction.                       | Decimal number       |
|                      | Echoed back from the request.                             |                      |
+----------------------+-----------------------------------------------------------+----------------------+
| FeeAmount            | The Fee Amount of the transaction.                        | Decimal number       |
+----------------------+-----------------------------------------------------------+----------------------+
| Bills                | The Bills paid. Echoed back from the request.             | |cl| `Bill`_         |
+----------------------+-----------------------------------------------------------+----------------------+
| Payor                | The Payor for the payment. Echoed back from the request.  | `Individual`_        |
+----------------------+-----------------------------------------------------------+----------------------+
| ReadDetails          | The Read details for the Device Read associated with the  | `ReadDetails`_       |
|                      | Transaction.                                              |                      |
+----------------------+-----------------------------------------------------------+----------------------+
| Authorizations       | The Authorizations for the Transaction.                   | |cl| `Authorization`_|
+----------------------+-----------------------------------------------------------+----------------------+
| Token                | The reusable token if requested                           | String               |
+----------------------+-----------------------------------------------------------+----------------------+


.. |cl|  replace:: Collection of


BDSErrorCode
------------

The BDS error code is returned as a string. These are the possible values:

+-----------------------+
| Name                  |
+=======================+
| NoError               |
+-----------------------+
| GeneralError          |
+-----------------------+
| UserCancel            |
+-----------------------+
| DeviceFunctionError   |
+-----------------------+
| DeviceCapabilityError |
+-----------------------+
| ErrorCallingBolletta  |
+-----------------------+
| ErrorCallingIssuer    |
+-----------------------+
| Decline               |
+-----------------------+
| AdminCancel           |
+-----------------------+



Bill
----

The bill information is returned with a transaction request and it will echo details sent with the request.

+----------------------+-----------------------------------------------------------+----------------------+
| Name                 | Description                                               | Data Type            |
+======================+===========================================================+======================+
| BillType             | The Bill Type as Setup in Bolletta                        | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| ID1                  | The Reference sent with the request.                      | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| Amount               | Bill amount.                                              | Decimal number       |
+----------------------+-----------------------------------------------------------+----------------------+


Individual
----------

An individual in the Bolletta system is sent as the **Payor** with a Transaction request. The transaction result
will echo back the same details that are sent with the request. Any fields not sent will be omitted from
the response as well.

+----------------------+-----------------------------------------------------------+----------------------+
| Name                 | Additional information                                    | Data Type            |
+======================+===========================================================+======================+
| BusinessName         | Max length 100                                            | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| FirstName            | Max length 50                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| MiddleName           | Max length 50                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| LastName             | Max length 50                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| EmailAddress         | Max length 70. Must be formatted as an email address.     | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| PhoneNumber          | Max length 10                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| Address              | Max length 100                                            | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| City                 | Max length 50                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| State                | Max length 50                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| Country              | Country code, max length 2                                | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| PostalCode           | Max length 20                                             | String               |
+----------------------+-----------------------------------------------------------+----------------------+


ReadDetails
-----------

Read details show the information read form the card during the transaction with the IPP320 terminal.

+----------------------+-----------------------------------------------------------+----------------------+
| Name                 | Description                                               | Data Type            |
+======================+===========================================================+======================+
| ReadId               | The Id used to store this read for use in the Payment     | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| PaymentMethod        | The payment method                                        | `PaymentMethod`_     |
+----------------------+-----------------------------------------------------------+----------------------+
| Amount               | The amount passed in                                      | Decimal number       |
+----------------------+-----------------------------------------------------------+----------------------+
| ConvenienceFee       | The convenience fee as calculated for the Amount and      | Decimal number       |
|                      | Payment Type                                              |                      |
+----------------------+-----------------------------------------------------------+----------------------+
| ExpirationMonth      | The expiration month if applicable                        | Integer              |
+----------------------+-----------------------------------------------------------+----------------------+
| ExpirationYear       | The expiration year if applicable                         | Integer              |
+----------------------+-----------------------------------------------------------+----------------------+
| AccountLastFour      | The Last 4 digits of the Account Number                   | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| CardHolderName       | The card holder name retrieved from the track data        | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| EntryMethod          | Indicator how the Card was entered if applicable          | Integer              |
+----------------------+-----------------------------------------------------------+----------------------+
| IsEmvFallback        | Indicates if this read was a result of an EMV Failure     | Boolean              |
|                      | that prompted a Fallback                                  |                      |
+----------------------+-----------------------------------------------------------+----------------------+
| |prev|               | Indicates if the previous EMV transaction was also a      | Boolean              |
|                      | fallback (could indicate a problem with the device)       |                      |
+----------------------+-----------------------------------------------------------+----------------------+


.. |prev| replace:: PreviousEmvAlsoFallback



Authorization
-------------

The Authorization Class holds details about the individual authorizations made during a transaction.

+----------------------+-----------------------------------------------------------+----------------------+
| Name                 | Description                                               | Data Type            |
+======================+===========================================================+======================+
| Amount               | The Amount of the Authorization                           | Decimal number       |
+----------------------+-----------------------------------------------------------+----------------------+
| AuthCode             | The Authorization Code from the Issuer                    | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| Type                 | The Type of Authorization                                 | Integer              |
+----------------------+-----------------------------------------------------------+----------------------+
| BatchId              | The Batch ID of the Authorization                         | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| MaskedAccountNumber  | The Masked Account Number                                 | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| PaymentMethod        | The payment method                                        | `PaymentMethod`_     |
+----------------------+-----------------------------------------------------------+----------------------+
| GatewayReferenceId   | The ID of the Authorization in the Gateway System         | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| EmvApplicationName   | The Emv Application Name if applicable                    | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| |emvAppId|           |The EmvApplicationIdentifier if applicable                 | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| |emvAppCT|           | The Emv Application Cryptogram Type if applicable         | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| |emvAppCryp|         | The Emv Application Cryptogram if applicable              | String               |
+----------------------+-----------------------------------------------------------+----------------------+
| |emvCardVM|          | The Emv Cardholder Verification Method if applicable      | String               |
+----------------------+-----------------------------------------------------------+----------------------+

.. |emvAppId| replace:: EmvApplicationIdentifier
.. |emvAppCT| replace:: EmvApplicationCryptogramType
.. |emvAppCryp| replace:: EmvApplicationCryptogram
.. |emvCardVM| replace:: EmvCardholderVerificationMethod


PaymentMethod
-------------

Possible enumeration values:

+-----------------------+-------+
| Name                  | Value |
+=======================+=======+
| Unassigned            | 0     |
+-----------------------+-------+
| VisaCredit            | 1     |
+-----------------------+-------+
| MastercardCredit      | 2     |
+-----------------------+-------+
| DiscoverCredit        | 3     |
+-----------------------+-------+
| AmericanExpressCredit | 4     |
+-----------------------+-------+
| VisaDebit             | 11    |
+-----------------------+-------+
| MastercardDebit       | 12    |
+-----------------------+-------+
| DiscoverDebit         | 13    |
+-----------------------+-------+
| AmericanExpressDebit  | 14    |
+-----------------------+-------+

