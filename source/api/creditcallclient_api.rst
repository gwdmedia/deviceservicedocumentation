Credit Call ChipDNAServer API
==================================

The device service is integrated with the ChipDNA Server. This third party service handles the wrapping to talk to a variety of different card terminals. The API request structure will remain the same no matter
the card reader you decide to integrate with your project. The only notable difference will be during installation and configuration.


URL
------
The below endpoint will allow you to send transaction and status requests to the card terminal via the ChipDNAServer.

BaseURL http://localhost:8526/DeviceService/CreditCall


Status
------
Get the current connection status of the connected card reader via the ChipDNA server, as well as the status of any ongoing transaction.
The connection status can be used to check the terminal is available
when first starting the application or perhaps when initiating a flow which requires the card reader in your software/app. Prior to any transaction request, the availability
of the card terminal is first checked and state updated before sending a transaction, so there is no need to request a status update immediately before sending a transaction request.
The TransactionStatus can be used to show feedback to the end user about the transaction that is taking place.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/CreditCall/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `CreditCallStatus`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Wed, 01 Aug 2018 12:00:00 GMT

        {
            "StatusCode": 1,
            "Description": "Available",
            "TransactionStatus": null
        }


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Wed, 01 Aug 2018 12:00:00 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





--------------------------------------------------------------------------------------------------------------

Transaction Request
-------------------

Send a new transaction request to the terminal and receive a response detailing the transaction result. The request will
first check the card terminal is available. The device service will then call to the ChipDNAServer to initialize a
transaction on the card terminal.

The request result will be returned after the card terminal returns the transaction information.
This may take some time while the customer uses the terminal to pay. You should wait for the result to
be returned. If you time out before the transaction has completed you will not receive the information
but the terminal may still finish the transaction and charge the customer card. Note that you should check the transaction
approved field to determine the success and not rely on the HTTP status code.

:URL: /Transaction

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | Amount     | Yes      | Transaction amount. Must be a positive number. | decimal            |
    +------------+----------+------------------------------------------------+--------------------+
    | Reference  | No       | Reference for the transaction.                 | string             |
    +------------+----------+------------------------------------------------+--------------------+

.. |rece|  replace:: ReceiptAddress
.. |desc|  replace:: TransactionDescription



:Example:

    ::

        POST http://localhost:8526/DeviceService/CreditCall/Transaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "Reference": "test6",
            "Amount": 1.23
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Transaction`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 1581
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:16:04 GMT

        {
           {
            "CustomerReceipt": {
                "MerchantName": "ChipDNA Lite",
                "PanMasked": "************0010",
                "TransactionType": "SALE",
                "AuthReference": "7e63f874-cf3a-4439-aaf9-186be54045a3",
                "MerchantIdMasked": "***45678",
                "TerminalIdMasked": "****2000",
                "AuthDateTime": "2018/08/06 09:43:25",
                "RetentionMessage": "Please retain for your records",
                "TransactionResult": "APPROVED",
                "AuthCode": "ABA8A8",
                "CardholderVerification": "PIN VERIFIED",
                "TotalAmount": "USD0.01",
                "TransactionSource": "ICC",
                "PanSequence": "01",
                "Aid": "A0000000031010",
                "CardScheme": "VISA CREDIT"
            },
            "TransactionApproved": true,
            "Errors": "",
            "CardHash": "xxveDP+xBLbFRkmXlGhxdehuWqQ=",
            "CardReference": "11f7b0a8-cf54-eba1-80fc-141877469759"
        }


:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "amount",
                            "Message": "Amount contains invalid characters."
                        }
                    ],
                    "CardHash": null,
                    "CardReference": null
                }
            }

    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT


    500 Internal Server Error

        ::

            HTTP/1.1 500 Internal Server Error
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT
            {
                "ResponseStatus": {
                    "ErrorCode": "DeviceError",
                    "Message": null,
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "device_failure",
                            "Message": "Internal error while processing transaction"
                        }
                    ],
                    "CardHash": null,
                    "CardReference": null
                }
            }


--------------------------------------------------------------------------------------------------------------

Update Terminal Request
-----------------------

Terminal configuration is done through a terminal management system. An API call may be made when the terminal is
connected, which when the terminal is next idle, will cause it to check for any updates on the TMS. A success response
only tells us that the request was made successfully to que the job on the ChipDNAServer and not that the device has
been successfully updated. This will need to be checked on the TMS itself.

:URL: /UpdateTerminalRequest
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/CreditCall/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Wed, 01 Aug 2018 12:00:00 GMT

        RequestSuccessful


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Wed, 01 Aug 2018 12:00:00 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





--------------------------------------------------------------------------------------------------------------


CreditCallClient API Models
=============================

CreditCallStatus
----------------

+--------------------+-----------------------------------------------------------------------------------------------+
| Field Name         | Value                                                                                         |
+====================+===============================================================================================+
| StatusCode         | `StatusCodes`_ int.                                                                           |
+--------------------+-----------------------------------------------------------------------------------------------+
| Description        | A plain text string describing the given status code.                                         |
+--------------------+-----------------------------------------------------------------------------------------------+
| TransactionStatus  | A string describing the status of an ongoing transaction. See `TransactionStatus`_ for the    |
|                    | typical strings that can be returned. May be null when no transaction is happening.           |
+--------------------+-----------------------------------------------------------------------------------------------+


Transaction
-----------

These fields are returned with a Transaction request. Note that CardHash and CardRef are only available for
Device service versions built on or after 25th March 2021.

+------------------------+----------------------------------------------------------+--------------------+
| Name                   | Description                                              | Data Type          |
+========================+==========================================================+====================+
| CustomerReceipt        | Holds multiple fields for a customer receipt, will       | String             |
|                        | return empty if the transaction is not approved.         |                    |
+------------------------+----------------------------------------------------------+--------------------+
| TransactionApproved    | Shows the status of the transaction to determine if the  | Bool               |
|                        | transaction was approved and confirmed successfully.     |                    |
+------------------------+----------------------------------------------------------+--------------------+
| `Errors`_              | ErrorCode that indicates whether communication with the  | String             |
|                        | terminal was successful and what error was encountered.  |                    |
|                        | See the `Errors`_ for all the possible values.           |                    |
|                        | An approved transaction should also return no errors.    |                    |
+------------------------+----------------------------------------------------------+--------------------+
| CardHash               | Unique identifier for the payment card that can be used  | String             |
|                        | together with CardRef to identify and reuse the payment  |                    |
|                        | card and to store the information safely as token.       |                    |
|                        | May be null if transaction fails or no card info found.  |                    |
|                        | Only available for builds after 2021/03/25.              |                    |
+------------------------+----------------------------------------------------------+--------------------+
| CardRef                | Unique identifier for the payment card that can be used  | String             |
|                        | together with CardHash to identify and reuse the payment |                    |
|                        | card and to store the information safely as token.       |                    |
|                        | May be null if transaction fails or no card info found.  |                    |
|                        | Only available for builds after 2021/03/25.              |                    |
+------------------------+----------------------------------------------------------+--------------------+

Errors
------

+-------------------------+---------------------------------------------------------------------------------------------+
| Error value             | Description                                                                                 |
+=========================+=============================================================================================+
| Unknown                 | The transaction has returned an unknown error code. Please contact our support team to help |
|                         | to debug this issue.                                                                        |
+-------------------------+---------------------------------------------------------------------------------------------+
| Cancelled               | The transaction was cancelled either on the pin pad, through a timeout or because the card  |
|                         | was removed before the transaction was completed.                                           |
+-------------------------+---------------------------------------------------------------------------------------------+
| Declined                | The transaction was declined by the payment gateway, please try again or contact your card  |
|                         | provider if this issue occurs across multiple payment terminals.                            |
+-------------------------+---------------------------------------------------------------------------------------------+
| CardReaderUnavailable   | The card reader is currently in a state where it cannot handle a transaction request. To    |
|                         | get a detailed reason, make a `Status`_ call to the API.                                    |
+-------------------------+---------------------------------------------------------------------------------------------+
| ServiceIsBusy           | Service is currently busy dealing with an existing transaction posted to the device service.|
+-------------------------+---------------------------------------------------------------------------------------------+
| InvalidValue            | Card terminal has been passed an incorrect value, ensure the amount only includes digits 0-9|
|                         | and that the amount is above 25. If a reference is entered, ensure it is unique.            |
+-------------------------+---------------------------------------------------------------------------------------------+


StatusCodes
------------

+----------------------------+-----------------------------------------------------------------------------------------------+
| State value                | Description                                                                                   |
+============================+===============================================================================================+
| Available                  | Your configured device has been found and is ready to receive instructions.                   |
+----------------------------+-----------------------------------------------------------------------------------------------+
| DeviceUnavailable          | Your configured device has been found, but is currently unable to receive instructions.       |
+----------------------------+-----------------------------------------------------------------------------------------------+
| DeviceUnconfigured         | Could not find your configured device, ensure config settings are set correctly.              |
+----------------------------+-----------------------------------------------------------------------------------------------+
| ChipDNAServerIsUnavailable | There is an issue with the ChipDNA service, ensure the service is setup and the config values |
|                            | are set correctly.                                                                            |
+----------------------------+-----------------------------------------------------------------------------------------------+
| TransactionInProgress      |  Card terminal has recognised there is an ongoing transaction, ensure no other services or    |
|                            |  clients are talking to the ChipDNAServer. You should rarely see this error message. Please   |
|                            |  contact our support team if you see this in a live environment                               |
+----------------------------+-----------------------------------------------------------------------------------------------+
| Unknown                    | An unknown error has occurred when checking state, please contact our support team.           |
+----------------------------+-----------------------------------------------------------------------------------------------+
| DuplicateTransaction       | The reference that was provided is not unique, ensure you generate a new one for each         |
|                            | transaction. Alternatively, don't pass in the reference field and let it auto generate.       |
+----------------------------+-----------------------------------------------------------------------------------------------+


TransactionStatus
-----------------

These status strings may be returned when a transaction is in progress. Note that not all the strings are returned
for all types or transactions, and it may be possible to get results not listed here.

+-----------------------------+
| Value                       |
+=============================+
| RequestingTransaction       |
+-----------------------------+
| TransactionStarted          |
+-----------------------------+
| OnlineAuthRequested         |
+-----------------------------+
| OnlineAuthCompleted         |
+-----------------------------+
| ApplicationSelectionStarted |
+-----------------------------+
| AmountConfirmationStarted   |
+-----------------------------+
| CardRemovalRequested        |
+-----------------------------+
| PanKeyEntryStarted          |
+-----------------------------+
| PanKeyEntryCompleted        |
+-----------------------------+
| PinEntryStarted             |
+-----------------------------+
| TippingRequested            |
+-----------------------------+
| VoiceReferralCompleted      |
+-----------------------------+
| Unknown                     |
+-----------------------------+
