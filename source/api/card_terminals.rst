==================
Card Terminal APIs
==================

.. toctree::
   :maxdepth: 2

   BDS_api
   globalpay_api
   acceptacard_api
   creditcallclient_api
   kinetic_api
   latpay_api
