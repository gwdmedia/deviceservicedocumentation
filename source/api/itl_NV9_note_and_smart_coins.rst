=============================================
ITL NV9 Note Reader and Smart Coin System API
=============================================

The ITL NV9 Note Reader and SMART Coin System must be used together sharing the same COM port. They share
their configuration file in the device service settings file NV9NoteAcceptorService.config. The API is mostly shared,
though some commands will only target one of the devices.

Please note that while both devices are run together, you can still only send commands to one device at a time as they
share the COM port. You may get a busy response if one of the devices is responding to a slow request or running a slow
process, so you should ensure you handle the busy status in your client application and retry any critical operations.

Another thing to note is that unlike the NV200 with the Smart Payout, the NV9 has not payout and
cannot pay any notes out of the device. The notes are held in escrow when first inserted, and after you
stack the note, it cannot be paid out again. Only coins can be dispensed as change.


Url
---
The base url is http://localhost:8526/DeviceService/ITL/NV9AndCoin.

Get Status
----------
Get the current status of the NV9 Note Reader and SMART Coin System. The status also contains information of cash inserted since when
you last time checked the status and any note currently in escrow. It's recommended to check status before
you enable the devices to make sure it doesn't have any notes that haven't been dealt with and that it isn't in a fatal state.
It is also recommended to check status after you disable the devices to make sure it's successfully disabled and all cash received has been dealt with.


:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

        ::

            GET http://localhost:8526/DeviceService/ITL/NV9AndCoin/Status HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Devices Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "CurrentNoteAcceptorState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false,
                "IsCashboxInPlace": true,
                "IsStackerFull": false
            },
            "EscrowedBill": {
                "WhenInserted": "2018-03-07T10:15:41.6316841Z",
                "Value": 5,
                "Currency": "GBP"
            },
            "CashReceivedSinceLastCheck": [
                {
                    "WhenInserted": "2018-03-07T10:15:38.0089773Z",
                    "Value": 10,
                    "Currency": "GBP"
                }
            ],
            "CashDispensedSinceLastCheck": [],
            "CurrentHopperState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false
            }
        }

:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



Enable the devices to accept cash
---------------------------------
Enable the note reader and coin acceptor to start taking in cash. The devices will reject all
cash if they are disabled. It's recommended to call `Get Status`_
every 500 - 1000ms after the the devices is enabled so you can get information of cash inserted in a timely manner.
The devices will remain enabled until `Disable the Cash Acceptors`_ is called or a fatal error occurs.


:URL: /Enable
:Method: POST
:URL Params:  none
:Data Params: none
:Example:

    ::

        POST http://localhost:8526/DeviceService//ITL/NV9AndCoin/Enable HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the devices when they are busy.
    This usually happens if you send a new request while device is still processing the previous request or
    is stacking cash.
    Please notice that `Get Status`_ request will not return the error and will not cause other requests
    to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy.
                        Please wait until other actions have finished."
                    }
                ]
            }
        }



    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the devices have encountered an issue. You can get more details by sending
    a `Get Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "Cannot enable the device since the note or
                  coin machine is not currently connected."
                }
              ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Check Can Dispense Coins
------------------------
Use this endpoint to check if an amount is possible to be dispensed as coins to the end user.
You should call this before you call `Dispense Coins`_ so that you can avoid getting an error if there
is not suitable combination of coins available.


:URL: /CheckCanDispense
:Method: POST
:URL Params:  none
:Data Params:

    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | Name      | Required | Description                                                                                           | Data Type                 |
    +===========+==========+=======================================================================================================+===========================+
    | amount    |  true    | The total coin amount to be dispensed.                                                                | Decimal (or integer)      |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | currency  |  true    | Three letter currency code for the note to be dispensed, for example "GBP". Must match the data       | Boolean (default to true) |
    |           |          | data channels the device has been set up with.                                                        |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NV9AndCoin/CheckCanDispense HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

        {
            "amount": 5.81,
            "currency": "GBP"
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: The response has a boolean "CanDispense" that tells you whether the amount is available or not.

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "CanDispense": true
        }


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to dispense when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions have finished."
                    }
                ]
            }
        }


    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Invalid Argument

    This is returned if the parameters provided are missing or incorrect. See the ErrorCode, FieldName and Message
    for further information.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": "invalid_argument",
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "invalid",
                        "FieldName": "Amount",
                        "Message": "Please provide an Amount that is greater than 0."
                    }
                ]
            }
        }





    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the coin dispenser has encountered an issue while attempting to
    check if the amount is available.nv

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Coin machine cannot dispense coin because of an unexpected error."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





Dispense Coins
--------------
Use this endpoint to dispense coins to the end user. Notes cannot be dispensed so any change given
to the user must always be coins. Errors will be returned if dispensing is not possible for any reason.
To reduce the number of errors, you should call the `Check Can Dispense Coins`_ beforehand, and only request the dispense
if the amount is available.

When the dispensing is finished, the response will confirm the amount of coins dispensed.
You should ensure you do not time out this request until a response has been returned as otherwise you will not know for certain if the
note was dispensed or not. If the coin hopper is low on change, it may take a while to find the coins it needs for the dispense.

:URL: /Dispense
:Method: POST
:URL Params:  none
:Data Params:

    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | Name      | Required | Description                                                                                           | Data Type                 |
    +===========+==========+=======================================================================================================+===========================+
    | amount    |  true    | The total coin amount to be dispensed.                                                                | Decimal (or integer)      |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | currency  |  true    | Three letter currency code for the note to be dispensed, for example "GBP". Must match the data       | Boolean (default to true) |
    |           |          | data channels the device has been set up with.                                                        |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | timestamp |  true    | Current UTC timestamp in  ISO 8601 format, e.g. "2018-10-18T16:49:56Z". Call will be rejected if the  | Datetime as ISO 8601      |
    |           |          | timestamp is too old.                                                                                 | formatted string          |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+
    | signature |  true    | SHA256 hash of the timestamp sent with the request formatted as yyyyMMddHHmmss UTC plus the MD5 hash  | String                    |
    |           |          | of the dispensing password that was provided to you with the set up instructions.                     |                           |
    |           |          | SHA256(yyyyMMddHHmmss + md5(password)). UTF8 encoding is used for the hashes.                         |                           |
    +-----------+----------+-------------------------------------------------------------------------------------------------------+---------------------------+


:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NV9AndCoin/Dispense HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

        {
            "amount": 5.8,
            "currency": "GBP",
            "timestamp": "2020-10-18T16:49:56Z",
            "signature": "bf73a37326b58c3cac9fc3fd3931f6ea547f5b1ab1b6b3567d95c019d04454a5"
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Dispense Coins Response`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "currency": "GBP",
            "dispensed_amount": 4.97
        }


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to dispense when the device is busy.
    This usually happens if you send a new request while device is still processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions have finished."
                    }
                ]
            }
        }


    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Invalid Argument

    This is returned if the parameters provided are missing or incorrect. See the ErrorCode, FieldName and Message
    for further information.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": "invalid_argument",
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "required",
                        "FieldName": "Signature",
                        "Message": "Please provide a valid signature."
                    }
                ]
            }
        }





    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the coin dispenser has encountered an issue while attempting to dispense the requested amount.
    This can also mean the requested amount was not available in the hopper.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Coin machine cannot dispense coin because of an unexpected error."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



Disable the Cash Acceptors
--------------------------
Disable the note reader and coin device to stop accepting cash. If the devices are disabled, they will reject
all cash inserted. It is recommended to perform a `Get Status`_ request after successfully disabling the devices
to ensure there was no cash inserted and not dealt with.
If you disable the devices while there is a note in escrow, the note will be held until you call to reject or stack it.

:URL: /Disable
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NV9AndCoin/Disable/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to enable or disable the devices when they are busy. This usually happens if
    you send a new request while device is processing the previous request. Please notice that `Get Status`_ request will
    not return the error and will not cause other requests to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the devices have encountered an issue. You can get more details by sending
    a `Get Status`_ request. The issue is usually hardware related, for example it has been disconnected.

    If you need to troubleshoot the issue, you can also check log files, which are
    in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "Cannot disable the device since the note or
                  coin machine is not currently connected."
                }
              ]
            }
        }


    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Return Escrowed Note
--------------------
Return the note currently in escrow. After you send the request, the escrowed note will be returned to user.
This request is usually used when a note is in escrow and you do not want to stack the note, for example
if you do not have enough change for that denomination.
If there is no note in escrow, an error will be returned.

:URL: /ReturnEscrow
:Method: POST
:URL Params:  none
:Data Params: none

:Example:
    ::

        POST http://localhost:8526/DeviceService/ITL/NV9AndCoin/ReturnEscrow/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT


:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

            This is returned if you are trying to return an escrowed note while the device is busy. This usually happens if
            you send a new request while device is processing the previous request.
            Please note that `Get Status`_ request will not return the error and will not cause other requests
            to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: No Escrow Error

            This is returned if you are trying to return when there isn't a note in escrow.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "no_escrow",
                        "Message": "There is no note held in escrow. Please check state to find out more"
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

            This is returned when the device has encountered an issue and therefore cannot return the note. You can get
            more details by sending a `Get Status`_ request. The issue is usually hardware related, for example the device has motor error.
            If you need to troubleshoot the issue, you can also check the log files in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unexpected error. Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

Stack Escrowed Note
-------------------
Stack the note that is currently in escrow. After you send the request, the escrowed note will be stacked to cash box and it will no longer be possible
to return it to the user. If there is no note in escrow, an error will be returned.

:URL: /StackEscrow
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/ITL/NV9AndCoin/StackEscrow/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: none

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

        This is returned if you are trying to stack an escrowed note while the device is busy. This usually happens if
        you send a new request while device is processing the previous request.
        Please note that `Get Status`_ request will not return the error and will not cause other requests
        to receive this error.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: No Escrow Error

            This is returned if you are trying to stack when there is no note in escrow.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "BadRequest",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "no_escrow",
                        "Message": "There is no note held in escrow. Please check state to find out more"
                    }
                ]
            }
        }


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

            This is returned when the device has encountered an issue and therefore cannot stack the note. You can get
            more details by sending a `Get Status`_ request. The issue is usually hardware related.
            If you need to troubleshoot the issue, you can also check the log files
            in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "device_error",
                        "Message": "Unexpected error. Please check state to find out more."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

        This is returned if the username or password are incorrect or have not been provided.
        The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

API Models
----------

Devices Status
==============

The status is a JSON object returned with a `Get Status`_ request. It has five fields:
CurrentNoteAcceptorState, CurrentHopperState, EscrowedBill, CashDispensedSinceLastCheck and CashReceivedSinceLastCheck.
See below for further information about each.

:Example:

    ::

        {
            "CurrentNoteAcceptorState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false,
                "IsCashboxInPlace": true,
                "IsStackerFull": false
            },
            "EscrowedBill": null,
            "CashReceivedSinceLastCheck": [],
            "CashDispensedSinceLastCheck": [],
            "CurrentHopperState": {
                "IsConnected": true,
                "IsEnabled": false,
                "IsJammed": false
            }
        }


CurrentNoteAcceptorState
^^^^^^^^^^^^^^^^^^^^^^^^

The current note acceptor state has five boolean values that describe the status of the device.

+--------------------+-------------------------------------------------------------------------------------------------------+
| Field              | Explanation                                                                                           |
+====================+=======================================================================================================+
| IsConnected        | Whether the device is connected or not. Will be false if serial port cannot be opened or the device   |
|                    | cannot be polled. Default false.                                                                      |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsEnabled          | Is the device enabled to accept notes. Default false.                                                 |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsJammed           | Is the device detected to be jammed. Log files will show more details of jams. Default false.         |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsCashboxInPlace   | Is the cash box in place. If not, the device will not accept any notes. Default true.                 |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsStackerFull      | Is the cash box full. If yes, the device will not accept any more notes. Default false.               |
+--------------------+-------------------------------------------------------------------------------------------------------+


CurrentHopperState
^^^^^^^^^^^^^^^^^^

The current hopper state has three boolean values that describe the status of the smart coin system (aka coin hopper).

+--------------------+-------------------------------------------------------------------------------------------------------+
| Field              | Explanation                                                                                           |
+====================+=======================================================================================================+
| IsConnected        | Whether the device is connected or not. Will be false if serial port cannot be opened or the device   |
|                    | cannot be polled. Default false.                                                                      |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsEnabled          | Is the device enabled to accept coins. Default false.                                                 |
+--------------------+-------------------------------------------------------------------------------------------------------+
| IsJammed           | Is the device detected to be jammed. Log files will show more details of jams. Default false.         |
+--------------------+-------------------------------------------------------------------------------------------------------+


EscrowedBill
^^^^^^^^^^^^

This field shows the escrowed note that is held in the note reader but has not yet been accepted.
If there isn't any note in escrow, it will be null.
You should either return or stack the escrowed note by calling `Return Escrowed Note`_ or `Stack Escrowed Note`_ respectively.
When the note is stacked, the note will show in the `CashReceivedSinceLastCheck`_ array.

If there is a note in escrow, you can see its value, currency and timestamp of when it was inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was inserted.       |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+

CashReceivedSinceLastCheck
^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a list of cash that are received and accepted by the note and coin reader since when you last time check the status by `Get Status`_ .
The list is cleaned up afterwards, so next time when you check the status, you will get a new list. The notes on this list can
not be returned to the user, but the coins should be available to be dispensed.

Each item in the list has its value, currency and timestamp of when it's inserted.

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was inserted.       |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+


CashDispensedSinceLastCheck
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a list of amounts that have been dispensed by the coin dispenser since when you last time checked the status by `Get Status`_ .
The list is cleaned up afterwards, so next time when you check the status, you will get a new list.
Each item in the list has its value, currency and timestamp of when it was dispenedd

+--------------+-----------------------------------------------------------------+
| Field        | Explanation                                                     |
+==============+=================================================================+
| WhenInserted | The UTC ISO-8601 timestamp of when the note was dispensed.      |
+--------------+-----------------------------------------------------------------+
| Value        | The value of the note. For example, 1 means one pound in GBP.   |
+--------------+-----------------------------------------------------------------+
| Currency     | The currency code of the note, for example GBP or EUR.          |
+--------------+-----------------------------------------------------------------+


Dispense Coins Response
=======================

The response for `Dispense Coins`_ request has two fields, as described below:

+------------------+------------------------------------------------------------------+
| Field            | Explanation                                                      |
+==================+==================================================================+
| dispensed_amount | Total amount dispensed. Should match the requested amount, but   |
|                  | may differ in case an error occurred.                            |
+------------------+------------------------------------------------------------------+
| currency         | The currency code of cash dispensed, for example GBP or EUR.     |
+------------------+------------------------------------------------------------------+

