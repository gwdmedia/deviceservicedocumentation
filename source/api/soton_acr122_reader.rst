============================
Soton ACR 122 NFC Reader API
============================

This endpoint allows reading NFC smart cards and getting the customer account number for Southampton using the ACR 122 reader.
Please note that other smartcards cannot be read using this endpoint.

Url
---
The base url is http://localhost:8526/DeviceService/ACR122Reader/Soton/

Get Status
----------
Get the current connection status of the reader. You can see if the device is currently connected to the PC, whether it
is active or not, and if there is a smart card on the reader.


:URL: /Status

:Method: GET

:URL Params:  none

:Data Params: none


:Example:
    ::

        GET http://localhost:8526/DeviceService/ACR122Reader/Soton/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

:Code: 200 OK

:Content-Type: application/json

:Content Body: `Reader Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "IsConnected": true,
            "IsActive": false,
            "CardOnReader": false
        }



:Error Response:

:Code: 401 Unauthorized
    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


Read Account Number
-------------------
This endpoint activates the reader, waits for a smart card to be placed on the reader, and then reads the account number from
the card. A result is returned with the status of the read and the account number if one was found.
If a card is not found before the device times out,
a "NoCardFound" response is returned. Only one account number can be requested at a time, and you should always wait until a response is
returned as the results are not stored, they are only sent in the response. The timeout for waiting for a card to be read can be set
on the device service configuration.

:URL: /AccountNumber

:Method: GET

:URL Params:  none

:Data Params: none


:Example:

        ::

            GET http://localhost:8526/DeviceService/ACR122Reader/Soton/AccountNumber/ HTTP/1.1
            Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
            Content-Type: application/json


:Success Response:

  :Code: 200 OK

  :Content-Type: application/json

  :Content Body: `Read Soton Account Number`_ with status "CardRead"

    ::

      HTTP/1.1 200 OK
      Content-Length: 37
      Content-Type: application/json; charset=utf-8
      Server: Microsoft-HTTPAPI/2.0
      Date: Thu, 16 Mar 2017 11:12:53 GMT
      {
          "AccountNumber": "111222333",
          "Status": "CardRead"
      }


:No Card found Response:

  :Code: 200 OK

  :Content-Type: application/json

  :Content Body: `Read Soton Account Number`_ with status "NoCardFound"

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "AccountNumber": "",
            "Status": "NoCardFound"
        }


:Error Responses:


    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This error is returned if a suitable reader is not connected.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "ACR 122 reader is not connected"
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }



--------------------------------------------------------------------------------------------------------------

Soton ACR 122 NFC Reader API Models
-----------------------------------

Reader Status
==============

The reader status is a JSON object returned with a `Get Status`_ request. It has three boolean fields to describe the status.

+--------------+-----------------------------------------------------------------------------------------------+
| Field name   | Explanation                                                                                   |
+==============+===============================================================================================+
| IsConnected  | Is the ACR reader connected to the PC or not.                                                 |
+--------------+-----------------------------------------------------------------------------------------------+
| IsActive     | Is the reader actively trying to find a Soton card. Note that the reader is always on, but    |
|              | it will only attempt to read Soton cards while active.                                        |
+--------------+-----------------------------------------------------------------------------------------------+
| CardOnReader | Is the reader detecting any smart card on the reader - regardless if active or not.           |
+--------------+-----------------------------------------------------------------------------------------------+


Read Soton Account Number
=========================

When making a `Read Account Number`_ request, the following response is returned.

+-------------------+-------------------------------------------------------------------------+
| Field name        | Explanation                                                             |
+===================+=========================================================================+
| Status            | Status of the read. One of `Card Reading State`_ options.               |
+-------------------+-------------------------------------------------------------------------+
| AccountNumber     | The account number as string. Empty if no account number or card found. |
+-------------------+-------------------------------------------------------------------------+

Card Reading State
==================

+------------------+-----------------------------------------------------------------------------------------------+
| Value            | Explanation                                                                                   |
+==================+===============================================================================================+
| NoCardFound      | No card was found on the reader before timing out.                                            |
+------------------+-----------------------------------------------------------------------------------------------+
| CardRead         | Card was found and account number successfully read.                                          |
+------------------+-----------------------------------------------------------------------------------------------+
| UnableToReadCard | Card was found but content was not read correctly. This may be because the card is not for    |
|                  | Soton. It can also be one of the older type of cards. The end user should be advised to retry |
|                  | as the reader will swap to use a different protocol so the next read should succeed.          |
+------------------+-----------------------------------------------------------------------------------------------+
































