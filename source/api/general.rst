============
Introduction
============
The DeviceService provides an API for communicating with devices.
The base url for all API calls is http://localhost:8526/DeviceService/ .



General Information
-------------------

The url is different for different devices, for example the BDS base url is http://localhost:8526/DeviceService/BDS/ .
You may choose the devices you need for your applications and only access the relevant endpoints, which wil be detailed
later in this chapter.
Please ensure the required hardware is installed and working prior to using the API.


Authentication
--------------

When consuming the API, all calls must authenticate using 
`Basic Authentication <https://en.wikipedia.org/wiki/Basic_access_authentication/>`_ .
The user name and password will be provided in your setup instructions.

