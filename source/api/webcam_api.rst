===================
WebCam API
===================

This endpoint allows taking snapshots using a webcam. The snapshots are saved on the machine running the Device Service,
they are not sent to or stored on a server.

Url
---
The base url is http://localhost:8526/DeviceService/WebCam.

Take Snapshot
-------------
Take a snapshot using the connected webcam that has been named in the config file.
The snapshots are saved in C:\\ProgramData\\GW Devices\\Applications\\Device Service\\webcam\\snapshots
directory. The name for the files is in format "YYYY-MM-DD_HH-mm-ss-fff_{reference}.jpeg", the actual name is
returned for successful calls.
The web camera name or part of it must be included in the configuration. By default the picture taken is
600 by 800 pixels in size, and the images are kept for 7 days. The size and days to keep images for can be configured.
The "days_to_keep_snapshots" should be set to 0 if the images should be kept indefinitely.

:URL: /Snapshot
:Method: POST
:URL Params:  None
:Data Params:

    +------------+----------+-------------------------------------------------+--------------------+
    | Name       | Required | Description                                     | Data Type          |
    +============+==========+=================================================+====================+
    | reference  | No       | If a reference is provided, it will be included | string             |
    |            |          | in the end of the filename so that the image    |                    |
    |            |          | can be identified more easily. If no reference  |                    |
    |            |          | is provided, the word "snapshot" is used as a   |                    |
    |            |          | default. Note that any whitespace in the        |                    |
    |            |          | reference will be replaced by "_".              |                    |
    +------------+----------+-------------------------------------------------+--------------------+


:Example:

    ::

        POST /DeviceService/WebCam/Snapshot HTTP/1.1
        Content-Type: application/json
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        {
            "reference": "transId1234"
        }

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: Full path and filename for the image that was taken as a SnapshotPath.

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "SnapshotPath": "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\
            webcam\\snapshots\\2018-02-02_11-18-15-177_transId1234.jpeg"
        }



:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to take a snapshot when the web cam is already in use. This usually happens if
    you send a new snapshot request while the device is still processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "The webcam is busy."
                    }
                ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when an error is preventing the snapshot being taken. The "message" will provide more information
    on what caused the error. The issue is usually hardware related, for example if no camera with the name in the
    configuration file can be found, the message "Webcam not found." will be returned.
    Other possible errors can happen if the webcam is unable to take an image for any reason or the image cannot
    be saved in the folder specified. If you need to troubleshoot the issue further, you can check the log files
    which are in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder.

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Errors": [
                    {
                        "ErrorCode": "hardware_error",
                        "Message": "An error occurred while saving a snapshot."
                    }
                ]
            }
        }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

Get Status
----------
This endpoint returns the connection status of the webcam named in the configuration file. If
no camera is found, check the name of the device in the Windows device manager and ensure that the name or part of it is
in the WebCamService configuration file as the device name.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/webcam/status/ HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `WebCam Status`_ as status

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

        {
            "status": "CameraPresent"
        }

:Error Responses:

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


WebCam API Models
-----------------

WebCam Status
=============

+------------------+---------------------------------------------------------------------------------------------------+
| Value            | Explanation                                                                                       |
+==================+===================================================================================================+
| CameraPresent    | Found the web camera, snapshots can be requested.                                                 |
+------------------+---------------------------------------------------------------------------------------------------+
| CameraNotPresent | Unable to find the web camera with the configured name. Ensure a webcam is connected and that the |
|                  | configuration file has the correct name for the camera.                                           |
+------------------+---------------------------------------------------------------------------------------------------+