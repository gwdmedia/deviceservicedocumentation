##################
Device Service API
##################

.. toctree::
   :maxdepth: 2

   general
   card_terminals
   cash_acceptors
   rfid_readers
   other_devices
   receipt_printing



