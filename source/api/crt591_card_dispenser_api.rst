=========================
CRT591 Card Dispenser API
=========================

This endpoint allows dispensing cards using CRT591 card dispenser and monitoring its status.

Url
---
The base url is http://localhost:8526/DeviceService/CardDispenser/CRT591.

Get Status
----------
Get the current status of the CRT591 card dispenser.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/CardDispenser/CRT591/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Dispenser Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

         {
            "CardState": "NoCardInChannel",
            "StackerState": "StackerLow",
            "BinState": "BinOK",
            "DispenserError": "NoError",
            "HasFatalError": false
        }

:Error Responses:
    There isn't any error response for this service.

Dispense IC Card
----------------
Dispense an IC card. After the card is dispensed, the card will stay at the front gate of the device until it's collected.
You can use `Get Status`_ request to check the card state afterwards. If no card is in device channel, then the card has been
collected. You can also recycle the card using `Recycle Card`_  if it's not been collected for long time.

:URL: /DispenseICCard
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/CardDispenser/CRT591/DispenseICCard HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to dispense or recycle a card when the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please note that `Get Status`_ request will
    not return the error, however you will get the error if you send a different request while `Get Status`_ request is
    processing.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the dispenser has encountered an issue and therefore the card was not dispensed. You can get
    more details by sending a `Get Status`_ request. The issue is usually hardware related, for example the dispenser runs
    out the card or it has motor error. If you need to troubleshoot the issue, you can also check log files, which
    is in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

         {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The dispenser has encountered an issue and therefore
                             the card was not dispensed. Please check status to find out more."
                }
              ]
            }
         }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

Recycle Card
------------
Recycle a card which is inside the device channel. This can be called after a card was dispensed if it has not been
collected to remove it. If there isn't any card in the channel, this request will return success immediately.

:URL: /RecycleCard
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/CardDispenser/CRT591/RecycleCard HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: None

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2017 11:12:53 GMT

:Error Responses:

    :Code: 400 Bad Request

    :Content-Type: application/json

    :Content Body: Device Busy Error

    This is returned if you are trying to dispense or recycle a card when the device is busy. This usually happens if
    you send a new request while device is processing the previous request. Please note that `Get Status`_ request will
    not return the error, however you will get the error if you send a different request while `Get Status`_ request is
    processing.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
              "ErrorCode": "DeviceBusy",
              "Errors": [
                {
                  "ErrorCode": "device_busy",
                  "Message": "The device is busy."
                }
              ]
            }
        }

    :Code: 500 Internal Server Error

    :Content-Type: application/json

    :Content Body: Device Error

    This is returned when the dispenser has encountered an issue and therefore the card was not recycled. You can get
    more details by sending a `Get Status`_ request. The issue is usually hardware related, for example the dispenser runs
    out the card or it has motor error. If you need to troubleshoot the issue, you can also check log files, which
    is in "C:\\ProgramData\\GW Devices\\Applications\\Device Service\\logs\\" folder

    ::

        HTTP/1.1 500 Internal Server Error
        Content-Type: application/json; charset=utf-8

         {
            "ResponseStatus": {
              "ErrorCode": "DeviceError",
              "Errors": [
                {
                  "ErrorCode": "hardware_error",
                  "Message": "The dispenser has encountered an issue and therefore
                             the card was not recycled. Please check status to find out more."
                }
              ]
            }
         }

    :Code: 401 Unauthorized

    This is returned if the username or password are incorrect or have not been provided.
    The message body may be empty or it may contain a further description.

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }

--------------------------------------------------------------------------------------------------------------

Card Dispenser API Models
-------------------------

Dispenser Status
================

The dispenser status is a JSON object returned with a `Get Status`_ request. It has five fields: CardState,
StackerState, BinState, Connectivity and HasFatalError. See further information about the fields below.

To determine if the dispenser is available to be used, you must ensure that the device is connected, stacker is not
empty and there is no fatal error.

:Example:

    ::

        {
          "CardState": "NoCardInChannel",
          "StackerState": "StackerLow",
          "BinState": "BinOK",
          "Connectivity": "Connected",
          "HasFatalError": true
        }

CardState
^^^^^^^^^

+-----------------+-----------------------------------------------------------------------------------------------------------+
| Value           | Explanation                                                                                               |
+=================+===========================================================================================================+
| NoCardInChannel | There isn't a card in the channel.                                                                        |
+-----------------+-----------------------------------------------------------------------------------------------------------+
| CardInFront     | There is a card at the front gate. This usually means a card is dispensed but not taken yet.              |
+-----------------+-----------------------------------------------------------------------------------------------------------+
| CardInPosition  | There is a card at RF/IC card operation position. This usually happens when reading/writing the card.     |
+-----------------+-----------------------------------------------------------------------------------------------------------+
| Unknown         | State is unknown.  This usually happens when the device is unavailable, so the state cannot be retrieved. |
+-----------------+-----------------------------------------------------------------------------------------------------------+

StackerState
^^^^^^^^^^^^
+--------------+-----------------------------------------------------------------------------------------------------------+
| Value        | Explanation                                                                                               |
+==============+===========================================================================================================+
| StackerEmpty | There isn't any card in the stacker. This is fatal.  When you get this state, dispensing a card will fail.|
+--------------+-----------------------------------------------------------------------------------------------------------+
| StackerLow   | There are only a few cards in the stacker.                                                                |
+--------------+-----------------------------------------------------------------------------------------------------------+
| StackerOK    | There are enough cards in the stacker.                                                                    |
+--------------+-----------------------------------------------------------------------------------------------------------+
| Unknown      | State is unknown.  This usually happens when the device is unavailable, so the state cannot be retrieved. |
+--------------+-----------------------------------------------------------------------------------------------------------+

BinState
^^^^^^^^

+---------+-----------------------------------------------------------------------------------------------------------+
| Value   | Explanation                                                                                               |
+=========+===========================================================================================================+
| BinOK   | The recycle bin has enough space.                                                                         |
+---------+-----------------------------------------------------------------------------------------------------------+
| BinFull | The recycle bin is full. This is fatal. When you get this state, recycling a card will fail.              |
+---------+-----------------------------------------------------------------------------------------------------------+
| Unknown | State is unknown.  This usually happens when the device is unavailable, so the state cannot be retrieved. |
+---------+-----------------------------------------------------------------------------------------------------------+

Connectivity
^^^^^^^^^^^^

+--------------------+-------------------------------------------------+
| Value              | Explanation                                     |
+====================+=================================================+
| Disconnected       | Device is not connected                         |
+--------------------+-------------------------------------------------+
| Connected          | Device is connected                             |
+--------------------+-------------------------------------------------+
| CommunicationError | Device is connected but has communication issue |
+--------------------+-------------------------------------------------+


HasFatalError
^^^^^^^^^^^^^

+--------------------+-------------------------------------------------+
| Value              | Explanation                                     |
+====================+=================================================+
| false              | Device is not in a fatal error state.           |
+--------------------+-------------------------------------------------+
| true               | Device has an fatal error meaning it is not     |
|                    | available to be used.                           |
+--------------------+-------------------------------------------------+


