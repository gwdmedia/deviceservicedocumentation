Acceptacard and Payworks Miura 010
==================================

This endpoint allows processing transactions using Acceptacard and Payworks Miura 010 card terminal.

Url
---
The base url is http://localhost:8526/DeviceService/Acceptacard/ .

Status
------
Get the current connection and transaction status of the Miura Card reader. The connection status can be used to check the terminal is available
before making a transaction request or when first starting the application. Please note that the card terminal may not
detect all changes in connection status, so a transaction may fail even if the terminal reported to be connected. The terminal gets
the correct status initially, but it is unable to detect some changes in status, like becoming disconnected after being
successfully connected.

The transaction status gives more information about a transaction that is currently in progress. This can be used to display
information to the end user. The information is the same as is shown on the Miura reader while the transaction is in progress.
If the reader doesn't return any information or there is no transaction in progress, the transaction status may be null.
This endpoint can be used to poll while a transaction request is pending to gain insight into what is happening with the
transaction.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/Acceptacard/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Miura Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:12:53 GMT

        {
            "ConnectionStatus": "CONNECTED",
            "TransactionStatus": null
        }


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }





--------------------------------------------------------------------------------------------------------------



Transaction Request
-------------------

Send a new transaction request to the terminal and receive a response detailing the transaction result.
The transaction will first be registered with Acceptacard, and if the registration is successful, then processed with
the Payworks Miura 010 card terminal.
The request result will be returned after the card terminal returns the transaction information.
This may take some time while the customer uses the terminal to pay. You should wait for the result to
be returned. If you time out before the transaction has completed you will not receive the information
but the terminal may still finish the transaction and charge the customer card. Note that you should check the transaction
status to determine success and not rely on the HTTP status code.

:URL: /Transaction

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | Amount     | Yes      | Transaction amount. Must be a positive number. | decimal or string  |
    +------------+----------+------------------------------------------------+--------------------+
    | Reference  | Yes      | Reference for the transaction.                 | string             |
    +------------+----------+------------------------------------------------+--------------------+
    | |rece|     | No       | Acceptacard Email/SMS number to send receipt   | string             |
    |            |          | to - if not provided, no receipt will be sent. |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | |desc|     | No       | Transaction description.                       | string             |
    +------------+----------+------------------------------------------------+--------------------+

.. |rece|  replace:: ReceiptAddress
.. |desc|  replace:: TransactionDescription



:Example:

    ::

        POST http://localhost:8526/DeviceService/Acceptacard/transaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "Reference": "test6",
            "Amount":1.45,
            "ReceiptAddress": "devteam@kittens.com",
            "TransactionDescription": "Card Payment"
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Transaction`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 11:16:04 GMT

            {
                "PayworksTransactionId": "33e1ee9e641711e8a1cbafcf7e30f9e8",
                "Status": "APPROVED",
                "Error": "NoError",
                "ErrorMessage": "",
                "MerchantReceipt": {
                    "ClearingDetails": {
                        "Transaction": "001122",
                        "Authorization": "987654/012345",
                        "Merchant ID": "420000000000000",
                        "Terminal ID": "T0000002"
                    },
                    "TipLineRequired": false,
                    "SignatureLineRequired": false,
                    "PaymentDetails": {
                        "Scheme": "Payworks VISA",
                        "Account": "**** **** **** 0119",
                        "AID": "A0000000031010",
                        "Entry Mode": "Contactless"
                    },
                    "GeneralInformation": {
                        "Information": "STATUS",
                        "Time": "1:00:00 AM",
                        "Date": "1/1/1970",
                        "Amount": "£1.00",
                        "PWID": "33e1ee9e641711e8a1cbafcf7e30f9e8",
                        "Description": "Card Payment",
                        "Type": "Payment",
                        "DccDisclaimer": null,
                        "DccConvertedAmountAndCurrency": null,
                        "DccConversionRate": null
                    },
                    "MerchantDetails": {
                        "Name": "Test Merchant",
                        "Address": "Street 42",
                        "Zip": "10000",
                        "City": "City",
                        "Country": "Germany",
                        "Contact": "+49 (170) 1234567",
                        "Additional Information": ""
                    }
                },
                "CustomerReceipt": {
                    "ClearingDetails": {
                        "Transaction": "001122",
                        "Authorization": "987654/012345",
                        "Merchant ID": "420000000000000",
                        "Terminal ID": "T0000002"
                    },
                    "TipLineRequired": false,
                    "SignatureLineRequired": false,
                    "PaymentDetails": {
                        "Scheme": "Payworks VISA",
                        "Account": "**** **** **** 0119",
                        "AID": "A0000000031010",
                        "Entry Mode": "Contactless"
                    },
                    "GeneralInformation": {
                        "Information": "STATUS",
                        "Time": "1:00:00 AM",
                        "Date": "1/1/1970",
                        "Amount": "£1.00",
                        "PWID": "33e1ee9e641711e8a1cbafcf7e30f9e8",
                        "Description": "Card Payment",
                        "Type": "Payment",
                        "DccDisclaimer": null,
                        "DccConvertedAmountAndCurrency": null,
                        "DccConversionRate": null
                    },
                    "MerchantDetails": {
                        "Name": "Test Merchant",
                        "Address": "Street 42",
                        "Zip": "10000",
                        "City": "City",
                        "Country": "Germany",
                        "Contact": "+49 (170) 1234567",
                        "Additional Information": ""
                    }
                }
            }



        See `Acceptacard Example Transaction JSON`_ for more examples for different kind of transaction results.

:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "amount",
                            "Message": "Amount 0 is not valid for a transaction"
                        }
                    ]
                }
            }

    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT


    500 Internal Server Error

        ::

            HTTP/1.1 500 Internal Server Error
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT
            {
                "ResponseStatus": {
                    "ErrorCode": "DeviceError",
                    "Message": null,
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "device_failure",
                            "Message": "Internal error while processing transaction"
                        }
                    ]
                }
            }


--------------------------------------------------------------------------------------------------------------


Acceptacard API Models
======================

Miura Status
------------

+--------------------+-----------------------------------------------------------------------------------------------+
| Field Name         | Value                                                                                         |
+====================+===============================================================================================+
| ConnectionStatus   | `ConnectionStatus`_ string.                                                                   |
+--------------------+-----------------------------------------------------------------------------------------------+
| TransactionStatus  | String that shows information about the currently ongoing transaction aimed for the end user. |
|                    | E.g. "3.00 GBP Present card" or "Processing transaction..." etc. May be null. The string is   |
|                    | provided by the card terminal so formatting may vary.                                         |
+--------------------+-----------------------------------------------------------------------------------------------+

ConnectionStatus
----------------

+--------------------+-----------------------------------------------------------------------------------------------+
| Value              | Explanation                                                                                   |
+====================+===============================================================================================+
| INITIALIZED        | Terminal is initialized but not connected.                                                    |
+--------------------+-----------------------------------------------------------------------------------------------+
| CONNECTED          | Connected and ready to process transactions.                                                  |
+--------------------+-----------------------------------------------------------------------------------------------+
| DISCONNECTED       | Disconnected and not available for processing a transaction.                                  |
+--------------------+-----------------------------------------------------------------------------------------------+

Transaction
-----------

These fields are returned with a Transaction request.

+------------------------+---------------------------------------------------------+--------------------+
| Name                   | Description                                             | Data Type          |
+========================+=========================================================+====================+
| Status                 | Shows the status of the transaction to determine if the | String             |
|                        | transaction was successful. For example DECLINED or     |                    |
|                        | APPROVED, see `Transaction Status`_ for details.        |                    |
+------------------------+---------------------------------------------------------+--------------------+
| CustomerReceipt        | Details of the transaction as provided by the terminal, | `Receipt`_         |
|                        | aimed to be printed on customer receipt. May be null    |                    |
|                        | and omitted if transaction didn't complete successfully.|                    |
|                        | See CustomerReceipt for fields.                         |                    |
+------------------------+---------------------------------------------------------+--------------------+
| MerchantReceipt        | Details of the transaction as provided by the terminal, | `Receipt`_         |
|                        | aimed to be printed on customer receipt. May be null    |                    |
|                        | and omitted if transaction didn't complete successfully.|                    |
|                        | See MerchantReceipt for fields.                         |                    |
+------------------------+---------------------------------------------------------+--------------------+
| `Error`_               | ErrorCode that indicates whether communication with the | String             |
|                        | terminal was successful and what error was encountered. |                    |
|                        | See the `Error`_ for all the possible values.           |                    |
|                        | Note that this does not indicate whether the            |                    |
|                        | transaction was authorized or declined.                 |                    |
+------------------------+---------------------------------------------------------+--------------------+
| ErrorMessage           | Further description of the `Error`_. May be null or     | String             |
|                        | empty.                                                  |                    |
+------------------------+---------------------------------------------------------+--------------------+
| PayworksTransactionId  | Unique identifier for the transaction provided by       | String             |
|                        | Payworks.                                               |                    |
+------------------------+---------------------------------------------------------+--------------------+


Transaction Status
------------------

+-----------------------+------------------------------------------------------------------------------------------+
| Status String         | Description                                                                              |
+=======================+==========================================================================================+
| APPROVED              | The transaction was approved. The goods can be handed out by the Merchant.               |
+-----------------------+------------------------------------------------------------------------------------------+
| ACCEPTED              | The transaction was accepted offline for Deferred Authorization. For settlement, the     |
|                       | transaction still needs to be submitted for Deferred Authorization and still might be    |
|                       | declined when doing so.                                                                  |
+-----------------------+------------------------------------------------------------------------------------------+
| DECLINED              | The transaction was declined.                                                            |
+-----------------------+------------------------------------------------------------------------------------------+
| ABORTED               | The transaction was aborted by the Merchant or the Shopper. May also happen if signature |
|                       | is required as taking signature is not possible with a kiosk application.                |
+-----------------------+------------------------------------------------------------------------------------------+
| ERROR                 | The transaction errored. The detailed reason for the error can found in the Error and    |
|                       | ErrorMessage of the Transaction Response.                                                |
+-----------------------+------------------------------------------------------------------------------------------+
| INCONCLUSIVE          | The transaction was inconclusive. The state indicates that the SDK wasn't able to        |
|                       | complete the transaction successfully with the backend as it was unable to reach it for  |
|                       | some reason and is unaware of the status of the Transaction on the backend. It is        |
|                       | recommended that you lookup the transaction one more time and check the status.          |
+-----------------------+------------------------------------------------------------------------------------------+
| UNKNOWN               | The status is unknown. This usually indicates an error.                                  |
+-----------------------+------------------------------------------------------------------------------------------+



Error
-----

+-----------------------+------------------------------------------------------------------------------------------+
| Error value           | Description                                                                              |
+=======================+==========================================================================================+
| NoError               | No error during processing. The ErrorMessage will be empty.                              |
+-----------------------+------------------------------------------------------------------------------------------+
| CardReaderError       | An error occurred with the Miura 010 processing the transaction with Payworks. The       |
|                       | ErrorMessage field will contain more information about what went wrong. You can also see |
|                       | the log files for the debugging details if needed.                                       |
+-----------------------+------------------------------------------------------------------------------------------+
| AcceptacardError      | An error occurred when registering the transaction with Acceptacard before connecting to |
|                       | the card terminal. ErrorMessage field will contain more information about what went      |
|                       | wrong. You can also see the log files for the debugging details if needed.               |
+-----------------------+------------------------------------------------------------------------------------------+



Receipt
-------

Receipt contains all the information provided by the Payworks Miura 010 terminal after a transaction
request has been made. The Merchant and Customer receipts share the same fields. Note that fields may be null or empty
if the terminal does not provide the information. Receipts are only returned if the transaction did not fail.

+------------------------+-------------------------------------------------------+-------------------------------+
| Name                   | Description                                           | Data Type                     |
+========================+=======================================================+===============================+
| ClearingDetails        | Details about the clearing.                           | `Clearing Details`_           |
+------------------------+-------------------------------------------------------+-------------------------------+
| TipLineRequired        | Whether the tip line needs to be printed.             | Boolean                       |
+------------------------+-------------------------------------------------------+-------------------------------+
| SignatureLineRequired  | Whether the signature line needs to be printed.       | Boolean                       |
+------------------------+-------------------------------------------------------+-------------------------------+
| PaymentDetails         | Information about the payment card.                   | `Payment Details`_            |
+------------------------+-------------------------------------------------------+-------------------------------+
| GeneralInformation     | General information about the transaction, like time  | `General Information`_        |
|                        | and amount.                                           |                               |
+------------------------+-------------------------------------------------------+-------------------------------+
| MerchantDetails        | Information about the merchant.                       | `Merchant Details`_           |
+------------------------+-------------------------------------------------------+-------------------------------+


Clearing Details
----------------


Receipt clearing details is a collection of keys and values. Both key and value are strings.

+---------------+--------------------+
| Key name      | Example value      |
+===============+====================+
| Transaction   | 001122             |
+---------------+--------------------+
| Authorization | 987654/012345      |
+---------------+--------------------+
| Merchant ID   | 420000000000000    |
+---------------+--------------------+
| Terminal ID   | T0000002           |
+---------------+--------------------+


Payment Details
---------------

Payment details is a collection of keys and values. Both key and value are strings.

+---------------+---------------------+
| Key name      | Example value       |
+===============+=====================+
| Scheme        | Payworks VISA       |
+---------------+---------------------+
| Account       | **** **** **** 0119 |
+---------------+---------------------+
| AID           | A0000000031010      |
+---------------+---------------------+
| Entry Mode    | Contactless         |
+---------------+---------------------+


General Information
-------------------

General Information is a collection of keys and values. Both key and value are strings, values may be null.

+-------------------------------+-------------------------------------+
| Key name                      | Example value                       |
+===============================+=====================================+
| Information                   | STATUS                              |
+-------------------------------+-------------------------------------+
| Time                          | 1:00:00 AM                          |
+-------------------------------+-------------------------------------+
| Date                          | 1/1/1970                            |
+-------------------------------+-------------------------------------+
| Amount                        | £1.00                               |
+-------------------------------+-------------------------------------+
| PWID [2]_                     | 33e1ee9e641711e8a1cbafcf7e30f9e8    |
+-------------------------------+-------------------------------------+
| Description                   | Card Payment                        |
+-------------------------------+-------------------------------------+
| Type                          | Payment                             |
+-------------------------------+-------------------------------------+
| DccDisclaimer                 | null                                |
+-------------------------------+-------------------------------------+
| DccConvertedAmountAndCurrency | null                                |
+-------------------------------+-------------------------------------+
| DccConversionRate             | null                                |
+-------------------------------+-------------------------------------+

.. [2] Payworks Transaction ID


Merchant Details
----------------


Merchant Details is a collection of keys and values. Both key and value are strings, values may be null.

+-------------------------+-------------------------------------+
| Key name                | Example value                       |
+=========================+=====================================+
| Name                    | Test Merchant                       |
+-------------------------+-------------------------------------+
| Address                 | Street 42                           |
+-------------------------+-------------------------------------+
| Zip                     | 10000                               |
+-------------------------+-------------------------------------+
| City                    | Some City                           |
+-------------------------+-------------------------------------+
| Country                 | Germany                             |
+-------------------------+-------------------------------------+
| Contact                 | +49 (170) 1234567                   |
+-------------------------+-------------------------------------+
| Additional Information  | ""                                  |
+-------------------------+-------------------------------------+



--------------------------------------------------------------------------------------------------------------

Acceptacard Example Transaction JSON
====================================

Here are some examples of the JSON returned by a transaction request for different kinds of transaction results.


:Declined:

    ::


        {
            "PayworksTransactionId": "b6a84883680f11e8a6cd1345cfbde2c5",
            "Status": "DECLINED",
            "Error": "NoError",
            "ErrorMessage": "",
            "MerchantReceipt": {
                "ClearingDetails": {},
                "TipLineRequired": false,
                "SignatureLineRequired": false,
                "PaymentDetails": {
                    "Scheme": "Payworks VISA",
                    "Account": "**** **** **** 0119"
                },
                "GeneralInformation": {
                    "Information": "Authorization declined.",
                    "Time": "4:55:33 PM",
                    "Date": "6/4/2018",
                    "Amount": "£100.00",
                    "PWID": "b6a84883680f11e8a6cd1345cfbde2c5",
                    "Description": "",
                    "Type": "Payment",
                    "DccDisclaimer": null,
                    "DccConvertedAmountAndCurrency": null,
                    "DccConversionRate": null
                },
                "MerchantDetails": {
                    "Name": "",
                    "Address": "",
                    "Zip": "",
                    "City": "",
                    "Country": "United Kingdom",
                    "Contact": "",
                    "Additional Information": ""
                }
            },
            "CustomerReceipt": {
                "ClearingDetails": {},
                "TipLineRequired": false,
                "SignatureLineRequired": false,
                "PaymentDetails": {
                    "Scheme": "Payworks VISA",
                    "Account": "**** **** **** 0119"
                },
                "GeneralInformation": {
                    "Information": "Authorization declined.",
                    "Time": "4:55:33 PM",
                    "Date": "6/4/2018",
                    "Amount": "£100.00",
                    "PWID": "b6a84883680f11e8a6cd1345cfbde2c5",
                    "Description": "",
                    "Type": "Payment",
                    "DccDisclaimer": null,
                    "DccConvertedAmountAndCurrency": null,
                    "DccConversionRate": null
                },
                "MerchantDetails": {
                    "Name": "",
                    "Address": "",
                    "Zip": "",
                    "City": "",
                    "Country": "United Kingdom",
                    "Contact": "",
                    "Additional Information": ""
                }
            }
        }



:Contactless:

    ::


        {
            "PayworksTransactionId": "9671f9b8680f11e8aaac8533e1a4ec6f",
            "Status": "APPROVED",
            "Error": "NoError",
            "ErrorMessage": "",
            "MerchantReceipt": {
                "ClearingDetails": {
                    "Transaction": "CI_S3i",
                    "Authorization": "AI_OxX/II_v30",
                    "Merchant ID": "049eaaf0-c99e-4036-bf20-54ae12312312",
                    "Terminal ID": "TEST1234"
                },
                "TipLineRequired": false,
                "SignatureLineRequired": false,
                "PaymentDetails": {
                    "Scheme": "Payworks VISA",
                    "Account": "**** **** **** 0119",
                    "AID": "A0000000031010",
                    "Entry Mode": "Contactless"
                },
                "GeneralInformation": {
                    "Information": "Please retain receipt!",
                    "Time": "4:54:51 PM",
                    "Date": "6/4/2018",
                    "Amount": "£1.78",
                    "PWID": "9671f9b8680f11e8aaac8533e1a4ec6f",
                    "Description": "",
                    "Type": "Payment",
                    "DccDisclaimer": null,
                    "DccConvertedAmountAndCurrency": null,
                    "DccConversionRate": null
                },
                "MerchantDetails": {
                    "Name": "",
                    "Address": "",
                    "Zip": "",
                    "City": "",
                    "Country": "United Kingdom",
                    "Contact": "",
                    "Additional Information": ""
                }
            },
            "CustomerReceipt": {
                "ClearingDetails": {
                    "Transaction": "CI_S3i",
                    "Authorization": "AI_OxX/II_v30",
                    "Merchant ID": "049eaaf0-c99e-4036-bf20-54ae12312312",
                    "Terminal ID": "TEST1234"
                },
                "TipLineRequired": false,
                "SignatureLineRequired": false,
                "PaymentDetails": {
                    "Scheme": "Payworks VISA",
                    "Account": "**** **** **** 0119",
                    "AID": "A0000000031010",
                    "Entry Mode": "Contactless"
                },
                "GeneralInformation": {
                    "Information": "Please retain receipt!",
                    "Time": "4:54:51 PM",
                    "Date": "6/4/2018",
                    "Amount": "£1.78",
                    "PWID": "9671f9b8680f11e8aaac8533e1a4ec6f",
                    "Description": "",
                    "Type": "Payment",
                    "DccDisclaimer": null,
                    "DccConvertedAmountAndCurrency": null,
                    "DccConversionRate": null
                },
                "MerchantDetails": {
                    "Name": "",
                    "Address": "",
                    "Zip": "",
                    "City": "",
                    "Country": "United Kingdom",
                    "Contact": "",
                    "Additional Information": ""
                }
            }
        }


:Card Reader Error:

    ::

        {
            "PayworksTransactionId": null,
            "Status": "UNKNOWN",
            "Error": "CardReaderError",
            "ErrorMessage": "Internal error",
            "MerchantReceipt": null,
            "CustomerReceipt": null
        }


:Acceptacard Error:

    ::

        {
            "PayworksTransactionId": null,
            "Status": "ERROR",
            "Error": "AcceptacardError",
            "ErrorMessage": "Unable to register transaction with Acceptacard",
            "MerchantReceipt": null,
            "CustomerReceipt": null
        }

:Cancelled:

    ::

        {
            "PayworksTransactionId": "ed79a3a6680f11e8a6cd1345cfbde2c5",
            "Status": "ABORTED",
            "Error": "NoError",
            "ErrorMessage": "",
            "MerchantReceipt": {
                "ClearingDetails": {},
                "TipLineRequired": false,
                "SignatureLineRequired": false,
                "PaymentDetails": {},
                "GeneralInformation": {
                    "Information": "Canceled transaction.",
                    "Time": "4:57:05 PM",
                    "Date": "6/4/2018",
                    "Amount": "£5.26",
                    "PWID": "ed79a3a6680f11e8a6cd1345cfbde2c5",
                    "Description": "",
                    "Type": "Payment",
                    "DccDisclaimer": null,
                    "DccConvertedAmountAndCurrency": null,
                    "DccConversionRate": null
                },
                "MerchantDetails": {
                    "Name": "",
                    "Address": "",
                    "Zip": "",
                    "City": "",
                    "Country": "United Kingdom",
                    "Contact": "",
                    "Additional Information": ""
                }
            },
            "CustomerReceipt": {
                "ClearingDetails": {},
                "TipLineRequired": false,
                "SignatureLineRequired": false,
                "PaymentDetails": {},
                "GeneralInformation": {
                    "Information": "Canceled transaction.",
                    "Time": "4:57:05 PM",
                    "Date": "6/4/2018",
                    "Amount": "£5.26",
                    "PWID": "ed79a3a6680f11e8a6cd1345cfbde2c5",
                    "Description": "",
                    "Type": "Payment",
                    "DccDisclaimer": null,
                    "DccConvertedAmountAndCurrency": null,
                    "DccConversionRate": null
                },
                "MerchantDetails": {
                    "Name": "",
                    "Address": "",
                    "Zip": "",
                    "City": "",
                    "Country": "United Kingdom",
                    "Contact": "",
                    "Additional Information": ""
                }
            }
        }


