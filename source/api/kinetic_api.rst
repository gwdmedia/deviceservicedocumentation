Kinetic and Castles MP200
=========================

This endpoint allows processing transactions using Kinetic and Castles MP200 card terminal.

Url
---
The base url is http://localhost:8526/DeviceService/Kinetic/ .

Status
------
Get the current connection and transaction status of the card reader. The connection status can be used to check the terminal is available
before making a transaction request or when first starting the application. Please note that the card terminal may not
detect all changes in connection status, so a transaction may fail even if the terminal reported to be connected. The terminal gets
the correct status initially and checks it periodically after that.

The transaction status gives more information about a transaction that is currently in progress. This can be used to display
information to the end user. The information is the same as is shown on the Mp200 reader while the transaction is in progress.
If the reader doesn't return any information or there is no transaction in progress, the transaction status may be null.
This endpoint can be used to poll while a transaction request is pending to gain insight into what is happening with the
transaction.

:URL: /Status
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/Kinetic/Status HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Kinetic Status`_

    ::

        HTTP/1.1 200 OK
        Content-Length: 37
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2022 11:12:53 GMT

        {
            "IsConnected": true,
            "TransactionInProgress": false,
            "Status": "Idle",
            "SupportOfflineTransaction": false
        }


:Error Response:

    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


--------------------------------------------------------------------------------------------------------------


Transaction Request
-------------------

Send a new transaction request to the terminal and receive a response detailing the transaction result.
The request result will be returned after the card terminal returns the transaction information.
This may take some time while the customer uses the terminal to pay. You should wait for the result to
be returned. If you time out before the transaction has completed you will not receive the information
but the terminal may still finish the transaction and charge the customer card. Note that you should check the transaction
Result to determine success and not rely on the HTTP status code.

:URL: /Transaction

:Method: POST

:URL Params:  none

:Data Params:

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | Amount     | Yes      | Transaction amount. Must be a positive number. | decimal or string  |
    +------------+----------+------------------------------------------------+--------------------+
    | Reference  | No       | Reference for the transaction. Should be a     | string             |
    |            |          | uuid v4 or a new id will be created.           |                    |
    +------------+----------+------------------------------------------------+--------------------+
    | Offline    | No       | Indicates whether the transaction is offline   | boolean            |
    |            |          |                                                | (default to false) |
    +------------+----------+------------------------------------------------+--------------------+



:Example:

    ::

        POST http://localhost:8526/DeviceService/Kinetic/transaction HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json

        {
            "amount": "1.23",
            "Reference": "d9c95df1-b52c-4a44-aa52-ed4b3fd61cee"
        }


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Transaction`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2022 11:16:04 GMT

            {
                "Complete": true,
                "Description": "Transaction found",
                "TransactionId": "d9c95df1-b52c-4a44-aa52-ed4b3fd61cee",
                "Result": "approved",
                "TotalAmount": 2.23,
                "AuthResponse": "AUTH CODE:000B9D",
                "MaskedPan": "**** **** **** 0014",
                "TerminalId": "22001111",
                "CardNetwork": "mastercard",
                "DataSource": "swipe",
                "TransactionType": "sale",
                "SignatureRequired": true,
                "IsContactless": false,
                "VerifiedByPin": false,
                "VerifiedByOdcvm": false
            }



        See `Kinetic Example Transaction JSON`_ for more examples for different kind of transaction results.

:Error Responses:

    400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "amount",
                            "Message": "Amount 0 is not valid for a transaction"
                        }
                    ]
                }
            }


    Device Busy Error

    This is returned if you are trying call this endpoint if another transaction or get transaction request is ongoing.
    This usually happens if you send a new request while device is processing the previous request, but may also happen if
    there is a problem with processing the previous transaction and the device is still busy, but the previous request
    has returned a response.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions finished."
                    }
                ]
            }
        }

    401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT


    500 Internal Server Error

        ::

            HTTP/1.1 500 Internal Server Error
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT
            {
                "ResponseStatus": {
                    "ErrorCode": "DeviceError",
                    "Message": null,
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "device_failure",
                            "Message": "Internal error while processing transaction"
                        }
                    ]
                }
            }


--------------------------------------------------------------------------------------------------------------



Get Transaction details
-----------------------

Query the result of an ongoing or completed transaction. This endpoint can be used if a result of a `Transaction Request`_ is
lost or indicates the transaction has not completed to verify the status and receive a response detailing the transaction result.
The result willbe formatted the same as it would be for a new transaction.
If the transaction is still ongoing, the request will wait until
the processing status has not changed for over 30s before returning, as this increases the chances of the transaction
completing before the call returns. Note that if the "Complete" flag is false,
the transaction is still ongoing and you should keep checking periodically until the transaction completes. This endpoint cannot
be called while a `Transaction Request`_  is ongoing.

:URL: /Transaction

:Method: GET

:URL Params:  uuid

    +------------+----------+------------------------------------------------+--------------------+
    | Name       | Required | Description                                    | Data Type          |
    +============+==========+================================================+====================+
    | uuid       | Yes      | Reference for the transaction. Should be a uuid| string (uuid v4)   |
    |            |          | v4 and for a transaction made on same terminal |                    |
    +------------+----------+------------------------------------------------+--------------------+


:Data Params: None


:Example:

    ::

        GET http://localhost:8526/DeviceService/Kinetic/Transaction/?uuid=e22a1398-1043-4293-97e3-ac25d47475b5 HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Transaction`_

        ::

            HTTP/1.1 200 OK
            Content-Length: 1581
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2022 11:16:04 GMT

            {
                "Complete": true,
                "Description": "Transaction found",
                "TransactionId": "d9c95df1-b52c-4a44-aa52-ed4b3fd61cee",
                "Result": "approved",
                "TotalAmount": 2.23,
                "AuthResponse": "AUTH CODE:000B9D",
                "MaskedPan": "**** **** **** 0014",
                "TerminalId": "22001111",
                "CardNetwork": "mastercard",
                "DataSource": "swipe",
                "TransactionType": "sale",
                "SignatureRequired": true,
                "IsContactless": false,
                "VerifiedByPin": false,
                "VerifiedByOdcvm": false
            }



        See `Kinetic Example Transaction JSON`_ for more examples for different kind of transaction results.

:Error Responses:
    :Code: Device Busy Error

    This is returned if you are trying call this endpoint if another transaction or get transaction request is ongoing.
    This usually happens if you send a new request while device is processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions finished."
                    }
                ]
            }
        }


    :Code: 400 Bad Request

        ::

            HTTP/1.1 400 Bad Request
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2022 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "BadRequest",
                    "Message": "invalid_argument",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "invalid",
                            "FieldName": "uuid",
                            "Message": "kittens is not a valid transaction id, cannot be formatted as Guid"
                        }
                    ]
                }
            }


    :Code: 404 Not Found

        ::

            HTTP/1.1 404 Not Found
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2022 10:33:57 GMT

            {
                "ResponseStatus": {
                    "ErrorCode": "NotFound",
                    "Message": "Requested resource was not found",
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "not_found",
                            "FieldName": "uuid",
                            "Message": "e22a1398-1043-4293-97e3-ac25d47475b2 was not found"
                        }
                    ]
                }
            }



    :Code: 401 Unauthorized

        ::

            HTTP/1.1 401 Unauthorized
            Content-Length: 44
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:50:00 GMT


    :Code: 500 Internal Server Error

        ::

            HTTP/1.1 500 Internal Server Error
            Content-Length: 42
            Content-Type: application/json; charset=utf-8
            Server: Microsoft-HTTPAPI/2.0
            Date: Thu, 16 Mar 2017 10:33:57 GMT
            {
                "ResponseStatus": {
                    "ErrorCode": "DeviceError",
                    "Message": null,
                    "StackTrace": null,
                    "Errors": [
                        {
                            "ErrorCode": "device_failure",
                            "Message": "Internal error while processing transaction"
                        }
                    ]
                }
            }


--------------------------------------------------------------------------------------------------------------

Send offline trasanctions
-------------------------
Send offline transactions to the host. Return when all the transactions are sent.

:URL: /OfflineTransaction/Send
:Method: POST
:URL Params:  none
:Data Params: none

:Example:

    ::

        POST http://localhost:8526/DeviceService/Kinetic/OfflineTransaction/Send HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==
        Content-Type: application/json


:Success Response:

    :Code: 200 OK

    ::

        HTTP/1.1 200 OK
        Server: Microsoft-HTTPAPI/2.0


:Error Response:


    :Code: Device Busy Error

    This is returned if you are trying call this endpoint if another transaction or get transaction request is ongoing.
    This usually happens if you send a new request while device is processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions finished."
                    }
                ]
            }
        }


    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


--------------------------------------------------------------------------------------------------------------

Get offline trasanction count
-----------------------------
Get the number of offline transactions on the device.

:URL: /OfflineTransaction/Count
:Method: GET
:URL Params:  none
:Data Params: none

:Example:

    ::

        GET http://localhost:8526/DeviceService/Kinetic/OfflineTransaction/Count HTTP/1.1
        Authorization: Basic a2l0dGVuczphcmVzb2xvdmVseQ==

:Success Response:

    :Code: 200 OK

    :Content-Type: application/json

    :Content Body: `Kinetic OfflineCount`_

    ::

        HTTP/1.1 200 OK
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2022 11:16:04 GMT

        {
            "offlineCount": 2,
            "storedRecords": 0
        }

:Error Response:


    :Code: Device Busy Error

    This is returned if you are trying call this endpoint if another transaction or get transaction request is ongoing.
    This usually happens if you send a new request while device is processing the previous request.

    ::

        HTTP/1.1 400 Bad Request
        Content-Type: application/json; charset=utf-8

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceBusy",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "device_busy",
                        "Message": "Device is busy. Please wait until other actions finished."
                    }
                ]
            }
        }


    :Code: 401 Unauthorized

    :Example:

    ::

        HTTP/1.1 401 Unauthorized
        Content-Length: 45
        Content-Type: application/json; charset=utf-8
        Server: Microsoft-HTTPAPI/2.0
        Date: Thu, 16 Mar 2018 11:21:10 GMT

        {
            "ResponseStatus": {
                "ErrorCode": "Invalid UserName or Password",
                "Message": "Invalid UserName or Password"
            }
        }


--------------------------------------------------------------------------------------------------------------


Kinetic API Models
==================

Kinetic Status
--------------

+--------------------------+-----------------------------------------------------------------------------------------------+
| Field Name               | Value                                                                                         |
+==========================+===============================================================================================+
| IsConnected              | Boolean to say whether the device is connected or not over the internal API.                  |
+--------------------------+-----------------------------------------------------------------------------------------------+
| TransactionInProgress    | Boolean to advise if a transaction is currently in progress or not.                           |
+--------------------------+-----------------------------------------------------------------------------------------------+
| Status                   | String that shows information about the currently ongoing transaction aimed for the end user. |
|                          | E.g. "Idle" or "Processing transaction..." etc. May be null. The string is                    |
|                          | provided by the card terminal so formatting may vary.                                         |
+--------------------------+-----------------------------------------------------------------------------------------------+
| SupportOfflineTransaction| Boolean to say whether the service supports the offline transaction or not.                   |
+--------------------------+-----------------------------------------------------------------------------------------------+


Transaction
-----------

These fields are returned with a Transaction request or when checking an existing transaction.

+------------------------+---------------------------------------------------------+--------------------+
| Name                   | Description                                             | Data Type          |
+========================+=========================================================+====================+
| Result                 | Shows the status of the transaction to determine if the | String             |
|                        | transaction was successful. For example declined or     |                    |
|                        | approved, see `Transaction Result`_ for details.        |                    |
+------------------------+---------------------------------------------------------+--------------------+
| Complete               | Indicates whether the transaction processing has been   | Boolean            |
|                        | completed or not. Does not mean  that the               |                    |
|                        | transaction was successful and approved, only that a    |                    |
|                        | result has been reached and processing has finished.    |                    |
+------------------------+---------------------------------------------------------+--------------------+
| Description            | Details of the transaction as provided by the terminal. | String             |
|                        | Typically "Transaction found" for completed             |                    |
|                        | transactions. Shows any error message in case of error. |                    |
+------------------------+---------------------------------------------------------+--------------------+
| TransactionId          | Transaction id. This is the same as what the request    | String  (uuid)     |
|                        | was made with if the id passed in was a valid uuid.     |                    |
|                        | A new one is allocated if the formatting is not valid   |                    |
|                        | or no Id is provided. You should check this matches the |                    |
|                        | value you expect and update your record if needed.      |                    |
+------------------------+---------------------------------------------------------+--------------------+
| TotalAmount            | Amount the transaction was for.                         | Decimal            |
+------------------------+---------------------------------------------------------+--------------------+
| AuthResponse           | AuthResponse from the terminal. Print on receipt.       | String             |
+------------------------+---------------------------------------------------------+--------------------+
| MaskedPan              | Masked pan for approved/declined transactions. May be   | String             |
|                        | null.                                                   |                    |
+------------------------+---------------------------------------------------------+--------------------+
| TerminalId             | Terminal Id for approved/declined transactions. May be  | String             |
|                        | null.                                                   |                    |
+------------------------+---------------------------------------------------------+--------------------+
| CardNetwork            | e.g. "visa" or "mastercard". May be null.               | String             |
+------------------------+---------------------------------------------------------+--------------------+
| DataSource             | e.g. "tap" or "swipe". May be null.                     | String             |
+------------------------+---------------------------------------------------------+--------------------+
| TransactionType        | Should always be "sale".                                | String             |
+------------------------+---------------------------------------------------------+--------------------+
| SignatureRequired      | Collect signature on merchant receipt if true.          | Boolean            |
+------------------------+---------------------------------------------------------+--------------------+
| IsContactless          | True if contactless card used.                          | Boolean            |
+------------------------+---------------------------------------------------------+--------------------+
| VerifiedByPin          | True if pin verification used.                          | Boolean            |
+------------------------+---------------------------------------------------------+--------------------+
| VerifiedByOdcvm        | True if on device cvm used.                             | Boolean            |
+------------------------+---------------------------------------------------------+--------------------+
| IsOffline              | True if on the transaction is offline approved.         | Boolean            |
+------------------------+---------------------------------------------------------+--------------------+
| ResponseCode           | Response code once the offline transaction is synced to | String             |
|                        | the host.                                               |                    |
|                        | Null if the transaction is not synced.                  |                    |
|                        | 00 if the transaction is approved by the host.          |                    |
|                        | Any other codes are decliend.                           |                    |
+------------------------+---------------------------------------------------------+--------------------+

Transaction Result
------------------

+-----------------------+------------------------------------------------------------------------------------------+
| Status String         | Description                                                                              |
+=======================+==========================================================================================+
| approved              | The transaction was approved. The goods can be handed out by the Merchant.               |
+-----------------------+------------------------------------------------------------------------------------------+
| declined              | The transaction was declined.                                                            |
+-----------------------+------------------------------------------------------------------------------------------+
| void                  | The transaction was aborted or timed out.                                                |
+-----------------------+------------------------------------------------------------------------------------------+
| error                 | The transaction had an error. The detailed reason for the error can found in Description |
|                       | and in the log files.                                                                    |
+-----------------------+------------------------------------------------------------------------------------------+
| unknown               | The status is unknown. This usually indicates an unexpected error.                       |
+-----------------------+------------------------------------------------------------------------------------------+


Kinetic OfflineCount
--------------------

+-----------------------+------------------------------------------------------------------------------------------+
| Name                  | Description                                                                              |
+=======================+==========================================================================================+
| offlineCount          | The number of approved offline transactions that havn't been synced with the host        |
+-----------------------+------------------------------------------------------------------------------------------+
| storedRecords         | The transaction that are being synced, including the cancelled transactions.             |
+-----------------------+------------------------------------------------------------------------------------------+

--------------------------------------------------------------------------------------------------------------

Kinetic Example Transaction JSON
================================

Here are some examples of the JSON returned by a transaction request for different kinds of transaction results.


:Declined:

    ::


        {
            "Complete": true,
            "Description": "Transaction found",
            "TransactionId": "ff8b9d75-6685-4938-9d01-1fd0a06e948c",
            "Result": "declined",
            "TotalAmount": 2.29,
            "AuthResponse": "Declined",
            "MaskedPan": "**** **** **** 0119",
            "TerminalId": "22001111",
            "CardNetwork": "visa",
            "DataSource": "tap",
            "TransactionType": "sale",
            "SignatureRequired": false,
            "IsContactless": true,
            "VerifiedByPin": false,
            "VerifiedByOdcvm": false
        }




:Contactless:

    ::


        {
            "Complete": true,
            "Description": "Transaction found",
            "TransactionId": "384eec9b-1dc1-4bfb-bce8-fa71b188f1dc",
            "Result": "approved",
            "TotalAmount": 1,
            "AuthResponse": "AUTH CODE: AUTHOK",
            "MaskedPan": "**** **** **** 0119",
            "TerminalId": "22001111",
            "CardNetwork": "visa",
            "DataSource": "tap",
            "TransactionType": "sale",
            "SignatureRequired": false,
            "IsContactless": true,
            "VerifiedByPin": false,
            "VerifiedByOdcvm": false
        }


:Card Reader Error:

    ::

        {
            "ResponseStatus": {
                "ErrorCode": "DeviceError",
                "Message": null,
                "StackTrace": null,
                "Errors": [
                    {
                        "ErrorCode": "kinetic_error",
                        "Message": "Transaction UUID already exists"
                    }
                ]
            }
        }


:Cancelled:

    ::

        {
            "Complete": true,
            "Description": "Transaction found",
            "TransactionId": "2854fb89-96f1-4600-a870-7adc79fc6530",
            "Result": "cancelled",
            "TotalAmount": 2.23,
            "AuthResponse": "Transaction Void",
            "MaskedPan": null,
            "TerminalId": null,
            "CardNetwork": null,
            "DataSource": null,
            "TransactionType": "sale",
            "SignatureRequired": false,
            "IsContactless": false,
            "VerifiedByPin": false,
            "VerifiedByOdcvm": false
        }


