GWD Device Service Documentation
================================

The GWD Device Service offers a simple way talk to devices like card terminals and receipt printers through a locally hosted rest service.

.. toctree::
   :maxdepth: 2

   installation/index.rst
   api/index.rst